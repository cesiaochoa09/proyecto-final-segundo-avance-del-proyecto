-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 01, 2024 at 09:18 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyectocsp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_parameters` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`action_parameters`)),
  `subject` varchar(255) DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `conditions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`conditions`)),
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `action`, `action_parameters`, `subject`, `properties`, `conditions`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`) VALUES
(1, 'plugin::upload.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.121000', '2024-02-28 08:46:05.121000', NULL, NULL),
(2, 'plugin::upload.configure-view', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.129000', '2024-02-28 08:46:05.129000', NULL, NULL),
(3, 'plugin::upload.assets.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.140000', '2024-02-28 08:46:05.140000', NULL, NULL),
(4, 'plugin::upload.assets.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.149000', '2024-02-28 08:46:05.149000', NULL, NULL),
(5, 'plugin::upload.assets.download', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.164000', '2024-02-28 08:46:05.164000', NULL, NULL),
(6, 'plugin::upload.assets.copy-link', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.171000', '2024-02-28 08:46:05.171000', NULL, NULL),
(7, 'plugin::upload.read', '{}', NULL, '{}', '[\"admin::is-creator\"]', '2024-02-28 08:46:05.179000', '2024-02-28 08:46:05.179000', NULL, NULL),
(8, 'plugin::upload.configure-view', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.188000', '2024-02-28 08:46:05.188000', NULL, NULL),
(9, 'plugin::upload.assets.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.196000', '2024-02-28 08:46:05.196000', NULL, NULL),
(10, 'plugin::upload.assets.update', '{}', NULL, '{}', '[\"admin::is-creator\"]', '2024-02-28 08:46:05.204000', '2024-02-28 08:46:05.204000', NULL, NULL),
(11, 'plugin::upload.assets.download', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.212000', '2024-02-28 08:46:05.212000', NULL, NULL),
(12, 'plugin::upload.assets.copy-link', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.219000', '2024-02-28 08:46:05.219000', NULL, NULL),
(16, 'plugin::content-manager.explorer.delete', '{}', 'plugin::users-permissions.user', '{}', '[]', '2024-02-28 08:46:05.277000', '2024-02-28 08:46:05.277000', NULL, NULL),
(17, 'plugin::content-manager.single-types.configure-view', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.284000', '2024-02-28 08:46:05.284000', NULL, NULL),
(18, 'plugin::content-manager.collection-types.configure-view', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.292000', '2024-02-28 08:46:05.292000', NULL, NULL),
(19, 'plugin::content-manager.components.configure-layout', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.300000', '2024-02-28 08:46:05.300000', NULL, NULL),
(20, 'plugin::content-type-builder.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.308000', '2024-02-28 08:46:05.308000', NULL, NULL),
(21, 'plugin::email.settings.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.316000', '2024-02-28 08:46:05.316000', NULL, NULL),
(22, 'plugin::upload.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.323000', '2024-02-28 08:46:05.323000', NULL, NULL),
(23, 'plugin::upload.assets.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.330000', '2024-02-28 08:46:05.330000', NULL, NULL),
(24, 'plugin::upload.assets.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.341000', '2024-02-28 08:46:05.341000', NULL, NULL),
(25, 'plugin::upload.assets.download', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.348000', '2024-02-28 08:46:05.348000', NULL, NULL),
(26, 'plugin::upload.assets.copy-link', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.355000', '2024-02-28 08:46:05.355000', NULL, NULL),
(27, 'plugin::upload.configure-view', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.363000', '2024-02-28 08:46:05.363000', NULL, NULL),
(28, 'plugin::upload.settings.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.371000', '2024-02-28 08:46:05.371000', NULL, NULL),
(29, 'plugin::users-permissions.roles.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.379000', '2024-02-28 08:46:05.379000', NULL, NULL),
(30, 'plugin::users-permissions.roles.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.385000', '2024-02-28 08:46:05.385000', NULL, NULL),
(31, 'plugin::users-permissions.roles.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.393000', '2024-02-28 08:46:05.393000', NULL, NULL),
(32, 'plugin::users-permissions.roles.delete', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.401000', '2024-02-28 08:46:05.401000', NULL, NULL),
(33, 'plugin::users-permissions.providers.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.409000', '2024-02-28 08:46:05.409000', NULL, NULL),
(34, 'plugin::users-permissions.providers.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.416000', '2024-02-28 08:46:05.416000', NULL, NULL),
(35, 'plugin::users-permissions.email-templates.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.423000', '2024-02-28 08:46:05.423000', NULL, NULL),
(36, 'plugin::users-permissions.email-templates.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.432000', '2024-02-28 08:46:05.432000', NULL, NULL),
(37, 'plugin::users-permissions.advanced-settings.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.440000', '2024-02-28 08:46:05.440000', NULL, NULL),
(38, 'plugin::users-permissions.advanced-settings.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.450000', '2024-02-28 08:46:05.450000', NULL, NULL),
(39, 'plugin::i18n.locale.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.459000', '2024-02-28 08:46:05.459000', NULL, NULL),
(40, 'plugin::i18n.locale.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.467000', '2024-02-28 08:46:05.467000', NULL, NULL),
(41, 'plugin::i18n.locale.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.478000', '2024-02-28 08:46:05.478000', NULL, NULL),
(42, 'plugin::i18n.locale.delete', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.486000', '2024-02-28 08:46:05.486000', NULL, NULL),
(43, 'admin::marketplace.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.493000', '2024-02-28 08:46:05.493000', NULL, NULL),
(44, 'admin::webhooks.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.501000', '2024-02-28 08:46:05.501000', NULL, NULL),
(45, 'admin::webhooks.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.509000', '2024-02-28 08:46:05.509000', NULL, NULL),
(46, 'admin::webhooks.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.521000', '2024-02-28 08:46:05.521000', NULL, NULL),
(47, 'admin::webhooks.delete', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.529000', '2024-02-28 08:46:05.529000', NULL, NULL),
(48, 'admin::users.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.537000', '2024-02-28 08:46:05.537000', NULL, NULL),
(49, 'admin::users.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.545000', '2024-02-28 08:46:05.545000', NULL, NULL),
(50, 'admin::users.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.554000', '2024-02-28 08:46:05.554000', NULL, NULL),
(51, 'admin::users.delete', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.561000', '2024-02-28 08:46:05.561000', NULL, NULL),
(52, 'admin::roles.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.569000', '2024-02-28 08:46:05.569000', NULL, NULL),
(53, 'admin::roles.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.576000', '2024-02-28 08:46:05.576000', NULL, NULL),
(54, 'admin::roles.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.584000', '2024-02-28 08:46:05.584000', NULL, NULL),
(55, 'admin::roles.delete', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.592000', '2024-02-28 08:46:05.592000', NULL, NULL),
(56, 'admin::api-tokens.access', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.601000', '2024-02-28 08:46:05.601000', NULL, NULL),
(57, 'admin::api-tokens.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.609000', '2024-02-28 08:46:05.609000', NULL, NULL),
(58, 'admin::api-tokens.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.617000', '2024-02-28 08:46:05.617000', NULL, NULL),
(59, 'admin::api-tokens.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.624000', '2024-02-28 08:46:05.624000', NULL, NULL),
(60, 'admin::api-tokens.regenerate', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.632000', '2024-02-28 08:46:05.632000', NULL, NULL),
(61, 'admin::api-tokens.delete', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.640000', '2024-02-28 08:46:05.640000', NULL, NULL),
(62, 'admin::project-settings.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.647000', '2024-02-28 08:46:05.647000', NULL, NULL),
(63, 'admin::project-settings.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.657000', '2024-02-28 08:46:05.657000', NULL, NULL),
(64, 'admin::transfer.tokens.access', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.664000', '2024-02-28 08:46:05.664000', NULL, NULL),
(65, 'admin::transfer.tokens.create', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.672000', '2024-02-28 08:46:05.672000', NULL, NULL),
(66, 'admin::transfer.tokens.read', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.680000', '2024-02-28 08:46:05.680000', NULL, NULL),
(67, 'admin::transfer.tokens.update', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.689000', '2024-02-28 08:46:05.689000', NULL, NULL),
(68, 'admin::transfer.tokens.regenerate', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.697000', '2024-02-28 08:46:05.697000', NULL, NULL),
(69, 'admin::transfer.tokens.delete', '{}', NULL, '{}', '[]', '2024-02-28 08:46:05.705000', '2024-02-28 08:46:05.705000', NULL, NULL),
(76, 'plugin::content-manager.explorer.publish', '{}', 'plugin::users-permissions.user', '{}', '[]', '2024-02-29 09:31:12.161000', '2024-02-29 09:31:12.161000', NULL, NULL),
(80, 'plugin::content-manager.explorer.delete', '{}', 'api::cargas-combustible.cargas-combustible', '{}', '[]', '2024-02-29 09:42:15.745000', '2024-02-29 09:42:15.745000', NULL, NULL),
(81, 'plugin::content-manager.explorer.publish', '{}', 'api::cargas-combustible.cargas-combustible', '{}', '[]', '2024-02-29 09:42:15.754000', '2024-02-29 09:42:15.754000', NULL, NULL),
(85, 'plugin::content-manager.explorer.delete', '{}', 'api::vehiculo.vehiculo', '{}', '[]', '2024-02-29 09:48:41.843000', '2024-02-29 09:48:41.843000', NULL, NULL),
(86, 'plugin::content-manager.explorer.publish', '{}', 'api::vehiculo.vehiculo', '{}', '[]', '2024-02-29 09:48:41.871000', '2024-02-29 09:48:41.871000', NULL, NULL),
(87, 'plugin::content-manager.explorer.create', '{}', 'api::modelo-vehiculo.modelo-vehiculo', '{\"fields\":[\"num\",\"nombre\",\"consumoCombustible\",\"cantCarga\",\"capPasajeros\"]}', '[]', '2024-02-29 09:54:13.974000', '2024-02-29 09:54:13.974000', NULL, NULL),
(88, 'plugin::content-manager.explorer.read', '{}', 'api::modelo-vehiculo.modelo-vehiculo', '{\"fields\":[\"num\",\"nombre\",\"consumoCombustible\",\"cantCarga\",\"capPasajeros\"]}', '[]', '2024-02-29 09:54:13.992000', '2024-02-29 09:54:13.992000', NULL, NULL),
(89, 'plugin::content-manager.explorer.update', '{}', 'api::modelo-vehiculo.modelo-vehiculo', '{\"fields\":[\"num\",\"nombre\",\"consumoCombustible\",\"cantCarga\",\"capPasajeros\"]}', '[]', '2024-02-29 09:54:14.020000', '2024-02-29 09:54:14.020000', NULL, NULL),
(90, 'plugin::content-manager.explorer.delete', '{}', 'api::modelo-vehiculo.modelo-vehiculo', '{}', '[]', '2024-02-29 09:54:14.028000', '2024-02-29 09:54:14.028000', NULL, NULL),
(91, 'plugin::content-manager.explorer.publish', '{}', 'api::modelo-vehiculo.modelo-vehiculo', '{}', '[]', '2024-02-29 09:54:14.038000', '2024-02-29 09:54:14.038000', NULL, NULL),
(92, 'plugin::content-manager.explorer.create', '{}', 'api::marca-vehiculo.marca-vehiculo', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 10:24:01.523000', '2024-02-29 10:24:01.523000', NULL, NULL),
(93, 'plugin::content-manager.explorer.read', '{}', 'api::marca-vehiculo.marca-vehiculo', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 10:24:01.541000', '2024-02-29 10:24:01.541000', NULL, NULL),
(94, 'plugin::content-manager.explorer.update', '{}', 'api::marca-vehiculo.marca-vehiculo', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 10:24:01.549000', '2024-02-29 10:24:01.549000', NULL, NULL),
(95, 'plugin::content-manager.explorer.delete', '{}', 'api::marca-vehiculo.marca-vehiculo', '{}', '[]', '2024-02-29 10:24:01.559000', '2024-02-29 10:24:01.559000', NULL, NULL),
(96, 'plugin::content-manager.explorer.publish', '{}', 'api::marca-vehiculo.marca-vehiculo', '{}', '[]', '2024-02-29 10:24:01.568000', '2024-02-29 10:24:01.568000', NULL, NULL),
(97, 'plugin::content-manager.explorer.create', '{}', 'api::tipo-vehiculo.tipo-vehiculo', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 10:24:55.033000', '2024-02-29 10:24:55.033000', NULL, NULL),
(98, 'plugin::content-manager.explorer.read', '{}', 'api::tipo-vehiculo.tipo-vehiculo', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 10:24:55.050000', '2024-02-29 10:24:55.050000', NULL, NULL),
(99, 'plugin::content-manager.explorer.update', '{}', 'api::tipo-vehiculo.tipo-vehiculo', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 10:24:55.058000', '2024-02-29 10:24:55.058000', NULL, NULL),
(100, 'plugin::content-manager.explorer.delete', '{}', 'api::tipo-vehiculo.tipo-vehiculo', '{}', '[]', '2024-02-29 10:24:55.067000', '2024-02-29 10:24:55.067000', NULL, NULL),
(101, 'plugin::content-manager.explorer.publish', '{}', 'api::tipo-vehiculo.tipo-vehiculo', '{}', '[]', '2024-02-29 10:24:55.075000', '2024-02-29 10:24:55.075000', NULL, NULL),
(105, 'plugin::content-manager.explorer.delete', '{}', 'api::ruta.ruta', '{}', '[]', '2024-02-29 10:33:03.783000', '2024-02-29 10:33:03.783000', NULL, NULL),
(106, 'plugin::content-manager.explorer.publish', '{}', 'api::ruta.ruta', '{}', '[]', '2024-02-29 10:33:03.808000', '2024-02-29 10:33:03.808000', NULL, NULL),
(110, 'plugin::content-manager.explorer.create', '{}', 'api::cargas-combustible.cargas-combustible', '{\"fields\":[\"num\",\"numFolio\",\"litrosCargados\",\"fecha\",\"kilometraje\",\"comentarios\",\"costoTotal\",\"vehiculo\"]}', '[]', '2024-02-29 10:35:14.331000', '2024-02-29 10:35:14.331000', NULL, NULL),
(111, 'plugin::content-manager.explorer.read', '{}', 'api::cargas-combustible.cargas-combustible', '{\"fields\":[\"num\",\"numFolio\",\"litrosCargados\",\"fecha\",\"kilometraje\",\"comentarios\",\"costoTotal\",\"vehiculo\"]}', '[]', '2024-02-29 10:35:14.343000', '2024-02-29 10:35:14.343000', NULL, NULL),
(112, 'plugin::content-manager.explorer.update', '{}', 'api::cargas-combustible.cargas-combustible', '{\"fields\":[\"num\",\"numFolio\",\"litrosCargados\",\"fecha\",\"kilometraje\",\"comentarios\",\"costoTotal\",\"vehiculo\"]}', '[]', '2024-02-29 10:35:14.353000', '2024-02-29 10:35:14.353000', NULL, NULL),
(117, 'plugin::content-manager.explorer.create', '{}', 'api::vehiculo.vehiculo', '{\"fields\":[\"numSerie\",\"placas\",\"kilometraje\",\"fechaAdquisicion\",\"usaDiesel\",\"modelo\",\"marca\",\"tipo\",\"vehiculos_usuarios\"]}', '[]', '2024-02-29 10:39:20.480000', '2024-02-29 10:39:20.480000', NULL, NULL),
(119, 'plugin::content-manager.explorer.read', '{}', 'api::vehiculo.vehiculo', '{\"fields\":[\"numSerie\",\"placas\",\"kilometraje\",\"fechaAdquisicion\",\"usaDiesel\",\"modelo\",\"marca\",\"tipo\",\"vehiculos_usuarios\"]}', '[]', '2024-02-29 10:39:20.499000', '2024-02-29 10:39:20.499000', NULL, NULL),
(121, 'plugin::content-manager.explorer.update', '{}', 'api::vehiculo.vehiculo', '{\"fields\":[\"numSerie\",\"placas\",\"kilometraje\",\"fechaAdquisicion\",\"usaDiesel\",\"modelo\",\"marca\",\"tipo\",\"vehiculos_usuarios\"]}', '[]', '2024-02-29 10:39:20.519000', '2024-02-29 10:39:20.519000', NULL, NULL),
(128, 'plugin::content-manager.explorer.delete', '{}', 'api::parada.parada', '{}', '[]', '2024-02-29 11:01:55.773000', '2024-02-29 11:01:55.773000', NULL, NULL),
(129, 'plugin::content-manager.explorer.publish', '{}', 'api::parada.parada', '{}', '[]', '2024-02-29 11:01:55.782000', '2024-02-29 11:01:55.782000', NULL, NULL),
(130, 'plugin::content-manager.explorer.create', '{}', 'api::estado-parada.estado-parada', '{\"fields\":[\"num\",\"descripcion\"]}', '[]', '2024-02-29 11:03:46.453000', '2024-02-29 11:03:46.453000', NULL, NULL),
(131, 'plugin::content-manager.explorer.read', '{}', 'api::estado-parada.estado-parada', '{\"fields\":[\"num\",\"descripcion\"]}', '[]', '2024-02-29 11:03:46.479000', '2024-02-29 11:03:46.479000', NULL, NULL),
(132, 'plugin::content-manager.explorer.update', '{}', 'api::estado-parada.estado-parada', '{\"fields\":[\"num\",\"descripcion\"]}', '[]', '2024-02-29 11:03:46.496000', '2024-02-29 11:03:46.496000', NULL, NULL),
(133, 'plugin::content-manager.explorer.delete', '{}', 'api::estado-parada.estado-parada', '{}', '[]', '2024-02-29 11:03:46.514000', '2024-02-29 11:03:46.514000', NULL, NULL),
(134, 'plugin::content-manager.explorer.publish', '{}', 'api::estado-parada.estado-parada', '{}', '[]', '2024-02-29 11:03:46.531000', '2024-02-29 11:03:46.531000', NULL, NULL),
(140, 'plugin::content-manager.explorer.create', '{}', 'api::tipo-cobertura.tipo-cobertura', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 11:05:50.472000', '2024-02-29 11:05:50.472000', NULL, NULL),
(141, 'plugin::content-manager.explorer.read', '{}', 'api::tipo-cobertura.tipo-cobertura', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 11:05:50.488000', '2024-02-29 11:05:50.488000', NULL, NULL),
(142, 'plugin::content-manager.explorer.update', '{}', 'api::tipo-cobertura.tipo-cobertura', '{\"fields\":[\"num\",\"nombre\"]}', '[]', '2024-02-29 11:05:50.495000', '2024-02-29 11:05:50.495000', NULL, NULL),
(143, 'plugin::content-manager.explorer.delete', '{}', 'api::tipo-cobertura.tipo-cobertura', '{}', '[]', '2024-02-29 11:05:50.505000', '2024-02-29 11:05:50.505000', NULL, NULL),
(144, 'plugin::content-manager.explorer.publish', '{}', 'api::tipo-cobertura.tipo-cobertura', '{}', '[]', '2024-02-29 11:05:50.513000', '2024-02-29 11:05:50.513000', NULL, NULL),
(146, 'plugin::content-manager.explorer.create', '{}', 'api::parada.parada', '{\"fields\":[\"num\",\"nombre\",\"dirColonia\",\"dirCalle\",\"dirNumero\",\"rutas_paradas\"]}', '[]', '2024-02-29 11:07:55.200000', '2024-02-29 11:07:55.200000', NULL, NULL),
(149, 'plugin::content-manager.explorer.read', '{}', 'api::parada.parada', '{\"fields\":[\"num\",\"nombre\",\"dirColonia\",\"dirCalle\",\"dirNumero\",\"rutas_paradas\"]}', '[]', '2024-02-29 11:07:55.261000', '2024-02-29 11:07:55.261000', NULL, NULL),
(152, 'plugin::content-manager.explorer.update', '{}', 'api::parada.parada', '{\"fields\":[\"num\",\"nombre\",\"dirColonia\",\"dirCalle\",\"dirNumero\",\"rutas_paradas\"]}', '[]', '2024-02-29 11:07:55.336000', '2024-02-29 11:07:55.336000', NULL, NULL),
(154, 'plugin::content-manager.explorer.create', '{}', 'api::aseguradora.aseguradora', '{\"fields\":[\"num\",\"nombre\",\"dirColonia\",\"dirCalle\",\"dirNumero\",\"numTel\",\"correo\"]}', '[]', '2024-02-29 11:09:52.270000', '2024-02-29 11:09:52.270000', NULL, NULL),
(155, 'plugin::content-manager.explorer.read', '{}', 'api::aseguradora.aseguradora', '{\"fields\":[\"num\",\"nombre\",\"dirColonia\",\"dirCalle\",\"dirNumero\",\"numTel\",\"correo\"]}', '[]', '2024-02-29 11:09:52.288000', '2024-02-29 11:09:52.288000', NULL, NULL),
(156, 'plugin::content-manager.explorer.update', '{}', 'api::aseguradora.aseguradora', '{\"fields\":[\"num\",\"nombre\",\"dirColonia\",\"dirCalle\",\"dirNumero\",\"numTel\",\"correo\"]}', '[]', '2024-02-29 11:09:52.297000', '2024-02-29 11:09:52.297000', NULL, NULL),
(157, 'plugin::content-manager.explorer.delete', '{}', 'api::aseguradora.aseguradora', '{}', '[]', '2024-02-29 11:09:52.306000', '2024-02-29 11:09:52.306000', NULL, NULL),
(158, 'plugin::content-manager.explorer.publish', '{}', 'api::aseguradora.aseguradora', '{}', '[]', '2024-02-29 11:09:52.315000', '2024-02-29 11:09:52.315000', NULL, NULL),
(159, 'plugin::content-manager.explorer.create', '{}', 'api::poliza-seguro.poliza-seguro', '{\"fields\":[\"numPoliza\",\"fechaInicio\",\"fechaVencimiento\",\"montoCobertura\",\"costo\",\"frecuenciaPago\",\"vehiculo\",\"aseguradora\",\"cobertura\"]}', '[]', '2024-02-29 11:13:47.923000', '2024-02-29 11:13:47.923000', NULL, NULL),
(160, 'plugin::content-manager.explorer.read', '{}', 'api::poliza-seguro.poliza-seguro', '{\"fields\":[\"numPoliza\",\"fechaInicio\",\"fechaVencimiento\",\"montoCobertura\",\"costo\",\"frecuenciaPago\",\"vehiculo\",\"aseguradora\",\"cobertura\"]}', '[]', '2024-02-29 11:13:47.956000', '2024-02-29 11:13:47.956000', NULL, NULL),
(161, 'plugin::content-manager.explorer.update', '{}', 'api::poliza-seguro.poliza-seguro', '{\"fields\":[\"numPoliza\",\"fechaInicio\",\"fechaVencimiento\",\"montoCobertura\",\"costo\",\"frecuenciaPago\",\"vehiculo\",\"aseguradora\",\"cobertura\"]}', '[]', '2024-02-29 11:13:47.968000', '2024-02-29 11:13:47.968000', NULL, NULL),
(162, 'plugin::content-manager.explorer.delete', '{}', 'api::poliza-seguro.poliza-seguro', '{}', '[]', '2024-02-29 11:13:47.980000', '2024-02-29 11:13:47.980000', NULL, NULL),
(163, 'plugin::content-manager.explorer.publish', '{}', 'api::poliza-seguro.poliza-seguro', '{}', '[]', '2024-02-29 11:13:47.991000', '2024-02-29 11:13:47.991000', NULL, NULL),
(164, 'plugin::content-manager.explorer.create', '{}', 'plugin::users-permissions.user', '{\"fields\":[\"username\",\"provider\",\"resetPasswordToken\",\"confirmationToken\",\"confirmed\",\"blocked\",\"num\",\"nombrePila\",\"apPaterno\",\"apMaterno\",\"numTel\",\"email\",\"password\",\"role\",\"usuarios_vehiculos\",\"rutas_users\"]}', '[]', '2024-02-29 12:18:14.342000', '2024-02-29 12:18:14.342000', NULL, NULL),
(166, 'plugin::content-manager.explorer.read', '{}', 'plugin::users-permissions.user', '{\"fields\":[\"username\",\"provider\",\"resetPasswordToken\",\"confirmationToken\",\"confirmed\",\"blocked\",\"num\",\"nombrePila\",\"apPaterno\",\"apMaterno\",\"numTel\",\"email\",\"password\",\"role\",\"usuarios_vehiculos\",\"rutas_users\"]}', '[]', '2024-02-29 12:18:14.391000', '2024-02-29 12:18:14.391000', NULL, NULL),
(168, 'plugin::content-manager.explorer.update', '{}', 'plugin::users-permissions.user', '{\"fields\":[\"username\",\"provider\",\"resetPasswordToken\",\"confirmationToken\",\"confirmed\",\"blocked\",\"num\",\"nombrePila\",\"apPaterno\",\"apMaterno\",\"numTel\",\"email\",\"password\",\"role\",\"usuarios_vehiculos\",\"rutas_users\"]}', '[]', '2024-02-29 12:18:14.411000', '2024-02-29 12:18:14.411000', NULL, NULL),
(174, 'plugin::content-manager.explorer.create', '{}', 'api::ruta.ruta', '{\"fields\":[\"num\",\"fechaHoraInicio\",\"fechaHoraFin\",\"comentarios\",\"finalizado\",\"paradas_rutas\",\"users_rutas\"]}', '[]', '2024-02-29 12:21:47.877000', '2024-02-29 12:21:47.877000', NULL, NULL),
(176, 'plugin::content-manager.explorer.read', '{}', 'api::ruta.ruta', '{\"fields\":[\"num\",\"fechaHoraInicio\",\"fechaHoraFin\",\"comentarios\",\"finalizado\",\"paradas_rutas\",\"users_rutas\"]}', '[]', '2024-02-29 12:21:47.917000', '2024-02-29 12:21:47.917000', NULL, NULL),
(178, 'plugin::content-manager.explorer.update', '{}', 'api::ruta.ruta', '{\"fields\":[\"num\",\"fechaHoraInicio\",\"fechaHoraFin\",\"comentarios\",\"finalizado\",\"paradas_rutas\",\"users_rutas\"]}', '[]', '2024-02-29 12:21:47.954000', '2024-02-29 12:21:47.954000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions_role_links`
--

CREATE TABLE `admin_permissions_role_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `permission_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_permissions_role_links`
--

INSERT INTO `admin_permissions_role_links` (`id`, `permission_id`, `role_id`, `permission_order`) VALUES
(1, 1, 2, 1),
(2, 2, 2, 2),
(3, 3, 2, 3),
(4, 4, 2, 4),
(5, 5, 2, 5),
(6, 6, 2, 6),
(7, 7, 3, 1),
(8, 8, 3, 2),
(9, 9, 3, 3),
(10, 10, 3, 4),
(11, 11, 3, 5),
(12, 12, 3, 6),
(16, 16, 1, 4),
(17, 17, 1, 5),
(18, 18, 1, 6),
(19, 19, 1, 7),
(20, 20, 1, 8),
(21, 21, 1, 9),
(22, 22, 1, 10),
(23, 23, 1, 11),
(24, 24, 1, 12),
(25, 25, 1, 13),
(26, 26, 1, 14),
(27, 27, 1, 15),
(28, 28, 1, 16),
(29, 29, 1, 17),
(30, 30, 1, 18),
(31, 31, 1, 19),
(32, 32, 1, 20),
(33, 33, 1, 21),
(34, 34, 1, 22),
(35, 35, 1, 23),
(36, 36, 1, 24),
(37, 37, 1, 25),
(38, 38, 1, 26),
(39, 39, 1, 27),
(40, 40, 1, 28),
(41, 41, 1, 29),
(42, 42, 1, 30),
(43, 43, 1, 31),
(44, 44, 1, 32),
(45, 45, 1, 33),
(46, 46, 1, 34),
(47, 47, 1, 35),
(48, 48, 1, 36),
(49, 49, 1, 37),
(50, 50, 1, 38),
(51, 51, 1, 39),
(52, 52, 1, 40),
(53, 53, 1, 41),
(54, 54, 1, 42),
(55, 55, 1, 43),
(56, 56, 1, 44),
(57, 57, 1, 45),
(58, 58, 1, 46),
(59, 59, 1, 47),
(60, 60, 1, 48),
(61, 61, 1, 49),
(62, 62, 1, 50),
(63, 63, 1, 51),
(64, 64, 1, 52),
(65, 65, 1, 53),
(66, 66, 1, 54),
(67, 67, 1, 55),
(68, 68, 1, 56),
(69, 69, 1, 57),
(76, 76, 1, 61),
(80, 80, 1, 65),
(81, 81, 1, 66),
(85, 85, 1, 70),
(86, 86, 1, 71),
(87, 87, 1, 72),
(88, 88, 1, 73),
(89, 89, 1, 74),
(90, 90, 1, 75),
(91, 91, 1, 76),
(92, 92, 1, 77),
(93, 93, 1, 78),
(94, 94, 1, 79),
(95, 95, 1, 80),
(96, 96, 1, 81),
(97, 97, 1, 82),
(98, 98, 1, 83),
(99, 99, 1, 84),
(100, 100, 1, 85),
(101, 101, 1, 86),
(105, 105, 1, 90),
(106, 106, 1, 91),
(110, 110, 1, 95),
(111, 111, 1, 96),
(112, 112, 1, 97),
(117, 117, 1, 99),
(119, 119, 1, 101),
(121, 121, 1, 103),
(128, 128, 1, 110),
(129, 129, 1, 111),
(130, 130, 1, 112),
(131, 131, 1, 113),
(132, 132, 1, 114),
(133, 133, 1, 115),
(134, 134, 1, 116),
(140, 140, 1, 122),
(141, 141, 1, 123),
(142, 142, 1, 124),
(143, 143, 1, 125),
(144, 144, 1, 126),
(146, 146, 1, 128),
(149, 149, 1, 131),
(152, 152, 1, 134),
(154, 154, 1, 136),
(155, 155, 1, 137),
(156, 156, 1, 138),
(157, 157, 1, 139),
(158, 158, 1, 140),
(159, 159, 1, 141),
(160, 160, 1, 142),
(161, 161, 1, 143),
(162, 162, 1, 144),
(163, 163, 1, 145),
(164, 164, 1, 146),
(166, 166, 1, 148),
(168, 168, 1, 150),
(174, 174, 1, 152),
(176, 176, 1, 154),
(178, 178, 1, 156);

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `code`, `description`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`) VALUES
(1, 'Super Admin', 'strapi-super-admin', 'Super Admins can access and manage all features and settings.', '2024-02-28 08:46:05.100000', '2024-02-28 08:46:05.100000', NULL, NULL),
(2, 'Editor', 'strapi-editor', 'Editors can manage and publish contents including those of other users.', '2024-02-28 08:46:05.109000', '2024-02-28 08:46:05.109000', NULL, NULL),
(3, 'Author', 'strapi-author', 'Authors can manage the content they have created.', '2024-02-28 08:46:05.116000', '2024-02-28 08:46:05.116000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `registration_token` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `prefered_language` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `firstname`, `lastname`, `username`, `email`, `password`, `reset_password_token`, `registration_token`, `is_active`, `blocked`, `prefered_language`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`) VALUES
(1, 'Jose', 'Ponce', NULL, '0322103790@ut-tijuana.edu.mx', '$2a$10$pgL/gkLy7187VKV6VtToXusi9cpcT1Icln6jdUwvVdzLvwIpxFetu', NULL, NULL, 1, 0, NULL, '2024-02-28 08:46:59.864000', '2024-02-28 08:46:59.864000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users_roles_links`
--

CREATE TABLE `admin_users_roles_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `role_order` double UNSIGNED DEFAULT NULL,
  `user_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_users_roles_links`
--

INSERT INTO `admin_users_roles_links` (`id`, `user_id`, `role_id`, `role_order`, `user_order`) VALUES
(1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `aseguradoras`
--

CREATE TABLE `aseguradoras` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `dir_colonia` varchar(255) DEFAULT NULL,
  `dir_calle` varchar(255) DEFAULT NULL,
  `dir_numero` varchar(255) DEFAULT NULL,
  `num_tel` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aseguradoras`
--

INSERT INTO `aseguradoras` (`id`, `num`, `nombre`, `dir_colonia`, `dir_calle`, `dir_numero`, `num_tel`, `correo`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', 'Qualitas', 'El Refugio', 'Lienxo charro', 'L-11', '664 208 3939', 'maclovio@qualitas.com.mx', '2024-02-29 12:08:41.862000', '2024-02-29 12:41:40.343000', '2024-02-29 12:08:56.407000', 1, 1),
(2, '2', 'OTAY BORDER INSURANCE', 'Nueva Tijuana', 'Blvd. de las Bellas Artes', '19535-B', '664 979 0301', 'seguros@otayborder.com', '2024-02-29 12:42:49.244000', '2024-02-29 12:42:57.757000', '2024-02-29 12:42:57.752000', 1, 1),
(3, '3', 'Seguros AXA', '20 de Noviembre', 'Blvd. de Las Americas', '3565', '664 381 7352', 'seguros@axa.com', '2024-02-29 12:44:01.301000', '2024-02-29 12:44:02.556000', '2024-02-29 12:44:02.552000', 1, 1),
(4, '4', 'Safer Insurance Agency', 'Garita de Otay', 'Blvd. de las Bellas Artes', '1226-5', '664 647 5678', 'safer@insurance.com', '2024-02-29 12:45:09.830000', '2024-02-29 12:45:10.778000', '2024-02-29 12:45:10.771000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cargas_combustibles`
--

CREATE TABLE `cargas_combustibles` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `num_folio` varchar(255) DEFAULT NULL,
  `litros_cargados` decimal(10,2) DEFAULT NULL,
  `fecha` datetime(6) DEFAULT NULL,
  `kilometraje` bigint(20) DEFAULT NULL,
  `comentarios` longtext DEFAULT NULL,
  `costo_total` decimal(10,2) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cargas_combustibles`
--

INSERT INTO `cargas_combustibles` (`id`, `num`, `num_folio`, `litros_cargados`, `fecha`, `kilometraje`, `comentarios`, `costo_total`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', '17548', 10.00, '2023-03-03 00:00:00.000000', 25256, 'Sin comentarios', 222.80, '2024-02-29 13:00:31.024000', '2024-02-29 13:00:37.394000', '2024-02-29 13:00:37.386000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cargas_combustibles_vehiculo_links`
--

CREATE TABLE `cargas_combustibles_vehiculo_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `cargas_combustible_id` int(10) UNSIGNED DEFAULT NULL,
  `vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `vehiculo_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cargas_combustibles_vehiculo_links`
--

INSERT INTO `cargas_combustibles_vehiculo_links` (`id`, `cargas_combustible_id`, `vehiculo_id`, `vehiculo_order`) VALUES
(1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `estados_paradas`
--

CREATE TABLE `estados_paradas` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `estados_paradas`
--

INSERT INTO `estados_paradas` (`id`, `num`, `descripcion`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', 'Terminado', '2024-02-29 13:01:31.487000', '2024-02-29 13:01:32.354000', '2024-02-29 13:01:32.350000', 1, 1),
(2, '2', 'En proceso', '2024-02-29 13:01:48.643000', '2024-02-29 13:01:49.591000', '2024-02-29 13:01:49.587000', 1, 1),
(3, '3', 'Pendiente', '2024-02-29 13:01:58.841000', '2024-02-29 13:01:59.690000', '2024-02-29 13:01:59.686000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alternative_text` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `formats` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`formats`)),
  `hash` varchar(255) DEFAULT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `mime` varchar(255) DEFAULT NULL,
  `size` decimal(10,2) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `preview_url` varchar(255) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `provider_metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`provider_metadata`)),
  `folder_path` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files_folder_links`
--

CREATE TABLE `files_folder_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_id` int(10) UNSIGNED DEFAULT NULL,
  `folder_id` int(10) UNSIGNED DEFAULT NULL,
  `file_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files_related_morphs`
--

CREATE TABLE `files_related_morphs` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_id` int(10) UNSIGNED DEFAULT NULL,
  `related_id` int(10) UNSIGNED DEFAULT NULL,
  `related_type` varchar(255) DEFAULT NULL,
  `field` varchar(255) DEFAULT NULL,
  `order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `i18n_locale`
--

CREATE TABLE `i18n_locale` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `i18n_locale`
--

INSERT INTO `i18n_locale` (`id`, `name`, `code`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`) VALUES
(1, 'English (en)', 'en', '2024-02-28 08:46:05.060000', '2024-02-28 08:46:05.060000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `marcas_vehiculos`
--

CREATE TABLE `marcas_vehiculos` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `marcas_vehiculos`
--

INSERT INTO `marcas_vehiculos` (`id`, `num`, `nombre`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', 'Honda', '2024-02-29 11:16:59.795000', '2024-02-29 11:27:15.488000', '2024-02-29 11:27:15.484000', 1, 1),
(2, '2', 'Nissan', '2024-02-29 11:17:14.073000', '2024-02-29 11:27:23.856000', '2024-02-29 11:27:23.852000', 1, 1),
(3, '3', 'Chevrolet', '2024-02-29 11:17:30.262000', '2024-02-29 11:27:11.421000', '2024-02-29 11:27:11.419000', 1, 1),
(4, '4', 'Toyota', '2024-02-29 11:17:47.999000', '2024-02-29 11:27:28.627000', '2024-02-29 11:27:28.622000', 1, 1),
(5, '5', 'Volvo', '2024-02-29 11:18:11.811000', '2024-02-29 11:27:32.649000', '2024-02-29 11:27:32.645000', 1, 1),
(6, '6', 'Kenworth', '2024-02-29 11:18:22.294000', '2024-02-29 11:27:19.569000', '2024-02-29 11:27:19.566000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modelos_vehiculos`
--

CREATE TABLE `modelos_vehiculos` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `consumo_combustible` bigint(20) DEFAULT NULL,
  `cant_carga` bigint(20) DEFAULT NULL,
  `cap_pasajeros` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modelos_vehiculos`
--

INSERT INTO `modelos_vehiculos` (`id`, `num`, `nombre`, `consumo_combustible`, `cant_carga`, `cap_pasajeros`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', 'CB50X', 20, 200, 2, '2024-02-29 09:55:04.436000', '2024-02-29 09:55:10.436000', '2024-02-29 09:55:10.431000', 1, 1),
(2, '2', 'March', 15, 400, 4, '2024-02-29 11:19:03.217000', '2024-02-29 11:26:50.048000', '2024-02-29 11:26:50.045000', 1, 1),
(3, '3', 'Mariz', 18, 400, 4, '2024-02-29 11:21:26.956000', '2024-02-29 11:26:54.055000', '2024-02-29 11:26:54.051000', 1, 1),
(4, '4', 'Tacoma', 8, 600, 5, '2024-02-29 11:22:17.984000', '2024-02-29 11:26:58.210000', '2024-02-29 11:26:58.206000', 1, 1),
(5, '5', 'VNL', 4, 20000, 3, '2024-02-29 11:25:00.336000', '2024-02-29 11:27:02.415000', '2024-02-29 11:27:02.410000', 1, 1),
(6, '6', 'W900', 4, 20000, 3, '2024-02-29 11:25:29.038000', '2024-02-29 11:27:06.500000', '2024-02-29 11:27:06.497000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `paradas`
--

CREATE TABLE `paradas` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `dir_colonia` varchar(255) DEFAULT NULL,
  `dir_calle` varchar(255) DEFAULT NULL,
  `dir_numero` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `paradas`
--

INSERT INTO `paradas` (`id`, `num`, `nombre`, `dir_colonia`, `dir_calle`, `dir_numero`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', 'El Florido 1ra Seccion', 'El Florido 1ra y 2da Seccion', 'Blvd. Los Olivos', '11110', '2024-02-29 12:10:24.007000', '2024-02-29 13:03:51.162000', '2024-02-29 12:10:27.550000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `polizas_seguros`
--

CREATE TABLE `polizas_seguros` (
  `id` int(10) UNSIGNED NOT NULL,
  `num_poliza` varchar(255) DEFAULT NULL,
  `fecha_inicio` datetime(6) DEFAULT NULL,
  `fecha_vencimiento` datetime(6) DEFAULT NULL,
  `monto_cobertura` decimal(10,2) DEFAULT NULL,
  `costo` decimal(10,2) DEFAULT NULL,
  `frecuencia_pago` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `polizas_seguros`
--

INSERT INTO `polizas_seguros` (`id`, `num_poliza`, `fecha_inicio`, `fecha_vencimiento`, `monto_cobertura`, `costo`, `frecuencia_pago`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', '2023-01-16 00:00:00.000000', '2024-01-16 00:00:00.000000', 200000.00, 600.00, 'Mensual', '2024-02-29 13:06:48.514000', '2024-02-29 13:09:00.185000', '2024-02-29 13:08:27.216000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `polizas_seguros_aseguradora_links`
--

CREATE TABLE `polizas_seguros_aseguradora_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `poliza_seguro_id` int(10) UNSIGNED DEFAULT NULL,
  `aseguradora_id` int(10) UNSIGNED DEFAULT NULL,
  `aseguradora_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `polizas_seguros_aseguradora_links`
--

INSERT INTO `polizas_seguros_aseguradora_links` (`id`, `poliza_seguro_id`, `aseguradora_id`, `aseguradora_order`) VALUES
(1, 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `polizas_seguros_cobertura_links`
--

CREATE TABLE `polizas_seguros_cobertura_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `poliza_seguro_id` int(10) UNSIGNED DEFAULT NULL,
  `tipo_cobertura_id` int(10) UNSIGNED DEFAULT NULL,
  `tipo_cobertura_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `polizas_seguros_cobertura_links`
--

INSERT INTO `polizas_seguros_cobertura_links` (`id`, `poliza_seguro_id`, `tipo_cobertura_id`, `tipo_cobertura_order`) VALUES
(1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `polizas_seguros_vehiculo_links`
--

CREATE TABLE `polizas_seguros_vehiculo_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `poliza_seguro_id` int(10) UNSIGNED DEFAULT NULL,
  `vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `vehiculo_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `polizas_seguros_vehiculo_links`
--

INSERT INTO `polizas_seguros_vehiculo_links` (`id`, `poliza_seguro_id`, `vehiculo_id`, `vehiculo_order`) VALUES
(1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rutas`
--

CREATE TABLE `rutas` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `fecha_hora_inicio` datetime(6) DEFAULT NULL,
  `fecha_hora_fin` datetime(6) DEFAULT NULL,
  `comentarios` longtext DEFAULT NULL,
  `finalizado` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rutas`
--

INSERT INTO `rutas` (`id`, `num`, `fecha_hora_inicio`, `fecha_hora_fin`, `comentarios`, `finalizado`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', '2024-02-29 00:00:00.000000', '2024-03-01 00:00:00.000000', 'Sin comentarios', 1, '2024-02-29 12:09:55.757000', '2024-02-29 12:48:37.103000', '2024-02-29 12:10:32.627000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rutas_paradas_rutas_links`
--

CREATE TABLE `rutas_paradas_rutas_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `ruta_id` int(10) UNSIGNED DEFAULT NULL,
  `parada_id` int(10) UNSIGNED DEFAULT NULL,
  `parada_order` double UNSIGNED DEFAULT NULL,
  `ruta_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rutas_paradas_rutas_links`
--

INSERT INTO `rutas_paradas_rutas_links` (`id`, `ruta_id`, `parada_id`, `parada_order`, `ruta_order`) VALUES
(2, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `strapi_api_tokens`
--

CREATE TABLE `strapi_api_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `access_key` varchar(255) DEFAULT NULL,
  `last_used_at` datetime(6) DEFAULT NULL,
  `expires_at` datetime(6) DEFAULT NULL,
  `lifespan` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_api_token_permissions`
--

CREATE TABLE `strapi_api_token_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_api_token_permissions_token_links`
--

CREATE TABLE `strapi_api_token_permissions_token_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `api_token_permission_id` int(10) UNSIGNED DEFAULT NULL,
  `api_token_id` int(10) UNSIGNED DEFAULT NULL,
  `api_token_permission_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_core_store_settings`
--

CREATE TABLE `strapi_core_store_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `strapi_core_store_settings`
--

INSERT INTO `strapi_core_store_settings` (`id`, `key`, `value`, `type`, `environment`, `tag`) VALUES
(1, 'strapi_content_types_schema', '{\"admin::permission\":{\"collectionName\":\"admin_permissions\",\"info\":{\"name\":\"Permission\",\"description\":\"\",\"singularName\":\"permission\",\"pluralName\":\"permissions\",\"displayName\":\"Permission\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"action\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true},\"actionParameters\":{\"type\":\"json\",\"configurable\":false,\"required\":false,\"default\":{}},\"subject\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":false},\"properties\":{\"type\":\"json\",\"configurable\":false,\"required\":false,\"default\":{}},\"conditions\":{\"type\":\"json\",\"configurable\":false,\"required\":false,\"default\":[]},\"role\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"manyToOne\",\"inversedBy\":\"permissions\",\"target\":\"admin::role\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"admin_permissions\",\"info\":{\"name\":\"Permission\",\"description\":\"\",\"singularName\":\"permission\",\"pluralName\":\"permissions\",\"displayName\":\"Permission\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"action\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true},\"actionParameters\":{\"type\":\"json\",\"configurable\":false,\"required\":false,\"default\":{}},\"subject\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":false},\"properties\":{\"type\":\"json\",\"configurable\":false,\"required\":false,\"default\":{}},\"conditions\":{\"type\":\"json\",\"configurable\":false,\"required\":false,\"default\":[]},\"role\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"manyToOne\",\"inversedBy\":\"permissions\",\"target\":\"admin::role\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"permission\",\"connection\":\"default\",\"uid\":\"admin::permission\",\"plugin\":\"admin\",\"globalId\":\"AdminPermission\"},\"admin::user\":{\"collectionName\":\"admin_users\",\"info\":{\"name\":\"User\",\"description\":\"\",\"singularName\":\"user\",\"pluralName\":\"users\",\"displayName\":\"User\"},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"firstname\":{\"type\":\"string\",\"unique\":false,\"minLength\":1,\"configurable\":false,\"required\":false},\"lastname\":{\"type\":\"string\",\"unique\":false,\"minLength\":1,\"configurable\":false,\"required\":false},\"username\":{\"type\":\"string\",\"unique\":false,\"configurable\":false,\"required\":false},\"email\":{\"type\":\"email\",\"minLength\":6,\"configurable\":false,\"required\":true,\"unique\":true,\"private\":true},\"password\":{\"type\":\"password\",\"minLength\":6,\"configurable\":false,\"required\":false,\"private\":true,\"searchable\":false},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true,\"searchable\":false},\"registrationToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true,\"searchable\":false},\"isActive\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false,\"private\":true},\"roles\":{\"configurable\":false,\"private\":true,\"type\":\"relation\",\"relation\":\"manyToMany\",\"inversedBy\":\"users\",\"target\":\"admin::role\",\"collectionName\":\"strapi_users_roles\"},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false,\"private\":true},\"preferedLanguage\":{\"type\":\"string\",\"configurable\":false,\"required\":false,\"searchable\":false},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"config\":{\"attributes\":{\"resetPasswordToken\":{\"hidden\":true},\"registrationToken\":{\"hidden\":true}}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"admin_users\",\"info\":{\"name\":\"User\",\"description\":\"\",\"singularName\":\"user\",\"pluralName\":\"users\",\"displayName\":\"User\"},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"firstname\":{\"type\":\"string\",\"unique\":false,\"minLength\":1,\"configurable\":false,\"required\":false},\"lastname\":{\"type\":\"string\",\"unique\":false,\"minLength\":1,\"configurable\":false,\"required\":false},\"username\":{\"type\":\"string\",\"unique\":false,\"configurable\":false,\"required\":false},\"email\":{\"type\":\"email\",\"minLength\":6,\"configurable\":false,\"required\":true,\"unique\":true,\"private\":true},\"password\":{\"type\":\"password\",\"minLength\":6,\"configurable\":false,\"required\":false,\"private\":true,\"searchable\":false},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true,\"searchable\":false},\"registrationToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true,\"searchable\":false},\"isActive\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false,\"private\":true},\"roles\":{\"configurable\":false,\"private\":true,\"type\":\"relation\",\"relation\":\"manyToMany\",\"inversedBy\":\"users\",\"target\":\"admin::role\",\"collectionName\":\"strapi_users_roles\"},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false,\"private\":true},\"preferedLanguage\":{\"type\":\"string\",\"configurable\":false,\"required\":false,\"searchable\":false}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"user\",\"connection\":\"default\",\"uid\":\"admin::user\",\"plugin\":\"admin\",\"globalId\":\"AdminUser\"},\"admin::role\":{\"collectionName\":\"admin_roles\",\"info\":{\"name\":\"Role\",\"description\":\"\",\"singularName\":\"role\",\"pluralName\":\"roles\",\"displayName\":\"Role\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"minLength\":1,\"unique\":true,\"configurable\":false,\"required\":true},\"code\":{\"type\":\"string\",\"minLength\":1,\"unique\":true,\"configurable\":false,\"required\":true},\"description\":{\"type\":\"string\",\"configurable\":false},\"users\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"manyToMany\",\"mappedBy\":\"roles\",\"target\":\"admin::user\"},\"permissions\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"oneToMany\",\"mappedBy\":\"role\",\"target\":\"admin::permission\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"admin_roles\",\"info\":{\"name\":\"Role\",\"description\":\"\",\"singularName\":\"role\",\"pluralName\":\"roles\",\"displayName\":\"Role\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"minLength\":1,\"unique\":true,\"configurable\":false,\"required\":true},\"code\":{\"type\":\"string\",\"minLength\":1,\"unique\":true,\"configurable\":false,\"required\":true},\"description\":{\"type\":\"string\",\"configurable\":false},\"users\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"manyToMany\",\"mappedBy\":\"roles\",\"target\":\"admin::user\"},\"permissions\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"oneToMany\",\"mappedBy\":\"role\",\"target\":\"admin::permission\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"role\",\"connection\":\"default\",\"uid\":\"admin::role\",\"plugin\":\"admin\",\"globalId\":\"AdminRole\"},\"admin::api-token\":{\"collectionName\":\"strapi_api_tokens\",\"info\":{\"name\":\"Api Token\",\"singularName\":\"api-token\",\"pluralName\":\"api-tokens\",\"displayName\":\"Api Token\",\"description\":\"\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true,\"unique\":true},\"description\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":false,\"default\":\"\"},\"type\":{\"type\":\"enumeration\",\"enum\":[\"read-only\",\"full-access\",\"custom\"],\"configurable\":false,\"required\":true,\"default\":\"read-only\"},\"accessKey\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true,\"searchable\":false},\"lastUsedAt\":{\"type\":\"datetime\",\"configurable\":false,\"required\":false},\"permissions\":{\"type\":\"relation\",\"target\":\"admin::api-token-permission\",\"relation\":\"oneToMany\",\"mappedBy\":\"token\",\"configurable\":false,\"required\":false},\"expiresAt\":{\"type\":\"datetime\",\"configurable\":false,\"required\":false},\"lifespan\":{\"type\":\"biginteger\",\"configurable\":false,\"required\":false},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"strapi_api_tokens\",\"info\":{\"name\":\"Api Token\",\"singularName\":\"api-token\",\"pluralName\":\"api-tokens\",\"displayName\":\"Api Token\",\"description\":\"\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true,\"unique\":true},\"description\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":false,\"default\":\"\"},\"type\":{\"type\":\"enumeration\",\"enum\":[\"read-only\",\"full-access\",\"custom\"],\"configurable\":false,\"required\":true,\"default\":\"read-only\"},\"accessKey\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true,\"searchable\":false},\"lastUsedAt\":{\"type\":\"datetime\",\"configurable\":false,\"required\":false},\"permissions\":{\"type\":\"relation\",\"target\":\"admin::api-token-permission\",\"relation\":\"oneToMany\",\"mappedBy\":\"token\",\"configurable\":false,\"required\":false},\"expiresAt\":{\"type\":\"datetime\",\"configurable\":false,\"required\":false},\"lifespan\":{\"type\":\"biginteger\",\"configurable\":false,\"required\":false}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"api-token\",\"connection\":\"default\",\"uid\":\"admin::api-token\",\"plugin\":\"admin\",\"globalId\":\"AdminApiToken\"},\"admin::api-token-permission\":{\"collectionName\":\"strapi_api_token_permissions\",\"info\":{\"name\":\"API Token Permission\",\"description\":\"\",\"singularName\":\"api-token-permission\",\"pluralName\":\"api-token-permissions\",\"displayName\":\"API Token Permission\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"action\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true},\"token\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"manyToOne\",\"inversedBy\":\"permissions\",\"target\":\"admin::api-token\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"strapi_api_token_permissions\",\"info\":{\"name\":\"API Token Permission\",\"description\":\"\",\"singularName\":\"api-token-permission\",\"pluralName\":\"api-token-permissions\",\"displayName\":\"API Token Permission\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"action\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true},\"token\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"manyToOne\",\"inversedBy\":\"permissions\",\"target\":\"admin::api-token\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"api-token-permission\",\"connection\":\"default\",\"uid\":\"admin::api-token-permission\",\"plugin\":\"admin\",\"globalId\":\"AdminApiTokenPermission\"},\"admin::transfer-token\":{\"collectionName\":\"strapi_transfer_tokens\",\"info\":{\"name\":\"Transfer Token\",\"singularName\":\"transfer-token\",\"pluralName\":\"transfer-tokens\",\"displayName\":\"Transfer Token\",\"description\":\"\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true,\"unique\":true},\"description\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":false,\"default\":\"\"},\"accessKey\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true},\"lastUsedAt\":{\"type\":\"datetime\",\"configurable\":false,\"required\":false},\"permissions\":{\"type\":\"relation\",\"target\":\"admin::transfer-token-permission\",\"relation\":\"oneToMany\",\"mappedBy\":\"token\",\"configurable\":false,\"required\":false},\"expiresAt\":{\"type\":\"datetime\",\"configurable\":false,\"required\":false},\"lifespan\":{\"type\":\"biginteger\",\"configurable\":false,\"required\":false},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"strapi_transfer_tokens\",\"info\":{\"name\":\"Transfer Token\",\"singularName\":\"transfer-token\",\"pluralName\":\"transfer-tokens\",\"displayName\":\"Transfer Token\",\"description\":\"\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true,\"unique\":true},\"description\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":false,\"default\":\"\"},\"accessKey\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true},\"lastUsedAt\":{\"type\":\"datetime\",\"configurable\":false,\"required\":false},\"permissions\":{\"type\":\"relation\",\"target\":\"admin::transfer-token-permission\",\"relation\":\"oneToMany\",\"mappedBy\":\"token\",\"configurable\":false,\"required\":false},\"expiresAt\":{\"type\":\"datetime\",\"configurable\":false,\"required\":false},\"lifespan\":{\"type\":\"biginteger\",\"configurable\":false,\"required\":false}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"transfer-token\",\"connection\":\"default\",\"uid\":\"admin::transfer-token\",\"plugin\":\"admin\",\"globalId\":\"AdminTransferToken\"},\"admin::transfer-token-permission\":{\"collectionName\":\"strapi_transfer_token_permissions\",\"info\":{\"name\":\"Transfer Token Permission\",\"description\":\"\",\"singularName\":\"transfer-token-permission\",\"pluralName\":\"transfer-token-permissions\",\"displayName\":\"Transfer Token Permission\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"action\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true},\"token\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"manyToOne\",\"inversedBy\":\"permissions\",\"target\":\"admin::transfer-token\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"strapi_transfer_token_permissions\",\"info\":{\"name\":\"Transfer Token Permission\",\"description\":\"\",\"singularName\":\"transfer-token-permission\",\"pluralName\":\"transfer-token-permissions\",\"displayName\":\"Transfer Token Permission\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"action\":{\"type\":\"string\",\"minLength\":1,\"configurable\":false,\"required\":true},\"token\":{\"configurable\":false,\"type\":\"relation\",\"relation\":\"manyToOne\",\"inversedBy\":\"permissions\",\"target\":\"admin::transfer-token\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"transfer-token-permission\",\"connection\":\"default\",\"uid\":\"admin::transfer-token-permission\",\"plugin\":\"admin\",\"globalId\":\"AdminTransferTokenPermission\"},\"plugin::upload.file\":{\"collectionName\":\"files\",\"info\":{\"singularName\":\"file\",\"pluralName\":\"files\",\"displayName\":\"File\",\"description\":\"\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"alternativeText\":{\"type\":\"string\",\"configurable\":false},\"caption\":{\"type\":\"string\",\"configurable\":false},\"width\":{\"type\":\"integer\",\"configurable\":false},\"height\":{\"type\":\"integer\",\"configurable\":false},\"formats\":{\"type\":\"json\",\"configurable\":false},\"hash\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"ext\":{\"type\":\"string\",\"configurable\":false},\"mime\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"size\":{\"type\":\"decimal\",\"configurable\":false,\"required\":true},\"url\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"previewUrl\":{\"type\":\"string\",\"configurable\":false},\"provider\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"provider_metadata\":{\"type\":\"json\",\"configurable\":false},\"related\":{\"type\":\"relation\",\"relation\":\"morphToMany\",\"configurable\":false},\"folder\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::upload.folder\",\"inversedBy\":\"files\",\"private\":true},\"folderPath\":{\"type\":\"string\",\"min\":1,\"required\":true,\"private\":true,\"searchable\":false},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"indexes\":[{\"name\":\"upload_files_folder_path_index\",\"columns\":[\"folder_path\"],\"type\":null},{\"name\":\"upload_files_created_at_index\",\"columns\":[\"created_at\"],\"type\":null},{\"name\":\"upload_files_updated_at_index\",\"columns\":[\"updated_at\"],\"type\":null},{\"name\":\"upload_files_name_index\",\"columns\":[\"name\"],\"type\":null},{\"name\":\"upload_files_size_index\",\"columns\":[\"size\"],\"type\":null},{\"name\":\"upload_files_ext_index\",\"columns\":[\"ext\"],\"type\":null}],\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"files\",\"info\":{\"singularName\":\"file\",\"pluralName\":\"files\",\"displayName\":\"File\",\"description\":\"\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"alternativeText\":{\"type\":\"string\",\"configurable\":false},\"caption\":{\"type\":\"string\",\"configurable\":false},\"width\":{\"type\":\"integer\",\"configurable\":false},\"height\":{\"type\":\"integer\",\"configurable\":false},\"formats\":{\"type\":\"json\",\"configurable\":false},\"hash\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"ext\":{\"type\":\"string\",\"configurable\":false},\"mime\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"size\":{\"type\":\"decimal\",\"configurable\":false,\"required\":true},\"url\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"previewUrl\":{\"type\":\"string\",\"configurable\":false},\"provider\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"provider_metadata\":{\"type\":\"json\",\"configurable\":false},\"related\":{\"type\":\"relation\",\"relation\":\"morphToMany\",\"configurable\":false},\"folder\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::upload.folder\",\"inversedBy\":\"files\",\"private\":true},\"folderPath\":{\"type\":\"string\",\"min\":1,\"required\":true,\"private\":true,\"searchable\":false}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"file\",\"connection\":\"default\",\"uid\":\"plugin::upload.file\",\"plugin\":\"upload\",\"globalId\":\"UploadFile\"},\"plugin::upload.folder\":{\"collectionName\":\"upload_folders\",\"info\":{\"singularName\":\"folder\",\"pluralName\":\"folders\",\"displayName\":\"Folder\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"min\":1,\"required\":true},\"pathId\":{\"type\":\"integer\",\"unique\":true,\"required\":true},\"parent\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::upload.folder\",\"inversedBy\":\"children\"},\"children\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::upload.folder\",\"mappedBy\":\"parent\"},\"files\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::upload.file\",\"mappedBy\":\"folder\"},\"path\":{\"type\":\"string\",\"min\":1,\"required\":true},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"indexes\":[{\"name\":\"upload_folders_path_id_index\",\"columns\":[\"path_id\"],\"type\":\"unique\"},{\"name\":\"upload_folders_path_index\",\"columns\":[\"path\"],\"type\":\"unique\"}],\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"upload_folders\",\"info\":{\"singularName\":\"folder\",\"pluralName\":\"folders\",\"displayName\":\"Folder\"},\"options\":{},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"min\":1,\"required\":true},\"pathId\":{\"type\":\"integer\",\"unique\":true,\"required\":true},\"parent\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::upload.folder\",\"inversedBy\":\"children\"},\"children\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::upload.folder\",\"mappedBy\":\"parent\"},\"files\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::upload.file\",\"mappedBy\":\"folder\"},\"path\":{\"type\":\"string\",\"min\":1,\"required\":true}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"folder\",\"connection\":\"default\",\"uid\":\"plugin::upload.folder\",\"plugin\":\"upload\",\"globalId\":\"UploadFolder\"},\"plugin::content-releases.release\":{\"collectionName\":\"strapi_releases\",\"info\":{\"singularName\":\"release\",\"pluralName\":\"releases\",\"displayName\":\"Release\"},\"options\":{\"draftAndPublish\":false},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"required\":true},\"releasedAt\":{\"type\":\"datetime\"},\"scheduledAt\":{\"type\":\"datetime\"},\"timezone\":{\"type\":\"string\"},\"actions\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::content-releases.release-action\",\"mappedBy\":\"release\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"strapi_releases\",\"info\":{\"singularName\":\"release\",\"pluralName\":\"releases\",\"displayName\":\"Release\"},\"options\":{\"draftAndPublish\":false},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"required\":true},\"releasedAt\":{\"type\":\"datetime\"},\"scheduledAt\":{\"type\":\"datetime\"},\"timezone\":{\"type\":\"string\"},\"actions\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::content-releases.release-action\",\"mappedBy\":\"release\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"release\",\"connection\":\"default\",\"uid\":\"plugin::content-releases.release\",\"plugin\":\"content-releases\",\"globalId\":\"ContentReleasesRelease\"},\"plugin::content-releases.release-action\":{\"collectionName\":\"strapi_release_actions\",\"info\":{\"singularName\":\"release-action\",\"pluralName\":\"release-actions\",\"displayName\":\"Release Action\"},\"options\":{\"draftAndPublish\":false},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"type\":{\"type\":\"enumeration\",\"enum\":[\"publish\",\"unpublish\"],\"required\":true},\"entry\":{\"type\":\"relation\",\"relation\":\"morphToOne\",\"configurable\":false},\"contentType\":{\"type\":\"string\",\"required\":true},\"locale\":{\"type\":\"string\"},\"release\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::content-releases.release\",\"inversedBy\":\"actions\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"strapi_release_actions\",\"info\":{\"singularName\":\"release-action\",\"pluralName\":\"release-actions\",\"displayName\":\"Release Action\"},\"options\":{\"draftAndPublish\":false},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"type\":{\"type\":\"enumeration\",\"enum\":[\"publish\",\"unpublish\"],\"required\":true},\"entry\":{\"type\":\"relation\",\"relation\":\"morphToOne\",\"configurable\":false},\"contentType\":{\"type\":\"string\",\"required\":true},\"locale\":{\"type\":\"string\"},\"release\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::content-releases.release\",\"inversedBy\":\"actions\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"release-action\",\"connection\":\"default\",\"uid\":\"plugin::content-releases.release-action\",\"plugin\":\"content-releases\",\"globalId\":\"ContentReleasesReleaseAction\"},\"plugin::i18n.locale\":{\"info\":{\"singularName\":\"locale\",\"pluralName\":\"locales\",\"collectionName\":\"locales\",\"displayName\":\"Locale\",\"description\":\"\"},\"options\":{\"draftAndPublish\":false},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"min\":1,\"max\":50,\"configurable\":false},\"code\":{\"type\":\"string\",\"unique\":true,\"configurable\":false},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"info\":{\"singularName\":\"locale\",\"pluralName\":\"locales\",\"collectionName\":\"locales\",\"displayName\":\"Locale\",\"description\":\"\"},\"options\":{\"draftAndPublish\":false},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"min\":1,\"max\":50,\"configurable\":false},\"code\":{\"type\":\"string\",\"unique\":true,\"configurable\":false}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"locale\",\"connection\":\"default\",\"uid\":\"plugin::i18n.locale\",\"plugin\":\"i18n\",\"collectionName\":\"i18n_locale\",\"globalId\":\"I18NLocale\"},\"plugin::users-permissions.permission\":{\"collectionName\":\"up_permissions\",\"info\":{\"name\":\"permission\",\"description\":\"\",\"singularName\":\"permission\",\"pluralName\":\"permissions\",\"displayName\":\"Permission\"},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"action\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"role\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::users-permissions.role\",\"inversedBy\":\"permissions\",\"configurable\":false},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__schema__\":{\"collectionName\":\"up_permissions\",\"info\":{\"name\":\"permission\",\"description\":\"\",\"singularName\":\"permission\",\"pluralName\":\"permissions\",\"displayName\":\"Permission\"},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"action\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"role\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::users-permissions.role\",\"inversedBy\":\"permissions\",\"configurable\":false}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"permission\",\"connection\":\"default\",\"uid\":\"plugin::users-permissions.permission\",\"plugin\":\"users-permissions\",\"globalId\":\"UsersPermissionsPermission\"},\"plugin::users-permissions.role\":{\"collectionName\":\"up_roles\",\"info\":{\"name\":\"role\",\"description\":\"\",\"singularName\":\"role\",\"pluralName\":\"roles\",\"displayName\":\"Role\"},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"minLength\":3,\"required\":true,\"configurable\":false},\"description\":{\"type\":\"string\",\"configurable\":false},\"type\":{\"type\":\"string\",\"unique\":true,\"configurable\":false},\"permissions\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::users-permissions.permission\",\"mappedBy\":\"role\",\"configurable\":false},\"users\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::users-permissions.user\",\"mappedBy\":\"role\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"kind\":\"collectionType\",\"__filename__\":\"schema.json\",\"__schema__\":{\"collectionName\":\"up_roles\",\"info\":{\"name\":\"role\",\"description\":\"\",\"singularName\":\"role\",\"pluralName\":\"roles\",\"displayName\":\"Role\"},\"pluginOptions\":{\"content-manager\":{\"visible\":false},\"content-type-builder\":{\"visible\":false}},\"attributes\":{\"name\":{\"type\":\"string\",\"minLength\":3,\"required\":true,\"configurable\":false},\"description\":{\"type\":\"string\",\"configurable\":false},\"type\":{\"type\":\"string\",\"unique\":true,\"configurable\":false},\"permissions\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::users-permissions.permission\",\"mappedBy\":\"role\",\"configurable\":false},\"users\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"plugin::users-permissions.user\",\"mappedBy\":\"role\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"role\",\"connection\":\"default\",\"uid\":\"plugin::users-permissions.role\",\"plugin\":\"users-permissions\",\"globalId\":\"UsersPermissionsRole\"},\"plugin::users-permissions.user\":{\"collectionName\":\"up_users\",\"info\":{\"name\":\"user\",\"description\":\"\",\"singularName\":\"user\",\"pluralName\":\"users\",\"displayName\":\"User\"},\"options\":{\"draftAndPublish\":true},\"attributes\":{\"username\":{\"type\":\"string\",\"minLength\":3,\"unique\":true,\"configurable\":false,\"required\":true},\"provider\":{\"type\":\"string\",\"configurable\":false},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true,\"searchable\":false},\"confirmationToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true,\"searchable\":false},\"confirmed\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"num\":{\"type\":\"uid\",\"required\":true},\"nombrePila\":{\"type\":\"string\",\"required\":true},\"apPaterno\":{\"type\":\"string\",\"required\":true},\"apMaterno\":{\"type\":\"string\",\"required\":true},\"numTel\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"email\":{\"type\":\"email\",\"minLength\":6,\"required\":true},\"password\":{\"type\":\"password\",\"minLength\":6,\"private\":true,\"searchable\":false},\"role\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::users-permissions.role\",\"inversedBy\":\"users\"},\"usuarios_vehiculos\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"api::vehiculo.vehiculo\",\"inversedBy\":\"vehiculos_usuarios\"},\"rutas_users\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"api::ruta.ruta\",\"inversedBy\":\"users_rutas\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"config\":{\"attributes\":{\"resetPasswordToken\":{\"hidden\":true},\"confirmationToken\":{\"hidden\":true},\"provider\":{\"hidden\":true}}},\"kind\":\"collectionType\",\"__filename__\":\"schema.json\",\"__schema__\":{\"collectionName\":\"up_users\",\"info\":{\"name\":\"user\",\"description\":\"\",\"singularName\":\"user\",\"pluralName\":\"users\",\"displayName\":\"User\"},\"options\":{\"draftAndPublish\":true},\"attributes\":{\"username\":{\"type\":\"string\",\"minLength\":3,\"unique\":true,\"configurable\":false,\"required\":true},\"provider\":{\"type\":\"string\",\"configurable\":false},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true,\"searchable\":false},\"confirmationToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true,\"searchable\":false},\"confirmed\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"num\":{\"type\":\"uid\",\"required\":true},\"nombrePila\":{\"type\":\"string\",\"required\":true},\"apPaterno\":{\"type\":\"string\",\"required\":true},\"apMaterno\":{\"type\":\"string\",\"required\":true},\"numTel\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"email\":{\"type\":\"email\",\"minLength\":6,\"required\":true},\"password\":{\"type\":\"password\",\"minLength\":6,\"private\":true,\"searchable\":false},\"role\":{\"type\":\"relation\",\"relation\":\"manyToOne\",\"target\":\"plugin::users-permissions.role\",\"inversedBy\":\"users\"},\"usuarios_vehiculos\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"api::vehiculo.vehiculo\",\"inversedBy\":\"vehiculos_usuarios\"},\"rutas_users\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"api::ruta.ruta\",\"inversedBy\":\"users_rutas\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"user\",\"connection\":\"default\",\"uid\":\"plugin::users-permissions.user\",\"plugin\":\"users-permissions\",\"globalId\":\"UsersPermissionsUser\"},\"api::aseguradora.aseguradora\":{\"kind\":\"collectionType\",\"collectionName\":\"aseguradoras\",\"info\":{\"singularName\":\"aseguradora\",\"pluralName\":\"aseguradoras\",\"displayName\":\"aseguradoras\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"unique\":true,\"required\":true},\"dirColonia\":{\"type\":\"string\",\"required\":true},\"dirCalle\":{\"type\":\"string\",\"required\":true},\"dirNumero\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"numTel\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"correo\":{\"type\":\"email\",\"required\":true,\"unique\":true},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"aseguradoras\",\"info\":{\"singularName\":\"aseguradora\",\"pluralName\":\"aseguradoras\",\"displayName\":\"aseguradoras\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"unique\":true,\"required\":true},\"dirColonia\":{\"type\":\"string\",\"required\":true},\"dirCalle\":{\"type\":\"string\",\"required\":true},\"dirNumero\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"numTel\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"correo\":{\"type\":\"email\",\"required\":true,\"unique\":true}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"aseguradora\",\"connection\":\"default\",\"uid\":\"api::aseguradora.aseguradora\",\"apiName\":\"aseguradora\",\"globalId\":\"Aseguradora\",\"actions\":{},\"lifecycles\":{}},\"api::cargas-combustible.cargas-combustible\":{\"kind\":\"collectionType\",\"collectionName\":\"cargas_combustibles\",\"info\":{\"singularName\":\"cargas-combustible\",\"pluralName\":\"cargas-combustibles\",\"displayName\":\"cargasCombustible\",\"description\":\"\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"numFolio\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"litrosCargados\":{\"type\":\"decimal\",\"required\":true},\"fecha\":{\"type\":\"datetime\",\"required\":true},\"kilometraje\":{\"type\":\"biginteger\",\"required\":true},\"comentarios\":{\"type\":\"text\",\"default\":\"Sin comentarios\"},\"costoTotal\":{\"type\":\"decimal\",\"required\":true},\"vehiculo\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::vehiculo.vehiculo\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"cargas_combustibles\",\"info\":{\"singularName\":\"cargas-combustible\",\"pluralName\":\"cargas-combustibles\",\"displayName\":\"cargasCombustible\",\"description\":\"\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"numFolio\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"litrosCargados\":{\"type\":\"decimal\",\"required\":true},\"fecha\":{\"type\":\"datetime\",\"required\":true},\"kilometraje\":{\"type\":\"biginteger\",\"required\":true},\"comentarios\":{\"type\":\"text\",\"default\":\"Sin comentarios\"},\"costoTotal\":{\"type\":\"decimal\",\"required\":true},\"vehiculo\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::vehiculo.vehiculo\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"cargas-combustible\",\"connection\":\"default\",\"uid\":\"api::cargas-combustible.cargas-combustible\",\"apiName\":\"cargas-combustible\",\"globalId\":\"CargasCombustible\",\"actions\":{},\"lifecycles\":{}},\"api::estado-parada.estado-parada\":{\"kind\":\"collectionType\",\"collectionName\":\"estados_paradas\",\"info\":{\"singularName\":\"estado-parada\",\"pluralName\":\"estados-paradas\",\"displayName\":\"estadosParadas\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"descripcion\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"estados_paradas\",\"info\":{\"singularName\":\"estado-parada\",\"pluralName\":\"estados-paradas\",\"displayName\":\"estadosParadas\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"descripcion\":{\"type\":\"string\",\"required\":true,\"unique\":true}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"estado-parada\",\"connection\":\"default\",\"uid\":\"api::estado-parada.estado-parada\",\"apiName\":\"estado-parada\",\"globalId\":\"EstadoParada\",\"actions\":{},\"lifecycles\":{}},\"api::marca-vehiculo.marca-vehiculo\":{\"kind\":\"collectionType\",\"collectionName\":\"marcas_vehiculos\",\"info\":{\"singularName\":\"marca-vehiculo\",\"pluralName\":\"marcas-vehiculos\",\"displayName\":\"marcasVehiculos\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"unique\":true,\"required\":true},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"marcas_vehiculos\",\"info\":{\"singularName\":\"marca-vehiculo\",\"pluralName\":\"marcas-vehiculos\",\"displayName\":\"marcasVehiculos\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"unique\":true,\"required\":true}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"marca-vehiculo\",\"connection\":\"default\",\"uid\":\"api::marca-vehiculo.marca-vehiculo\",\"apiName\":\"marca-vehiculo\",\"globalId\":\"MarcaVehiculo\",\"actions\":{},\"lifecycles\":{}},\"api::modelo-vehiculo.modelo-vehiculo\":{\"kind\":\"collectionType\",\"collectionName\":\"modelos_vehiculos\",\"info\":{\"singularName\":\"modelo-vehiculo\",\"pluralName\":\"modelos-vehiculos\",\"displayName\":\"modelosVehiculos\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"consumoCombustible\":{\"type\":\"biginteger\",\"required\":true},\"cantCarga\":{\"type\":\"biginteger\",\"required\":true},\"capPasajeros\":{\"required\":true,\"type\":\"biginteger\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"modelos_vehiculos\",\"info\":{\"singularName\":\"modelo-vehiculo\",\"pluralName\":\"modelos-vehiculos\",\"displayName\":\"modelosVehiculos\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"consumoCombustible\":{\"type\":\"biginteger\",\"required\":true},\"cantCarga\":{\"type\":\"biginteger\",\"required\":true},\"capPasajeros\":{\"required\":true,\"type\":\"biginteger\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"modelo-vehiculo\",\"connection\":\"default\",\"uid\":\"api::modelo-vehiculo.modelo-vehiculo\",\"apiName\":\"modelo-vehiculo\",\"globalId\":\"ModeloVehiculo\",\"actions\":{},\"lifecycles\":{}},\"api::parada.parada\":{\"kind\":\"collectionType\",\"collectionName\":\"paradas\",\"info\":{\"singularName\":\"parada\",\"pluralName\":\"paradas\",\"displayName\":\"paradas\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"required\":true},\"dirColonia\":{\"type\":\"string\",\"required\":true},\"dirCalle\":{\"type\":\"string\",\"required\":true},\"dirNumero\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"rutas_paradas\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"api::ruta.ruta\",\"mappedBy\":\"paradas_rutas\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"paradas\",\"info\":{\"singularName\":\"parada\",\"pluralName\":\"paradas\",\"displayName\":\"paradas\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"required\":true},\"dirColonia\":{\"type\":\"string\",\"required\":true},\"dirCalle\":{\"type\":\"string\",\"required\":true},\"dirNumero\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"rutas_paradas\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"api::ruta.ruta\",\"mappedBy\":\"paradas_rutas\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"parada\",\"connection\":\"default\",\"uid\":\"api::parada.parada\",\"apiName\":\"parada\",\"globalId\":\"Parada\",\"actions\":{},\"lifecycles\":{}},\"api::poliza-seguro.poliza-seguro\":{\"kind\":\"collectionType\",\"collectionName\":\"polizas_seguros\",\"info\":{\"singularName\":\"poliza-seguro\",\"pluralName\":\"polizas-seguros\",\"displayName\":\"polizasSeguros\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"numPoliza\":{\"type\":\"uid\",\"required\":true},\"fechaInicio\":{\"type\":\"datetime\",\"required\":true},\"fechaVencimiento\":{\"type\":\"datetime\",\"required\":true},\"montoCobertura\":{\"type\":\"decimal\",\"required\":true},\"costo\":{\"type\":\"decimal\",\"required\":true},\"frecuenciaPago\":{\"type\":\"string\",\"required\":true},\"vehiculo\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::vehiculo.vehiculo\"},\"aseguradora\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::aseguradora.aseguradora\"},\"cobertura\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::tipo-cobertura.tipo-cobertura\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"polizas_seguros\",\"info\":{\"singularName\":\"poliza-seguro\",\"pluralName\":\"polizas-seguros\",\"displayName\":\"polizasSeguros\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"numPoliza\":{\"type\":\"uid\",\"required\":true},\"fechaInicio\":{\"type\":\"datetime\",\"required\":true},\"fechaVencimiento\":{\"type\":\"datetime\",\"required\":true},\"montoCobertura\":{\"type\":\"decimal\",\"required\":true},\"costo\":{\"type\":\"decimal\",\"required\":true},\"frecuenciaPago\":{\"type\":\"string\",\"required\":true},\"vehiculo\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::vehiculo.vehiculo\"},\"aseguradora\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::aseguradora.aseguradora\"},\"cobertura\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::tipo-cobertura.tipo-cobertura\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"poliza-seguro\",\"connection\":\"default\",\"uid\":\"api::poliza-seguro.poliza-seguro\",\"apiName\":\"poliza-seguro\",\"globalId\":\"PolizaSeguro\",\"actions\":{},\"lifecycles\":{}},\"api::ruta.ruta\":{\"kind\":\"collectionType\",\"collectionName\":\"rutas\",\"info\":{\"singularName\":\"ruta\",\"pluralName\":\"rutas\",\"displayName\":\"rutas\",\"description\":\"\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"fechaHoraInicio\":{\"type\":\"datetime\",\"required\":true},\"fechaHoraFin\":{\"type\":\"datetime\",\"required\":true},\"comentarios\":{\"type\":\"text\",\"default\":\"Sin comentarios\",\"required\":false},\"finalizado\":{\"type\":\"boolean\",\"default\":false,\"required\":true},\"paradas_rutas\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"api::parada.parada\",\"inversedBy\":\"rutas_paradas\"},\"users_rutas\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"plugin::users-permissions.user\",\"mappedBy\":\"rutas_users\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"rutas\",\"info\":{\"singularName\":\"ruta\",\"pluralName\":\"rutas\",\"displayName\":\"rutas\",\"description\":\"\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"fechaHoraInicio\":{\"type\":\"datetime\",\"required\":true},\"fechaHoraFin\":{\"type\":\"datetime\",\"required\":true},\"comentarios\":{\"type\":\"text\",\"default\":\"Sin comentarios\",\"required\":false},\"finalizado\":{\"type\":\"boolean\",\"default\":false,\"required\":true},\"paradas_rutas\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"api::parada.parada\",\"inversedBy\":\"rutas_paradas\"},\"users_rutas\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"plugin::users-permissions.user\",\"mappedBy\":\"rutas_users\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"ruta\",\"connection\":\"default\",\"uid\":\"api::ruta.ruta\",\"apiName\":\"ruta\",\"globalId\":\"Ruta\",\"actions\":{},\"lifecycles\":{}},\"api::tipo-cobertura.tipo-cobertura\":{\"kind\":\"collectionType\",\"collectionName\":\"tipos_coberturas\",\"info\":{\"singularName\":\"tipo-cobertura\",\"pluralName\":\"tipos-coberturas\",\"displayName\":\"tiposCoberturas\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"unique\":true,\"required\":true},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"tipos_coberturas\",\"info\":{\"singularName\":\"tipo-cobertura\",\"pluralName\":\"tipos-coberturas\",\"displayName\":\"tiposCoberturas\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"unique\":true,\"required\":true}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"tipo-cobertura\",\"connection\":\"default\",\"uid\":\"api::tipo-cobertura.tipo-cobertura\",\"apiName\":\"tipo-cobertura\",\"globalId\":\"TipoCobertura\",\"actions\":{},\"lifecycles\":{}},\"api::tipo-vehiculo.tipo-vehiculo\":{\"kind\":\"collectionType\",\"collectionName\":\"tipos_vehiculos\",\"info\":{\"singularName\":\"tipo-vehiculo\",\"pluralName\":\"tipos-vehiculos\",\"displayName\":\"tiposVehiculos\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"tipos_vehiculos\",\"info\":{\"singularName\":\"tipo-vehiculo\",\"pluralName\":\"tipos-vehiculos\",\"displayName\":\"tiposVehiculos\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"num\":{\"type\":\"uid\",\"required\":true},\"nombre\":{\"type\":\"string\",\"required\":true,\"unique\":true}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"tipo-vehiculo\",\"connection\":\"default\",\"uid\":\"api::tipo-vehiculo.tipo-vehiculo\",\"apiName\":\"tipo-vehiculo\",\"globalId\":\"TipoVehiculo\",\"actions\":{},\"lifecycles\":{}},\"api::vehiculo.vehiculo\":{\"kind\":\"collectionType\",\"collectionName\":\"vehiculos\",\"info\":{\"singularName\":\"vehiculo\",\"pluralName\":\"vehiculos\",\"displayName\":\"Vehiculos\",\"description\":\"\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"numSerie\":{\"type\":\"uid\",\"required\":true},\"placas\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"kilometraje\":{\"type\":\"biginteger\",\"required\":true},\"fechaAdquisicion\":{\"type\":\"datetime\",\"required\":true},\"usaDiesel\":{\"type\":\"boolean\",\"required\":true},\"modelo\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::modelo-vehiculo.modelo-vehiculo\"},\"marca\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::marca-vehiculo.marca-vehiculo\"},\"tipo\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::tipo-vehiculo.tipo-vehiculo\"},\"vehiculos_usuarios\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"plugin::users-permissions.user\",\"mappedBy\":\"usuarios_vehiculos\"},\"createdAt\":{\"type\":\"datetime\"},\"updatedAt\":{\"type\":\"datetime\"},\"publishedAt\":{\"type\":\"datetime\",\"configurable\":false,\"writable\":true,\"visible\":false},\"createdBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true},\"updatedBy\":{\"type\":\"relation\",\"relation\":\"oneToOne\",\"target\":\"admin::user\",\"configurable\":false,\"writable\":false,\"visible\":false,\"useJoinTable\":false,\"private\":true}},\"__schema__\":{\"collectionName\":\"vehiculos\",\"info\":{\"singularName\":\"vehiculo\",\"pluralName\":\"vehiculos\",\"displayName\":\"Vehiculos\",\"description\":\"\"},\"options\":{\"draftAndPublish\":true},\"pluginOptions\":{},\"attributes\":{\"numSerie\":{\"type\":\"uid\",\"required\":true},\"placas\":{\"type\":\"string\",\"required\":true,\"unique\":true},\"kilometraje\":{\"type\":\"biginteger\",\"required\":true},\"fechaAdquisicion\":{\"type\":\"datetime\",\"required\":true},\"usaDiesel\":{\"type\":\"boolean\",\"required\":true},\"modelo\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::modelo-vehiculo.modelo-vehiculo\"},\"marca\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::marca-vehiculo.marca-vehiculo\"},\"tipo\":{\"type\":\"relation\",\"relation\":\"oneToMany\",\"target\":\"api::tipo-vehiculo.tipo-vehiculo\"},\"vehiculos_usuarios\":{\"type\":\"relation\",\"relation\":\"manyToMany\",\"target\":\"plugin::users-permissions.user\",\"mappedBy\":\"usuarios_vehiculos\"}},\"kind\":\"collectionType\"},\"modelType\":\"contentType\",\"modelName\":\"vehiculo\",\"connection\":\"default\",\"uid\":\"api::vehiculo.vehiculo\",\"apiName\":\"vehiculo\",\"globalId\":\"Vehiculo\",\"actions\":{},\"lifecycles\":{}}}', 'object', NULL, NULL);
INSERT INTO `strapi_core_store_settings` (`id`, `key`, `value`, `type`, `environment`, `tag`) VALUES
(2, 'plugin_content_manager_configuration_content_types::admin::permission', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"action\",\"defaultSortBy\":\"action\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"action\":{\"edit\":{\"label\":\"action\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"action\",\"searchable\":true,\"sortable\":true}},\"actionParameters\":{\"edit\":{\"label\":\"actionParameters\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"actionParameters\",\"searchable\":false,\"sortable\":false}},\"subject\":{\"edit\":{\"label\":\"subject\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"subject\",\"searchable\":true,\"sortable\":true}},\"properties\":{\"edit\":{\"label\":\"properties\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"properties\",\"searchable\":false,\"sortable\":false}},\"conditions\":{\"edit\":{\"label\":\"conditions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"conditions\",\"searchable\":false,\"sortable\":false}},\"role\":{\"edit\":{\"label\":\"role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"role\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"action\",\"subject\",\"role\"],\"edit\":[[{\"name\":\"action\",\"size\":6}],[{\"name\":\"actionParameters\",\"size\":12}],[{\"name\":\"subject\",\"size\":6}],[{\"name\":\"properties\",\"size\":12}],[{\"name\":\"conditions\",\"size\":12}],[{\"name\":\"role\",\"size\":6}]]},\"uid\":\"admin::permission\"}', 'object', NULL, NULL),
(3, 'plugin_content_manager_configuration_content_types::admin::api-token-permission', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"action\",\"defaultSortBy\":\"action\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"action\":{\"edit\":{\"label\":\"action\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"action\",\"searchable\":true,\"sortable\":true}},\"token\":{\"edit\":{\"label\":\"token\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"token\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"action\",\"token\",\"createdAt\"],\"edit\":[[{\"name\":\"action\",\"size\":6},{\"name\":\"token\",\"size\":6}]]},\"uid\":\"admin::api-token-permission\"}', 'object', NULL, NULL),
(4, 'plugin_content_manager_configuration_content_types::admin::transfer-token', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"name\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"description\",\"searchable\":true,\"sortable\":true}},\"accessKey\":{\"edit\":{\"label\":\"accessKey\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"accessKey\",\"searchable\":true,\"sortable\":true}},\"lastUsedAt\":{\"edit\":{\"label\":\"lastUsedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"lastUsedAt\",\"searchable\":true,\"sortable\":true}},\"permissions\":{\"edit\":{\"label\":\"permissions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"action\"},\"list\":{\"label\":\"permissions\",\"searchable\":false,\"sortable\":false}},\"expiresAt\":{\"edit\":{\"label\":\"expiresAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"expiresAt\",\"searchable\":true,\"sortable\":true}},\"lifespan\":{\"edit\":{\"label\":\"lifespan\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"lifespan\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"description\",\"accessKey\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"description\",\"size\":6}],[{\"name\":\"accessKey\",\"size\":6},{\"name\":\"lastUsedAt\",\"size\":6}],[{\"name\":\"permissions\",\"size\":6},{\"name\":\"expiresAt\",\"size\":6}],[{\"name\":\"lifespan\",\"size\":4}]]},\"uid\":\"admin::transfer-token\"}', 'object', NULL, NULL),
(5, 'plugin_content_manager_configuration_content_types::admin::user', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"firstname\",\"defaultSortBy\":\"firstname\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"firstname\":{\"edit\":{\"label\":\"firstname\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"firstname\",\"searchable\":true,\"sortable\":true}},\"lastname\":{\"edit\":{\"label\":\"lastname\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"lastname\",\"searchable\":true,\"sortable\":true}},\"username\":{\"edit\":{\"label\":\"username\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"username\",\"searchable\":true,\"sortable\":true}},\"email\":{\"edit\":{\"label\":\"email\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"email\",\"searchable\":true,\"sortable\":true}},\"password\":{\"edit\":{\"label\":\"password\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"password\",\"searchable\":true,\"sortable\":true}},\"resetPasswordToken\":{\"edit\":{\"label\":\"resetPasswordToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"resetPasswordToken\",\"searchable\":true,\"sortable\":true}},\"registrationToken\":{\"edit\":{\"label\":\"registrationToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"registrationToken\",\"searchable\":true,\"sortable\":true}},\"isActive\":{\"edit\":{\"label\":\"isActive\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"isActive\",\"searchable\":true,\"sortable\":true}},\"roles\":{\"edit\":{\"label\":\"roles\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"roles\",\"searchable\":false,\"sortable\":false}},\"blocked\":{\"edit\":{\"label\":\"blocked\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"blocked\",\"searchable\":true,\"sortable\":true}},\"preferedLanguage\":{\"edit\":{\"label\":\"preferedLanguage\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"preferedLanguage\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"firstname\",\"lastname\",\"username\"],\"edit\":[[{\"name\":\"firstname\",\"size\":6},{\"name\":\"lastname\",\"size\":6}],[{\"name\":\"username\",\"size\":6},{\"name\":\"email\",\"size\":6}],[{\"name\":\"password\",\"size\":6},{\"name\":\"isActive\",\"size\":4}],[{\"name\":\"roles\",\"size\":6},{\"name\":\"blocked\",\"size\":4}],[{\"name\":\"preferedLanguage\",\"size\":6}]]},\"uid\":\"admin::user\"}', 'object', NULL, NULL),
(6, 'plugin_content_manager_configuration_content_types::plugin::upload.file', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"name\",\"searchable\":true,\"sortable\":true}},\"alternativeText\":{\"edit\":{\"label\":\"alternativeText\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"alternativeText\",\"searchable\":true,\"sortable\":true}},\"caption\":{\"edit\":{\"label\":\"caption\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"caption\",\"searchable\":true,\"sortable\":true}},\"width\":{\"edit\":{\"label\":\"width\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"width\",\"searchable\":true,\"sortable\":true}},\"height\":{\"edit\":{\"label\":\"height\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"height\",\"searchable\":true,\"sortable\":true}},\"formats\":{\"edit\":{\"label\":\"formats\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"formats\",\"searchable\":false,\"sortable\":false}},\"hash\":{\"edit\":{\"label\":\"hash\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"hash\",\"searchable\":true,\"sortable\":true}},\"ext\":{\"edit\":{\"label\":\"ext\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"ext\",\"searchable\":true,\"sortable\":true}},\"mime\":{\"edit\":{\"label\":\"mime\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"mime\",\"searchable\":true,\"sortable\":true}},\"size\":{\"edit\":{\"label\":\"size\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"size\",\"searchable\":true,\"sortable\":true}},\"url\":{\"edit\":{\"label\":\"url\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"url\",\"searchable\":true,\"sortable\":true}},\"previewUrl\":{\"edit\":{\"label\":\"previewUrl\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"previewUrl\",\"searchable\":true,\"sortable\":true}},\"provider\":{\"edit\":{\"label\":\"provider\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"provider\",\"searchable\":true,\"sortable\":true}},\"provider_metadata\":{\"edit\":{\"label\":\"provider_metadata\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"provider_metadata\",\"searchable\":false,\"sortable\":false}},\"folder\":{\"edit\":{\"label\":\"folder\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"folder\",\"searchable\":true,\"sortable\":true}},\"folderPath\":{\"edit\":{\"label\":\"folderPath\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"folderPath\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"alternativeText\",\"caption\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"alternativeText\",\"size\":6}],[{\"name\":\"caption\",\"size\":6},{\"name\":\"width\",\"size\":4}],[{\"name\":\"height\",\"size\":4}],[{\"name\":\"formats\",\"size\":12}],[{\"name\":\"hash\",\"size\":6},{\"name\":\"ext\",\"size\":6}],[{\"name\":\"mime\",\"size\":6},{\"name\":\"size\",\"size\":4}],[{\"name\":\"url\",\"size\":6},{\"name\":\"previewUrl\",\"size\":6}],[{\"name\":\"provider\",\"size\":6}],[{\"name\":\"provider_metadata\",\"size\":12}],[{\"name\":\"folder\",\"size\":6},{\"name\":\"folderPath\",\"size\":6}]]},\"uid\":\"plugin::upload.file\"}', 'object', NULL, NULL),
(7, 'plugin_content_manager_configuration_content_types::plugin::content-releases.release', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"name\",\"searchable\":true,\"sortable\":true}},\"releasedAt\":{\"edit\":{\"label\":\"releasedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"releasedAt\",\"searchable\":true,\"sortable\":true}},\"scheduledAt\":{\"edit\":{\"label\":\"scheduledAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"scheduledAt\",\"searchable\":true,\"sortable\":true}},\"timezone\":{\"edit\":{\"label\":\"timezone\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"timezone\",\"searchable\":true,\"sortable\":true}},\"actions\":{\"edit\":{\"label\":\"actions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"contentType\"},\"list\":{\"label\":\"actions\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"releasedAt\",\"scheduledAt\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"releasedAt\",\"size\":6}],[{\"name\":\"scheduledAt\",\"size\":6},{\"name\":\"timezone\",\"size\":6}],[{\"name\":\"actions\",\"size\":6}]]},\"uid\":\"plugin::content-releases.release\"}', 'object', NULL, NULL),
(8, 'plugin_content_manager_configuration_content_types::plugin::content-releases.release-action', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"contentType\",\"defaultSortBy\":\"contentType\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"type\",\"searchable\":true,\"sortable\":true}},\"contentType\":{\"edit\":{\"label\":\"contentType\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"contentType\",\"searchable\":true,\"sortable\":true}},\"locale\":{\"edit\":{\"label\":\"locale\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"locale\",\"searchable\":true,\"sortable\":true}},\"release\":{\"edit\":{\"label\":\"release\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"release\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"type\",\"contentType\",\"locale\"],\"edit\":[[{\"name\":\"type\",\"size\":6},{\"name\":\"contentType\",\"size\":6}],[{\"name\":\"locale\",\"size\":6},{\"name\":\"release\",\"size\":6}]]},\"uid\":\"plugin::content-releases.release-action\"}', 'object', NULL, NULL),
(9, 'plugin_content_manager_configuration_content_types::plugin::users-permissions.permission', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"action\",\"defaultSortBy\":\"action\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"action\":{\"edit\":{\"label\":\"action\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"action\",\"searchable\":true,\"sortable\":true}},\"role\":{\"edit\":{\"label\":\"role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"role\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"action\",\"role\",\"createdAt\"],\"edit\":[[{\"name\":\"action\",\"size\":6},{\"name\":\"role\",\"size\":6}]]},\"uid\":\"plugin::users-permissions.permission\"}', 'object', NULL, NULL),
(10, 'plugin_content_manager_configuration_content_types::plugin::i18n.locale', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"name\",\"searchable\":true,\"sortable\":true}},\"code\":{\"edit\":{\"label\":\"code\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"code\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"code\",\"createdAt\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"code\",\"size\":6}]]},\"uid\":\"plugin::i18n.locale\"}', 'object', NULL, NULL),
(11, 'plugin_content_manager_configuration_content_types::plugin::upload.folder', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"name\",\"searchable\":true,\"sortable\":true}},\"pathId\":{\"edit\":{\"label\":\"pathId\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"pathId\",\"searchable\":true,\"sortable\":true}},\"parent\":{\"edit\":{\"label\":\"parent\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"parent\",\"searchable\":true,\"sortable\":true}},\"children\":{\"edit\":{\"label\":\"children\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"children\",\"searchable\":false,\"sortable\":false}},\"files\":{\"edit\":{\"label\":\"files\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"files\",\"searchable\":false,\"sortable\":false}},\"path\":{\"edit\":{\"label\":\"path\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"path\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"pathId\",\"parent\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"pathId\",\"size\":4}],[{\"name\":\"parent\",\"size\":6},{\"name\":\"children\",\"size\":6}],[{\"name\":\"files\",\"size\":6},{\"name\":\"path\",\"size\":6}]]},\"uid\":\"plugin::upload.folder\"}', 'object', NULL, NULL),
(12, 'plugin_content_manager_configuration_content_types::admin::transfer-token-permission', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"action\",\"defaultSortBy\":\"action\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"action\":{\"edit\":{\"label\":\"action\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"action\",\"searchable\":true,\"sortable\":true}},\"token\":{\"edit\":{\"label\":\"token\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"token\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"action\",\"token\",\"createdAt\"],\"edit\":[[{\"name\":\"action\",\"size\":6},{\"name\":\"token\",\"size\":6}]]},\"uid\":\"admin::transfer-token-permission\"}', 'object', NULL, NULL),
(13, 'plugin_content_manager_configuration_content_types::admin::role', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"name\",\"searchable\":true,\"sortable\":true}},\"code\":{\"edit\":{\"label\":\"code\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"code\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"description\",\"searchable\":true,\"sortable\":true}},\"users\":{\"edit\":{\"label\":\"users\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"users\",\"searchable\":false,\"sortable\":false}},\"permissions\":{\"edit\":{\"label\":\"permissions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"action\"},\"list\":{\"label\":\"permissions\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"code\",\"description\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"code\",\"size\":6}],[{\"name\":\"description\",\"size\":6},{\"name\":\"users\",\"size\":6}],[{\"name\":\"permissions\",\"size\":6}]]},\"uid\":\"admin::role\"}', 'object', NULL, NULL),
(14, 'plugin_content_manager_configuration_content_types::admin::api-token', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"name\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"description\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"type\",\"searchable\":true,\"sortable\":true}},\"accessKey\":{\"edit\":{\"label\":\"accessKey\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"accessKey\",\"searchable\":true,\"sortable\":true}},\"lastUsedAt\":{\"edit\":{\"label\":\"lastUsedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"lastUsedAt\",\"searchable\":true,\"sortable\":true}},\"permissions\":{\"edit\":{\"label\":\"permissions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"action\"},\"list\":{\"label\":\"permissions\",\"searchable\":false,\"sortable\":false}},\"expiresAt\":{\"edit\":{\"label\":\"expiresAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"expiresAt\",\"searchable\":true,\"sortable\":true}},\"lifespan\":{\"edit\":{\"label\":\"lifespan\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"lifespan\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"description\",\"type\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"description\",\"size\":6}],[{\"name\":\"type\",\"size\":6},{\"name\":\"accessKey\",\"size\":6}],[{\"name\":\"lastUsedAt\",\"size\":6},{\"name\":\"permissions\",\"size\":6}],[{\"name\":\"expiresAt\",\"size\":6},{\"name\":\"lifespan\",\"size\":4}]]},\"uid\":\"admin::api-token\"}', 'object', NULL, NULL),
(15, 'plugin_content_manager_configuration_content_types::plugin::users-permissions.role', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"name\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"description\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"type\",\"searchable\":true,\"sortable\":true}},\"permissions\":{\"edit\":{\"label\":\"permissions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"action\"},\"list\":{\"label\":\"permissions\",\"searchable\":false,\"sortable\":false}},\"users\":{\"edit\":{\"label\":\"users\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"username\"},\"list\":{\"label\":\"users\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"description\",\"type\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"description\",\"size\":6}],[{\"name\":\"type\",\"size\":6},{\"name\":\"permissions\",\"size\":6}],[{\"name\":\"users\",\"size\":6}]]},\"uid\":\"plugin::users-permissions.role\"}', 'object', NULL, NULL),
(16, 'plugin_content_manager_configuration_content_types::plugin::users-permissions.user', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"username\",\"defaultSortBy\":\"username\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"username\":{\"edit\":{\"label\":\"username\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"username\",\"searchable\":true,\"sortable\":true}},\"provider\":{\"edit\":{\"label\":\"provider\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"provider\",\"searchable\":true,\"sortable\":true}},\"resetPasswordToken\":{\"edit\":{\"label\":\"resetPasswordToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"resetPasswordToken\",\"searchable\":true,\"sortable\":true}},\"confirmationToken\":{\"edit\":{\"label\":\"confirmationToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"confirmationToken\",\"searchable\":true,\"sortable\":true}},\"confirmed\":{\"edit\":{\"label\":\"confirmed\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"confirmed\",\"searchable\":true,\"sortable\":true}},\"blocked\":{\"edit\":{\"label\":\"blocked\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"blocked\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"nombrePila\":{\"edit\":{\"label\":\"nombrePila\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"nombrePila\",\"searchable\":true,\"sortable\":true}},\"apPaterno\":{\"edit\":{\"label\":\"apPaterno\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"apPaterno\",\"searchable\":true,\"sortable\":true}},\"apMaterno\":{\"edit\":{\"label\":\"apMaterno\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"apMaterno\",\"searchable\":true,\"sortable\":true}},\"numTel\":{\"edit\":{\"label\":\"numTel\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"numTel\",\"searchable\":true,\"sortable\":true}},\"email\":{\"edit\":{\"label\":\"email\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"email\",\"searchable\":true,\"sortable\":true}},\"password\":{\"edit\":{\"label\":\"password\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"password\",\"searchable\":true,\"sortable\":true}},\"role\":{\"edit\":{\"label\":\"role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"role\",\"searchable\":true,\"sortable\":true}},\"usuarios_vehiculos\":{\"edit\":{\"label\":\"usuarios_vehiculos\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"placas\"},\"list\":{\"label\":\"usuarios_vehiculos\",\"searchable\":false,\"sortable\":false}},\"rutas_users\":{\"edit\":{\"label\":\"rutas_users\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"id\"},\"list\":{\"label\":\"rutas_users\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"username\",\"email\",\"confirmed\"],\"edit\":[[{\"name\":\"username\",\"size\":6},{\"name\":\"email\",\"size\":6}],[{\"name\":\"password\",\"size\":6},{\"name\":\"confirmed\",\"size\":4}],[{\"name\":\"blocked\",\"size\":4},{\"name\":\"role\",\"size\":6}],[{\"name\":\"num\",\"size\":6},{\"name\":\"nombrePila\",\"size\":6}],[{\"name\":\"apPaterno\",\"size\":6},{\"name\":\"numTel\",\"size\":6}],[{\"name\":\"apMaterno\",\"size\":6},{\"name\":\"usuarios_vehiculos\",\"size\":6}],[{\"name\":\"rutas_users\",\"size\":6}]]},\"uid\":\"plugin::users-permissions.user\"}', 'object', NULL, NULL),
(17, 'plugin_upload_settings', '{\"sizeOptimization\":true,\"responsiveDimensions\":true,\"autoOrientation\":false}', 'object', NULL, NULL),
(18, 'plugin_upload_view_configuration', '{\"pageSize\":10,\"sort\":\"createdAt:DESC\"}', 'object', NULL, NULL),
(19, 'plugin_upload_metrics', '{\"weeklySchedule\":\"48 17 9 * * 3\",\"lastWeeklyUpdate\":1709140668048}', 'object', NULL, NULL),
(20, 'plugin_users-permissions_grant', '{\"email\":{\"enabled\":true,\"icon\":\"envelope\"},\"discord\":{\"enabled\":false,\"icon\":\"discord\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/discord/callback\",\"scope\":[\"identify\",\"email\"]},\"facebook\":{\"enabled\":false,\"icon\":\"facebook-square\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/facebook/callback\",\"scope\":[\"email\"]},\"google\":{\"enabled\":false,\"icon\":\"google\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/google/callback\",\"scope\":[\"email\"]},\"github\":{\"enabled\":false,\"icon\":\"github\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/github/callback\",\"scope\":[\"user\",\"user:email\"]},\"microsoft\":{\"enabled\":false,\"icon\":\"windows\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/microsoft/callback\",\"scope\":[\"user.read\"]},\"twitter\":{\"enabled\":false,\"icon\":\"twitter\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/twitter/callback\"},\"instagram\":{\"enabled\":false,\"icon\":\"instagram\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/instagram/callback\",\"scope\":[\"user_profile\"]},\"vk\":{\"enabled\":false,\"icon\":\"vk\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/vk/callback\",\"scope\":[\"email\"]},\"twitch\":{\"enabled\":false,\"icon\":\"twitch\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/twitch/callback\",\"scope\":[\"user:read:email\"]},\"linkedin\":{\"enabled\":false,\"icon\":\"linkedin\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/linkedin/callback\",\"scope\":[\"r_liteprofile\",\"r_emailaddress\"]},\"cognito\":{\"enabled\":false,\"icon\":\"aws\",\"key\":\"\",\"secret\":\"\",\"subdomain\":\"my.subdomain.com\",\"callback\":\"api/auth/cognito/callback\",\"scope\":[\"email\",\"openid\",\"profile\"]},\"reddit\":{\"enabled\":false,\"icon\":\"reddit\",\"key\":\"\",\"secret\":\"\",\"state\":true,\"callback\":\"api/auth/reddit/callback\",\"scope\":[\"identity\"]},\"auth0\":{\"enabled\":false,\"icon\":\"\",\"key\":\"\",\"secret\":\"\",\"subdomain\":\"my-tenant.eu\",\"callback\":\"api/auth/auth0/callback\",\"scope\":[\"openid\",\"email\",\"profile\"]},\"cas\":{\"enabled\":false,\"icon\":\"book\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/cas/callback\",\"scope\":[\"openid email\"],\"subdomain\":\"my.subdomain.com/cas\"},\"patreon\":{\"enabled\":false,\"icon\":\"\",\"key\":\"\",\"secret\":\"\",\"callback\":\"api/auth/patreon/callback\",\"scope\":[\"identity\",\"identity[email]\"]}}', 'object', NULL, NULL),
(21, 'plugin_users-permissions_email', '{\"reset_password\":{\"display\":\"Email.template.reset_password\",\"icon\":\"sync\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"joseph.thecrack117@gmail.com\"},\"response_email\":\"0322103790@gmail.com\",\"object\":\"Reset password\",\"message\":\"<p>We heard that you lost your password. Sorry about that!</p>\\n\\n<p>But don’t worry! You can use the following link to reset your password:</p>\\n<p><%= URL %>?code=<%= TOKEN %></p>\\n\\n<p>Thanks.</p>\"}},\"email_confirmation\":{\"display\":\"Email.template.email_confirmation\",\"icon\":\"check-square\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"joseph.thecrack117@gmail.com\"},\"response_email\":\"\",\"object\":\"Account confirmation\",\"message\":\"<p>Thank you for registering!</p>\\n\\n<p>You have to confirm your email address. Please click on the link below.</p>\\n\\n<p><%= URL %>?confirmation=<%= CODE %></p>\\n\\n<p>Thanks.</p>\"}}}', 'object', NULL, NULL),
(22, 'plugin_users-permissions_advanced', '{\"unique_email\":true,\"allow_register\":true,\"email_confirmation\":false,\"email_reset_password\":null,\"email_confirmation_redirection\":null,\"default_role\":\"authenticated\"}', 'object', NULL, NULL),
(23, 'plugin_i18n_default_locale', '\"en\"', 'string', NULL, NULL),
(24, 'core_admin_auth', '{\"providers\":{\"autoRegister\":false,\"defaultRole\":null,\"ssoLockedRoles\":null}}', 'object', NULL, NULL);
INSERT INTO `strapi_core_store_settings` (`id`, `key`, `value`, `type`, `environment`, `tag`) VALUES
(25, 'plugin_content_manager_configuration_content_types::api::cargas-combustible.cargas-combustible', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"numFolio\",\"defaultSortBy\":\"numFolio\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"numFolio\":{\"edit\":{\"label\":\"numFolio\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"numFolio\",\"searchable\":true,\"sortable\":true}},\"litrosCargados\":{\"edit\":{\"label\":\"litrosCargados\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"litrosCargados\",\"searchable\":true,\"sortable\":true}},\"fecha\":{\"edit\":{\"label\":\"fecha\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"fecha\",\"searchable\":true,\"sortable\":true}},\"kilometraje\":{\"edit\":{\"label\":\"kilometraje\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"kilometraje\",\"searchable\":true,\"sortable\":true}},\"comentarios\":{\"edit\":{\"label\":\"comentarios\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"comentarios\",\"searchable\":true,\"sortable\":true}},\"costoTotal\":{\"edit\":{\"label\":\"costoTotal\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"costoTotal\",\"searchable\":true,\"sortable\":true}},\"vehiculo\":{\"edit\":{\"label\":\"vehiculo\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"placas\"},\"list\":{\"label\":\"vehiculo\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"numFolio\",\"litrosCargados\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"numFolio\",\"size\":6}],[{\"name\":\"litrosCargados\",\"size\":4},{\"name\":\"fecha\",\"size\":6}],[{\"name\":\"kilometraje\",\"size\":4},{\"name\":\"comentarios\",\"size\":6}],[{\"name\":\"costoTotal\",\"size\":4},{\"name\":\"vehiculo\",\"size\":6}]]},\"uid\":\"api::cargas-combustible.cargas-combustible\"}', 'object', NULL, NULL),
(26, 'plugin_content_manager_configuration_content_types::api::vehiculo.vehiculo', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"placas\",\"defaultSortBy\":\"placas\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"numSerie\":{\"edit\":{\"label\":\"numSerie\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"numSerie\",\"searchable\":true,\"sortable\":true}},\"placas\":{\"edit\":{\"label\":\"placas\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"placas\",\"searchable\":true,\"sortable\":true}},\"kilometraje\":{\"edit\":{\"label\":\"kilometraje\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"kilometraje\",\"searchable\":true,\"sortable\":true}},\"fechaAdquisicion\":{\"edit\":{\"label\":\"fechaAdquisicion\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"fechaAdquisicion\",\"searchable\":true,\"sortable\":true}},\"usaDiesel\":{\"edit\":{\"label\":\"usaDiesel\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"usaDiesel\",\"searchable\":true,\"sortable\":true}},\"modelo\":{\"edit\":{\"label\":\"modelo\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"nombre\"},\"list\":{\"label\":\"modelo\",\"searchable\":false,\"sortable\":false}},\"marca\":{\"edit\":{\"label\":\"marca\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"nombre\"},\"list\":{\"label\":\"marca\",\"searchable\":false,\"sortable\":false}},\"tipo\":{\"edit\":{\"label\":\"tipo\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"nombre\"},\"list\":{\"label\":\"tipo\",\"searchable\":false,\"sortable\":false}},\"vehiculos_usuarios\":{\"edit\":{\"label\":\"vehiculos_usuarios\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"username\"},\"list\":{\"label\":\"vehiculos_usuarios\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"numSerie\",\"placas\",\"kilometraje\"],\"edit\":[[{\"name\":\"numSerie\",\"size\":6},{\"name\":\"placas\",\"size\":6}],[{\"name\":\"kilometraje\",\"size\":4},{\"name\":\"fechaAdquisicion\",\"size\":6}],[{\"name\":\"usaDiesel\",\"size\":4},{\"name\":\"modelo\",\"size\":6}],[{\"name\":\"marca\",\"size\":6},{\"name\":\"tipo\",\"size\":6}],[{\"name\":\"vehiculos_usuarios\",\"size\":6}]]},\"uid\":\"api::vehiculo.vehiculo\"}', 'object', NULL, NULL),
(27, 'plugin_content_manager_configuration_content_types::api::modelo-vehiculo.modelo-vehiculo', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"nombre\",\"defaultSortBy\":\"nombre\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"nombre\":{\"edit\":{\"label\":\"nombre\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"nombre\",\"searchable\":true,\"sortable\":true}},\"consumoCombustible\":{\"edit\":{\"label\":\"consumoCombustible\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"consumoCombustible\",\"searchable\":true,\"sortable\":true}},\"cantCarga\":{\"edit\":{\"label\":\"cantCarga\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"cantCarga\",\"searchable\":true,\"sortable\":true}},\"capPasajeros\":{\"edit\":{\"label\":\"capPasajeros\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"capPasajeros\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"nombre\",\"consumoCombustible\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"nombre\",\"size\":6}],[{\"name\":\"consumoCombustible\",\"size\":4},{\"name\":\"cantCarga\",\"size\":4},{\"name\":\"capPasajeros\",\"size\":4}]]},\"uid\":\"api::modelo-vehiculo.modelo-vehiculo\"}', 'object', NULL, NULL),
(28, 'plugin_content_manager_configuration_content_types::api::marca-vehiculo.marca-vehiculo', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"nombre\",\"defaultSortBy\":\"nombre\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"nombre\":{\"edit\":{\"label\":\"nombre\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"nombre\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"nombre\",\"createdAt\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"nombre\",\"size\":6}]]},\"uid\":\"api::marca-vehiculo.marca-vehiculo\"}', 'object', NULL, NULL),
(29, 'plugin_content_manager_configuration_content_types::api::tipo-vehiculo.tipo-vehiculo', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"nombre\",\"defaultSortBy\":\"nombre\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"nombre\":{\"edit\":{\"label\":\"nombre\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"nombre\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"nombre\",\"createdAt\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"nombre\",\"size\":6}]]},\"uid\":\"api::tipo-vehiculo.tipo-vehiculo\"}', 'object', NULL, NULL),
(30, 'plugin_content_manager_configuration_content_types::api::ruta.ruta', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"id\",\"defaultSortBy\":\"id\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"fechaHoraInicio\":{\"edit\":{\"label\":\"fechaHoraInicio\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"fechaHoraInicio\",\"searchable\":true,\"sortable\":true}},\"fechaHoraFin\":{\"edit\":{\"label\":\"fechaHoraFin\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"fechaHoraFin\",\"searchable\":true,\"sortable\":true}},\"comentarios\":{\"edit\":{\"label\":\"comentarios\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"comentarios\",\"searchable\":true,\"sortable\":true}},\"finalizado\":{\"edit\":{\"label\":\"finalizado\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"finalizado\",\"searchable\":true,\"sortable\":true}},\"paradas_rutas\":{\"edit\":{\"label\":\"paradas_rutas\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"nombre\"},\"list\":{\"label\":\"paradas_rutas\",\"searchable\":false,\"sortable\":false}},\"users_rutas\":{\"edit\":{\"label\":\"users_rutas\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"username\"},\"list\":{\"label\":\"users_rutas\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"fechaHoraInicio\",\"fechaHoraFin\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"fechaHoraInicio\",\"size\":6}],[{\"name\":\"fechaHoraFin\",\"size\":6},{\"name\":\"comentarios\",\"size\":6}],[{\"name\":\"finalizado\",\"size\":4}],[{\"name\":\"paradas_rutas\",\"size\":6}],[{\"name\":\"users_rutas\",\"size\":6}]]},\"uid\":\"api::ruta.ruta\"}', 'object', NULL, NULL),
(31, 'plugin_content_manager_configuration_content_types::api::parada.parada', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"nombre\",\"defaultSortBy\":\"nombre\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"nombre\":{\"edit\":{\"label\":\"nombre\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"nombre\",\"searchable\":true,\"sortable\":true}},\"dirColonia\":{\"edit\":{\"label\":\"dirColonia\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"dirColonia\",\"searchable\":true,\"sortable\":true}},\"dirCalle\":{\"edit\":{\"label\":\"dirCalle\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"dirCalle\",\"searchable\":true,\"sortable\":true}},\"dirNumero\":{\"edit\":{\"label\":\"dirNumero\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"dirNumero\",\"searchable\":true,\"sortable\":true}},\"rutas_paradas\":{\"edit\":{\"label\":\"rutas_paradas\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"id\"},\"list\":{\"label\":\"rutas_paradas\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"nombre\",\"dirColonia\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"nombre\",\"size\":6}],[{\"name\":\"dirColonia\",\"size\":6},{\"name\":\"dirCalle\",\"size\":6}],[{\"name\":\"dirNumero\",\"size\":6},{\"name\":\"rutas_paradas\",\"size\":6}]]},\"uid\":\"api::parada.parada\"}', 'object', NULL, NULL),
(32, 'plugin_content_manager_configuration_content_types::api::estado-parada.estado-parada', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"descripcion\",\"defaultSortBy\":\"descripcion\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"descripcion\":{\"edit\":{\"label\":\"descripcion\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"descripcion\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"descripcion\",\"createdAt\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"descripcion\",\"size\":6}]]},\"uid\":\"api::estado-parada.estado-parada\"}', 'object', NULL, NULL),
(34, 'plugin_content_manager_configuration_content_types::api::tipo-cobertura.tipo-cobertura', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"nombre\",\"defaultSortBy\":\"nombre\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"nombre\":{\"edit\":{\"label\":\"nombre\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"nombre\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"nombre\",\"createdAt\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"nombre\",\"size\":6}]]},\"uid\":\"api::tipo-cobertura.tipo-cobertura\"}', 'object', NULL, NULL),
(35, 'plugin_content_manager_configuration_content_types::api::aseguradora.aseguradora', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"nombre\",\"defaultSortBy\":\"nombre\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"num\":{\"edit\":{\"label\":\"num\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"num\",\"searchable\":true,\"sortable\":true}},\"nombre\":{\"edit\":{\"label\":\"nombre\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"nombre\",\"searchable\":true,\"sortable\":true}},\"dirColonia\":{\"edit\":{\"label\":\"dirColonia\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"dirColonia\",\"searchable\":true,\"sortable\":true}},\"dirCalle\":{\"edit\":{\"label\":\"dirCalle\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"dirCalle\",\"searchable\":true,\"sortable\":true}},\"dirNumero\":{\"edit\":{\"label\":\"dirNumero\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"dirNumero\",\"searchable\":true,\"sortable\":true}},\"numTel\":{\"edit\":{\"label\":\"numTel\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"numTel\",\"searchable\":true,\"sortable\":true}},\"correo\":{\"edit\":{\"label\":\"correo\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"correo\",\"searchable\":true,\"sortable\":true}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"num\",\"nombre\",\"dirColonia\"],\"edit\":[[{\"name\":\"num\",\"size\":6},{\"name\":\"nombre\",\"size\":6}],[{\"name\":\"dirColonia\",\"size\":6},{\"name\":\"dirCalle\",\"size\":6}],[{\"name\":\"dirNumero\",\"size\":6},{\"name\":\"numTel\",\"size\":6}],[{\"name\":\"correo\",\"size\":6}]]},\"uid\":\"api::aseguradora.aseguradora\"}', 'object', NULL, NULL),
(36, 'plugin_content_manager_configuration_content_types::api::poliza-seguro.poliza-seguro', '{\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"frecuenciaPago\",\"defaultSortBy\":\"frecuenciaPago\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"id\",\"searchable\":true,\"sortable\":true}},\"numPoliza\":{\"edit\":{\"label\":\"numPoliza\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"numPoliza\",\"searchable\":true,\"sortable\":true}},\"fechaInicio\":{\"edit\":{\"label\":\"fechaInicio\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"fechaInicio\",\"searchable\":true,\"sortable\":true}},\"fechaVencimiento\":{\"edit\":{\"label\":\"fechaVencimiento\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"fechaVencimiento\",\"searchable\":true,\"sortable\":true}},\"montoCobertura\":{\"edit\":{\"label\":\"montoCobertura\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"montoCobertura\",\"searchable\":true,\"sortable\":true}},\"costo\":{\"edit\":{\"label\":\"costo\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"costo\",\"searchable\":true,\"sortable\":true}},\"frecuenciaPago\":{\"edit\":{\"label\":\"frecuenciaPago\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"frecuenciaPago\",\"searchable\":true,\"sortable\":true}},\"vehiculo\":{\"edit\":{\"label\":\"vehiculo\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"placas\"},\"list\":{\"label\":\"vehiculo\",\"searchable\":false,\"sortable\":false}},\"aseguradora\":{\"edit\":{\"label\":\"aseguradora\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"nombre\"},\"list\":{\"label\":\"aseguradora\",\"searchable\":false,\"sortable\":false}},\"cobertura\":{\"edit\":{\"label\":\"cobertura\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"nombre\"},\"list\":{\"label\":\"cobertura\",\"searchable\":false,\"sortable\":false}},\"createdAt\":{\"edit\":{\"label\":\"createdAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"createdAt\",\"searchable\":true,\"sortable\":true}},\"updatedAt\":{\"edit\":{\"label\":\"updatedAt\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"updatedAt\",\"searchable\":true,\"sortable\":true}},\"createdBy\":{\"edit\":{\"label\":\"createdBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"createdBy\",\"searchable\":true,\"sortable\":true}},\"updatedBy\":{\"edit\":{\"label\":\"updatedBy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true,\"mainField\":\"firstname\"},\"list\":{\"label\":\"updatedBy\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"numPoliza\",\"fechaInicio\",\"fechaVencimiento\"],\"edit\":[[{\"name\":\"numPoliza\",\"size\":6},{\"name\":\"fechaInicio\",\"size\":6}],[{\"name\":\"fechaVencimiento\",\"size\":6},{\"name\":\"montoCobertura\",\"size\":4}],[{\"name\":\"costo\",\"size\":4},{\"name\":\"frecuenciaPago\",\"size\":6}],[{\"name\":\"vehiculo\",\"size\":6},{\"name\":\"aseguradora\",\"size\":6}],[{\"name\":\"cobertura\",\"size\":6}]]},\"uid\":\"api::poliza-seguro.poliza-seguro\"}', 'object', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `strapi_database_schema`
--

CREATE TABLE `strapi_database_schema` (
  `id` int(10) UNSIGNED NOT NULL,
  `schema` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`schema`)),
  `time` datetime DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `strapi_database_schema`
--

INSERT INTO `strapi_database_schema` (`id`, `schema`, `time`, `hash`) VALUES
(25, '{\"tables\":[{\"name\":\"strapi_core_store_settings\",\"indexes\":[],\"foreignKeys\":[],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"key\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"value\",\"type\":\"text\",\"args\":[\"longtext\"],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"type\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"environment\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"tag\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false}]},{\"name\":\"strapi_webhooks\",\"indexes\":[],\"foreignKeys\":[],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"url\",\"type\":\"text\",\"args\":[\"longtext\"],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"headers\",\"type\":\"jsonb\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"events\",\"type\":\"jsonb\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"enabled\",\"type\":\"boolean\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false}]},{\"name\":\"admin_permissions\",\"indexes\":[{\"name\":\"admin_permissions_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"admin_permissions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"admin_permissions_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"admin_permissions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"action\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"action_parameters\",\"type\":\"jsonb\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"subject\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"properties\",\"type\":\"jsonb\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"conditions\",\"type\":\"jsonb\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"admin_users\",\"indexes\":[{\"name\":\"admin_users_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"admin_users_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"admin_users_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"admin_users_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"firstname\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"lastname\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"username\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"email\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"password\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"reset_password_token\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"registration_token\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"is_active\",\"type\":\"boolean\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"blocked\",\"type\":\"boolean\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"prefered_language\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"admin_roles\",\"indexes\":[{\"name\":\"admin_roles_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"admin_roles_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"admin_roles_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"admin_roles_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"code\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"description\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_api_tokens\",\"indexes\":[{\"name\":\"strapi_api_tokens_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"strapi_api_tokens_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"strapi_api_tokens_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"strapi_api_tokens_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"description\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"type\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"access_key\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"last_used_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"expires_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"lifespan\",\"type\":\"bigInteger\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_api_token_permissions\",\"indexes\":[{\"name\":\"strapi_api_token_permissions_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"strapi_api_token_permissions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"strapi_api_token_permissions_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"strapi_api_token_permissions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"action\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_transfer_tokens\",\"indexes\":[{\"name\":\"strapi_transfer_tokens_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"strapi_transfer_tokens_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"strapi_transfer_tokens_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"strapi_transfer_tokens_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"description\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"access_key\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"last_used_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"expires_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"lifespan\",\"type\":\"bigInteger\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_transfer_token_permissions\",\"indexes\":[{\"name\":\"strapi_transfer_token_permissions_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"strapi_transfer_token_permissions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"strapi_transfer_token_permissions_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"strapi_transfer_token_permissions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"action\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"files\",\"indexes\":[{\"name\":\"upload_files_folder_path_index\",\"columns\":[\"folder_path\"],\"type\":null},{\"name\":\"upload_files_created_at_index\",\"columns\":[\"created_at\"],\"type\":null},{\"name\":\"upload_files_updated_at_index\",\"columns\":[\"updated_at\"],\"type\":null},{\"name\":\"upload_files_name_index\",\"columns\":[\"name\"],\"type\":null},{\"name\":\"upload_files_size_index\",\"columns\":[\"size\"],\"type\":null},{\"name\":\"upload_files_ext_index\",\"columns\":[\"ext\"],\"type\":null},{\"name\":\"files_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"files_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"files_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"files_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"alternative_text\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"caption\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"width\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"height\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"formats\",\"type\":\"jsonb\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"hash\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"ext\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"mime\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"size\",\"type\":\"decimal\",\"args\":[10,2],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"url\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"preview_url\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"provider\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"provider_metadata\",\"type\":\"jsonb\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"folder_path\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"upload_folders\",\"indexes\":[{\"name\":\"upload_folders_path_id_index\",\"columns\":[\"path_id\"],\"type\":\"unique\"},{\"name\":\"upload_folders_path_index\",\"columns\":[\"path\"],\"type\":\"unique\"},{\"name\":\"upload_folders_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"upload_folders_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"upload_folders_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"upload_folders_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"path_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"path\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_releases\",\"indexes\":[{\"name\":\"strapi_releases_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"strapi_releases_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"strapi_releases_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"strapi_releases_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"released_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"scheduled_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"timezone\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_release_actions\",\"indexes\":[{\"name\":\"strapi_release_actions_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"strapi_release_actions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"strapi_release_actions_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"strapi_release_actions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"type\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"target_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"target_type\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"content_type\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"locale\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"i18n_locale\",\"indexes\":[{\"name\":\"i18n_locale_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"i18n_locale_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"i18n_locale_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"i18n_locale_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"code\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"up_permissions\",\"indexes\":[{\"name\":\"up_permissions_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"up_permissions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"up_permissions_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"up_permissions_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"action\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"up_roles\",\"indexes\":[{\"name\":\"up_roles_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"up_roles_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"up_roles_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"up_roles_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"name\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"description\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"type\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"up_users\",\"indexes\":[{\"type\":\"unique\",\"name\":\"up_users_num_unique\",\"columns\":[\"num\"]},{\"name\":\"up_users_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"up_users_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"up_users_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"up_users_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"username\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"provider\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"reset_password_token\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"confirmation_token\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"confirmed\",\"type\":\"boolean\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"blocked\",\"type\":\"boolean\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"nombre_pila\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"ap_paterno\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"ap_materno\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"num_tel\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"email\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"password\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"aseguradoras\",\"indexes\":[{\"type\":\"unique\",\"name\":\"aseguradoras_num_unique\",\"columns\":[\"num\"]},{\"name\":\"aseguradoras_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"aseguradoras_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"aseguradoras_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"aseguradoras_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"nombre\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"dir_colonia\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"dir_calle\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"dir_numero\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"num_tel\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"correo\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"cargas_combustibles\",\"indexes\":[{\"type\":\"unique\",\"name\":\"cargas_combustibles_num_unique\",\"columns\":[\"num\"]},{\"name\":\"cargas_combustibles_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"cargas_combustibles_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"cargas_combustibles_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"cargas_combustibles_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"num_folio\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"litros_cargados\",\"type\":\"decimal\",\"args\":[10,2],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"fecha\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"kilometraje\",\"type\":\"bigInteger\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"comentarios\",\"type\":\"text\",\"args\":[\"longtext\"],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"costo_total\",\"type\":\"decimal\",\"args\":[10,2],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"estados_paradas\",\"indexes\":[{\"type\":\"unique\",\"name\":\"estados_paradas_num_unique\",\"columns\":[\"num\"]},{\"name\":\"estados_paradas_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"estados_paradas_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"estados_paradas_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"estados_paradas_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"descripcion\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"marcas_vehiculos\",\"indexes\":[{\"type\":\"unique\",\"name\":\"marcas_vehiculos_num_unique\",\"columns\":[\"num\"]},{\"name\":\"marcas_vehiculos_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"marcas_vehiculos_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"marcas_vehiculos_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"marcas_vehiculos_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"nombre\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"modelos_vehiculos\",\"indexes\":[{\"type\":\"unique\",\"name\":\"modelos_vehiculos_num_unique\",\"columns\":[\"num\"]},{\"name\":\"modelos_vehiculos_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"modelos_vehiculos_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"modelos_vehiculos_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"modelos_vehiculos_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"nombre\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"consumo_combustible\",\"type\":\"bigInteger\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"cant_carga\",\"type\":\"bigInteger\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"cap_pasajeros\",\"type\":\"bigInteger\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"paradas\",\"indexes\":[{\"type\":\"unique\",\"name\":\"paradas_num_unique\",\"columns\":[\"num\"]},{\"name\":\"paradas_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"paradas_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"paradas_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"paradas_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"nombre\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"dir_colonia\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"dir_calle\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"dir_numero\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"polizas_seguros\",\"indexes\":[{\"type\":\"unique\",\"name\":\"polizas_seguros_num_poliza_unique\",\"columns\":[\"num_poliza\"]},{\"name\":\"polizas_seguros_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"polizas_seguros_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"polizas_seguros_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"polizas_seguros_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num_poliza\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"fecha_inicio\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"fecha_vencimiento\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"monto_cobertura\",\"type\":\"decimal\",\"args\":[10,2],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"costo\",\"type\":\"decimal\",\"args\":[10,2],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"frecuencia_pago\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"rutas\",\"indexes\":[{\"type\":\"unique\",\"name\":\"rutas_num_unique\",\"columns\":[\"num\"]},{\"name\":\"rutas_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"rutas_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"rutas_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"rutas_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"fecha_hora_inicio\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"fecha_hora_fin\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"comentarios\",\"type\":\"text\",\"args\":[\"longtext\"],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"finalizado\",\"type\":\"boolean\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"tipos_coberturas\",\"indexes\":[{\"type\":\"unique\",\"name\":\"tipos_coberturas_num_unique\",\"columns\":[\"num\"]},{\"name\":\"tipos_coberturas_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"tipos_coberturas_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"tipos_coberturas_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"tipos_coberturas_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"nombre\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"tipos_vehiculos\",\"indexes\":[{\"type\":\"unique\",\"name\":\"tipos_vehiculos_num_unique\",\"columns\":[\"num\"]},{\"name\":\"tipos_vehiculos_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"tipos_vehiculos_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"tipos_vehiculos_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"tipos_vehiculos_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"nombre\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"vehiculos\",\"indexes\":[{\"type\":\"unique\",\"name\":\"vehiculos_num_serie_unique\",\"columns\":[\"num_serie\"]},{\"name\":\"vehiculos_created_by_id_fk\",\"columns\":[\"created_by_id\"]},{\"name\":\"vehiculos_updated_by_id_fk\",\"columns\":[\"updated_by_id\"]}],\"foreignKeys\":[{\"name\":\"vehiculos_created_by_id_fk\",\"columns\":[\"created_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"},{\"name\":\"vehiculos_updated_by_id_fk\",\"columns\":[\"updated_by_id\"],\"referencedTable\":\"admin_users\",\"referencedColumns\":[\"id\"],\"onDelete\":\"SET NULL\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"num_serie\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false,\"unique\":true},{\"name\":\"placas\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"kilometraje\",\"type\":\"bigInteger\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"fecha_adquisicion\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"usa_diesel\",\"type\":\"boolean\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"updated_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"published_at\",\"type\":\"datetime\",\"args\":[{\"useTz\":false,\"precision\":6}],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"created_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"updated_by_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"admin_permissions_role_links\",\"indexes\":[{\"name\":\"admin_permissions_role_links_fk\",\"columns\":[\"permission_id\"]},{\"name\":\"admin_permissions_role_links_inv_fk\",\"columns\":[\"role_id\"]},{\"name\":\"admin_permissions_role_links_unique\",\"columns\":[\"permission_id\",\"role_id\"],\"type\":\"unique\"},{\"name\":\"admin_permissions_role_links_order_inv_fk\",\"columns\":[\"permission_order\"]}],\"foreignKeys\":[{\"name\":\"admin_permissions_role_links_fk\",\"columns\":[\"permission_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"admin_permissions\",\"onDelete\":\"CASCADE\"},{\"name\":\"admin_permissions_role_links_inv_fk\",\"columns\":[\"role_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"admin_roles\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"permission_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"role_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"permission_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"admin_users_roles_links\",\"indexes\":[{\"name\":\"admin_users_roles_links_fk\",\"columns\":[\"user_id\"]},{\"name\":\"admin_users_roles_links_inv_fk\",\"columns\":[\"role_id\"]},{\"name\":\"admin_users_roles_links_unique\",\"columns\":[\"user_id\",\"role_id\"],\"type\":\"unique\"},{\"name\":\"admin_users_roles_links_order_fk\",\"columns\":[\"role_order\"]},{\"name\":\"admin_users_roles_links_order_inv_fk\",\"columns\":[\"user_order\"]}],\"foreignKeys\":[{\"name\":\"admin_users_roles_links_fk\",\"columns\":[\"user_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"admin_users\",\"onDelete\":\"CASCADE\"},{\"name\":\"admin_users_roles_links_inv_fk\",\"columns\":[\"role_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"admin_roles\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"user_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"role_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"role_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"user_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_api_token_permissions_token_links\",\"indexes\":[{\"name\":\"strapi_api_token_permissions_token_links_fk\",\"columns\":[\"api_token_permission_id\"]},{\"name\":\"strapi_api_token_permissions_token_links_inv_fk\",\"columns\":[\"api_token_id\"]},{\"name\":\"strapi_api_token_permissions_token_links_unique\",\"columns\":[\"api_token_permission_id\",\"api_token_id\"],\"type\":\"unique\"},{\"name\":\"strapi_api_token_permissions_token_links_order_inv_fk\",\"columns\":[\"api_token_permission_order\"]}],\"foreignKeys\":[{\"name\":\"strapi_api_token_permissions_token_links_fk\",\"columns\":[\"api_token_permission_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"strapi_api_token_permissions\",\"onDelete\":\"CASCADE\"},{\"name\":\"strapi_api_token_permissions_token_links_inv_fk\",\"columns\":[\"api_token_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"strapi_api_tokens\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"api_token_permission_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"api_token_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"api_token_permission_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_transfer_token_permissions_token_links\",\"indexes\":[{\"name\":\"strapi_transfer_token_permissions_token_links_fk\",\"columns\":[\"transfer_token_permission_id\"]},{\"name\":\"strapi_transfer_token_permissions_token_links_inv_fk\",\"columns\":[\"transfer_token_id\"]},{\"name\":\"strapi_transfer_token_permissions_token_links_unique\",\"columns\":[\"transfer_token_permission_id\",\"transfer_token_id\"],\"type\":\"unique\"},{\"name\":\"strapi_transfer_token_permissions_token_links_order_inv_fk\",\"columns\":[\"transfer_token_permission_order\"]}],\"foreignKeys\":[{\"name\":\"strapi_transfer_token_permissions_token_links_fk\",\"columns\":[\"transfer_token_permission_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"strapi_transfer_token_permissions\",\"onDelete\":\"CASCADE\"},{\"name\":\"strapi_transfer_token_permissions_token_links_inv_fk\",\"columns\":[\"transfer_token_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"strapi_transfer_tokens\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"transfer_token_permission_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"transfer_token_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"transfer_token_permission_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"files_related_morphs\",\"indexes\":[{\"name\":\"files_related_morphs_fk\",\"columns\":[\"file_id\"]},{\"name\":\"files_related_morphs_order_index\",\"columns\":[\"order\"]},{\"name\":\"files_related_morphs_id_column_index\",\"columns\":[\"related_id\"]}],\"foreignKeys\":[{\"name\":\"files_related_morphs_fk\",\"columns\":[\"file_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"files\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"file_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"related_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"related_type\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"field\",\"type\":\"string\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":false},{\"name\":\"order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"files_folder_links\",\"indexes\":[{\"name\":\"files_folder_links_fk\",\"columns\":[\"file_id\"]},{\"name\":\"files_folder_links_inv_fk\",\"columns\":[\"folder_id\"]},{\"name\":\"files_folder_links_unique\",\"columns\":[\"file_id\",\"folder_id\"],\"type\":\"unique\"},{\"name\":\"files_folder_links_order_inv_fk\",\"columns\":[\"file_order\"]}],\"foreignKeys\":[{\"name\":\"files_folder_links_fk\",\"columns\":[\"file_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"files\",\"onDelete\":\"CASCADE\"},{\"name\":\"files_folder_links_inv_fk\",\"columns\":[\"folder_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"upload_folders\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"file_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"folder_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"file_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"upload_folders_parent_links\",\"indexes\":[{\"name\":\"upload_folders_parent_links_fk\",\"columns\":[\"folder_id\"]},{\"name\":\"upload_folders_parent_links_inv_fk\",\"columns\":[\"inv_folder_id\"]},{\"name\":\"upload_folders_parent_links_unique\",\"columns\":[\"folder_id\",\"inv_folder_id\"],\"type\":\"unique\"},{\"name\":\"upload_folders_parent_links_order_inv_fk\",\"columns\":[\"folder_order\"]}],\"foreignKeys\":[{\"name\":\"upload_folders_parent_links_fk\",\"columns\":[\"folder_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"upload_folders\",\"onDelete\":\"CASCADE\"},{\"name\":\"upload_folders_parent_links_inv_fk\",\"columns\":[\"inv_folder_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"upload_folders\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"folder_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"inv_folder_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"folder_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"strapi_release_actions_release_links\",\"indexes\":[{\"name\":\"strapi_release_actions_release_links_fk\",\"columns\":[\"release_action_id\"]},{\"name\":\"strapi_release_actions_release_links_inv_fk\",\"columns\":[\"release_id\"]},{\"name\":\"strapi_release_actions_release_links_unique\",\"columns\":[\"release_action_id\",\"release_id\"],\"type\":\"unique\"},{\"name\":\"strapi_release_actions_release_links_order_inv_fk\",\"columns\":[\"release_action_order\"]}],\"foreignKeys\":[{\"name\":\"strapi_release_actions_release_links_fk\",\"columns\":[\"release_action_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"strapi_release_actions\",\"onDelete\":\"CASCADE\"},{\"name\":\"strapi_release_actions_release_links_inv_fk\",\"columns\":[\"release_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"strapi_releases\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"release_action_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"release_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"release_action_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"up_permissions_role_links\",\"indexes\":[{\"name\":\"up_permissions_role_links_fk\",\"columns\":[\"permission_id\"]},{\"name\":\"up_permissions_role_links_inv_fk\",\"columns\":[\"role_id\"]},{\"name\":\"up_permissions_role_links_unique\",\"columns\":[\"permission_id\",\"role_id\"],\"type\":\"unique\"},{\"name\":\"up_permissions_role_links_order_inv_fk\",\"columns\":[\"permission_order\"]}],\"foreignKeys\":[{\"name\":\"up_permissions_role_links_fk\",\"columns\":[\"permission_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"up_permissions\",\"onDelete\":\"CASCADE\"},{\"name\":\"up_permissions_role_links_inv_fk\",\"columns\":[\"role_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"up_roles\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"permission_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"role_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"permission_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"up_users_role_links\",\"indexes\":[{\"name\":\"up_users_role_links_fk\",\"columns\":[\"user_id\"]},{\"name\":\"up_users_role_links_inv_fk\",\"columns\":[\"role_id\"]},{\"name\":\"up_users_role_links_unique\",\"columns\":[\"user_id\",\"role_id\"],\"type\":\"unique\"},{\"name\":\"up_users_role_links_order_inv_fk\",\"columns\":[\"user_order\"]}],\"foreignKeys\":[{\"name\":\"up_users_role_links_fk\",\"columns\":[\"user_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"up_users\",\"onDelete\":\"CASCADE\"},{\"name\":\"up_users_role_links_inv_fk\",\"columns\":[\"role_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"up_roles\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"user_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"role_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"user_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"up_users_usuarios_vehiculos_links\",\"indexes\":[{\"name\":\"up_users_usuarios_vehiculos_links_fk\",\"columns\":[\"user_id\"]},{\"name\":\"up_users_usuarios_vehiculos_links_inv_fk\",\"columns\":[\"vehiculo_id\"]},{\"name\":\"up_users_usuarios_vehiculos_links_unique\",\"columns\":[\"user_id\",\"vehiculo_id\"],\"type\":\"unique\"},{\"name\":\"up_users_usuarios_vehiculos_links_order_fk\",\"columns\":[\"vehiculo_order\"]},{\"name\":\"up_users_usuarios_vehiculos_links_order_inv_fk\",\"columns\":[\"user_order\"]}],\"foreignKeys\":[{\"name\":\"up_users_usuarios_vehiculos_links_fk\",\"columns\":[\"user_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"up_users\",\"onDelete\":\"CASCADE\"},{\"name\":\"up_users_usuarios_vehiculos_links_inv_fk\",\"columns\":[\"vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"vehiculos\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"user_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"vehiculo_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"user_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"up_users_rutas_users_links\",\"indexes\":[{\"name\":\"up_users_rutas_users_links_fk\",\"columns\":[\"user_id\"]},{\"name\":\"up_users_rutas_users_links_inv_fk\",\"columns\":[\"ruta_id\"]},{\"name\":\"up_users_rutas_users_links_unique\",\"columns\":[\"user_id\",\"ruta_id\"],\"type\":\"unique\"},{\"name\":\"up_users_rutas_users_links_order_fk\",\"columns\":[\"ruta_order\"]},{\"name\":\"up_users_rutas_users_links_order_inv_fk\",\"columns\":[\"user_order\"]}],\"foreignKeys\":[{\"name\":\"up_users_rutas_users_links_fk\",\"columns\":[\"user_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"up_users\",\"onDelete\":\"CASCADE\"},{\"name\":\"up_users_rutas_users_links_inv_fk\",\"columns\":[\"ruta_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"rutas\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"user_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"ruta_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"ruta_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"user_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"cargas_combustibles_vehiculo_links\",\"indexes\":[{\"name\":\"cargas_combustibles_vehiculo_links_fk\",\"columns\":[\"cargas_combustible_id\"]},{\"name\":\"cargas_combustibles_vehiculo_links_inv_fk\",\"columns\":[\"vehiculo_id\"]},{\"name\":\"cargas_combustibles_vehiculo_links_unique\",\"columns\":[\"cargas_combustible_id\",\"vehiculo_id\"],\"type\":\"unique\"},{\"name\":\"cargas_combustibles_vehiculo_links_order_fk\",\"columns\":[\"vehiculo_order\"]}],\"foreignKeys\":[{\"name\":\"cargas_combustibles_vehiculo_links_fk\",\"columns\":[\"cargas_combustible_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"cargas_combustibles\",\"onDelete\":\"CASCADE\"},{\"name\":\"cargas_combustibles_vehiculo_links_inv_fk\",\"columns\":[\"vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"vehiculos\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"cargas_combustible_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"vehiculo_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"polizas_seguros_vehiculo_links\",\"indexes\":[{\"name\":\"polizas_seguros_vehiculo_links_fk\",\"columns\":[\"poliza_seguro_id\"]},{\"name\":\"polizas_seguros_vehiculo_links_inv_fk\",\"columns\":[\"vehiculo_id\"]},{\"name\":\"polizas_seguros_vehiculo_links_unique\",\"columns\":[\"poliza_seguro_id\",\"vehiculo_id\"],\"type\":\"unique\"},{\"name\":\"polizas_seguros_vehiculo_links_order_fk\",\"columns\":[\"vehiculo_order\"]}],\"foreignKeys\":[{\"name\":\"polizas_seguros_vehiculo_links_fk\",\"columns\":[\"poliza_seguro_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"polizas_seguros\",\"onDelete\":\"CASCADE\"},{\"name\":\"polizas_seguros_vehiculo_links_inv_fk\",\"columns\":[\"vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"vehiculos\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"poliza_seguro_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"vehiculo_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"polizas_seguros_aseguradora_links\",\"indexes\":[{\"name\":\"polizas_seguros_aseguradora_links_fk\",\"columns\":[\"poliza_seguro_id\"]},{\"name\":\"polizas_seguros_aseguradora_links_inv_fk\",\"columns\":[\"aseguradora_id\"]},{\"name\":\"polizas_seguros_aseguradora_links_unique\",\"columns\":[\"poliza_seguro_id\",\"aseguradora_id\"],\"type\":\"unique\"},{\"name\":\"polizas_seguros_aseguradora_links_order_fk\",\"columns\":[\"aseguradora_order\"]}],\"foreignKeys\":[{\"name\":\"polizas_seguros_aseguradora_links_fk\",\"columns\":[\"poliza_seguro_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"polizas_seguros\",\"onDelete\":\"CASCADE\"},{\"name\":\"polizas_seguros_aseguradora_links_inv_fk\",\"columns\":[\"aseguradora_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"aseguradoras\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"poliza_seguro_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"aseguradora_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"aseguradora_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"polizas_seguros_cobertura_links\",\"indexes\":[{\"name\":\"polizas_seguros_cobertura_links_fk\",\"columns\":[\"poliza_seguro_id\"]},{\"name\":\"polizas_seguros_cobertura_links_inv_fk\",\"columns\":[\"tipo_cobertura_id\"]},{\"name\":\"polizas_seguros_cobertura_links_unique\",\"columns\":[\"poliza_seguro_id\",\"tipo_cobertura_id\"],\"type\":\"unique\"},{\"name\":\"polizas_seguros_cobertura_links_order_fk\",\"columns\":[\"tipo_cobertura_order\"]}],\"foreignKeys\":[{\"name\":\"polizas_seguros_cobertura_links_fk\",\"columns\":[\"poliza_seguro_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"polizas_seguros\",\"onDelete\":\"CASCADE\"},{\"name\":\"polizas_seguros_cobertura_links_inv_fk\",\"columns\":[\"tipo_cobertura_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"tipos_coberturas\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"poliza_seguro_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"tipo_cobertura_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"tipo_cobertura_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"rutas_paradas_rutas_links\",\"indexes\":[{\"name\":\"rutas_paradas_rutas_links_fk\",\"columns\":[\"ruta_id\"]},{\"name\":\"rutas_paradas_rutas_links_inv_fk\",\"columns\":[\"parada_id\"]},{\"name\":\"rutas_paradas_rutas_links_unique\",\"columns\":[\"ruta_id\",\"parada_id\"],\"type\":\"unique\"},{\"name\":\"rutas_paradas_rutas_links_order_fk\",\"columns\":[\"parada_order\"]},{\"name\":\"rutas_paradas_rutas_links_order_inv_fk\",\"columns\":[\"ruta_order\"]}],\"foreignKeys\":[{\"name\":\"rutas_paradas_rutas_links_fk\",\"columns\":[\"ruta_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"rutas\",\"onDelete\":\"CASCADE\"},{\"name\":\"rutas_paradas_rutas_links_inv_fk\",\"columns\":[\"parada_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"paradas\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"ruta_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"parada_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"parada_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"ruta_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"vehiculos_modelo_links\",\"indexes\":[{\"name\":\"vehiculos_modelo_links_fk\",\"columns\":[\"vehiculo_id\"]},{\"name\":\"vehiculos_modelo_links_inv_fk\",\"columns\":[\"modelo_vehiculo_id\"]},{\"name\":\"vehiculos_modelo_links_unique\",\"columns\":[\"vehiculo_id\",\"modelo_vehiculo_id\"],\"type\":\"unique\"},{\"name\":\"vehiculos_modelo_links_order_fk\",\"columns\":[\"modelo_vehiculo_order\"]}],\"foreignKeys\":[{\"name\":\"vehiculos_modelo_links_fk\",\"columns\":[\"vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"vehiculos\",\"onDelete\":\"CASCADE\"},{\"name\":\"vehiculos_modelo_links_inv_fk\",\"columns\":[\"modelo_vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"modelos_vehiculos\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"modelo_vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"modelo_vehiculo_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"vehiculos_marca_links\",\"indexes\":[{\"name\":\"vehiculos_marca_links_fk\",\"columns\":[\"vehiculo_id\"]},{\"name\":\"vehiculos_marca_links_inv_fk\",\"columns\":[\"marca_vehiculo_id\"]},{\"name\":\"vehiculos_marca_links_unique\",\"columns\":[\"vehiculo_id\",\"marca_vehiculo_id\"],\"type\":\"unique\"},{\"name\":\"vehiculos_marca_links_order_fk\",\"columns\":[\"marca_vehiculo_order\"]}],\"foreignKeys\":[{\"name\":\"vehiculos_marca_links_fk\",\"columns\":[\"vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"vehiculos\",\"onDelete\":\"CASCADE\"},{\"name\":\"vehiculos_marca_links_inv_fk\",\"columns\":[\"marca_vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"marcas_vehiculos\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"marca_vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"marca_vehiculo_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]},{\"name\":\"vehiculos_tipo_links\",\"indexes\":[{\"name\":\"vehiculos_tipo_links_fk\",\"columns\":[\"vehiculo_id\"]},{\"name\":\"vehiculos_tipo_links_inv_fk\",\"columns\":[\"tipo_vehiculo_id\"]},{\"name\":\"vehiculos_tipo_links_unique\",\"columns\":[\"vehiculo_id\",\"tipo_vehiculo_id\"],\"type\":\"unique\"},{\"name\":\"vehiculos_tipo_links_order_fk\",\"columns\":[\"tipo_vehiculo_order\"]}],\"foreignKeys\":[{\"name\":\"vehiculos_tipo_links_fk\",\"columns\":[\"vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"vehiculos\",\"onDelete\":\"CASCADE\"},{\"name\":\"vehiculos_tipo_links_inv_fk\",\"columns\":[\"tipo_vehiculo_id\"],\"referencedColumns\":[\"id\"],\"referencedTable\":\"tipos_vehiculos\",\"onDelete\":\"CASCADE\"}],\"columns\":[{\"name\":\"id\",\"type\":\"increments\",\"args\":[{\"primary\":true,\"primaryKey\":true}],\"defaultTo\":null,\"notNullable\":true,\"unsigned\":false},{\"name\":\"vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"tipo_vehiculo_id\",\"type\":\"integer\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true},{\"name\":\"tipo_vehiculo_order\",\"type\":\"double\",\"args\":[],\"defaultTo\":null,\"notNullable\":false,\"unsigned\":true}]}]}', '2024-02-29 12:57:41', 'c03a3124008a39d0df99cd24a597f54e');

-- --------------------------------------------------------

--
-- Table structure for table `strapi_migrations`
--

CREATE TABLE `strapi_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_releases`
--

CREATE TABLE `strapi_releases` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `released_at` datetime(6) DEFAULT NULL,
  `scheduled_at` datetime(6) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_release_actions`
--

CREATE TABLE `strapi_release_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `target_id` int(10) UNSIGNED DEFAULT NULL,
  `target_type` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_release_actions_release_links`
--

CREATE TABLE `strapi_release_actions_release_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `release_action_id` int(10) UNSIGNED DEFAULT NULL,
  `release_id` int(10) UNSIGNED DEFAULT NULL,
  `release_action_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_transfer_tokens`
--

CREATE TABLE `strapi_transfer_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `access_key` varchar(255) DEFAULT NULL,
  `last_used_at` datetime(6) DEFAULT NULL,
  `expires_at` datetime(6) DEFAULT NULL,
  `lifespan` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_transfer_token_permissions`
--

CREATE TABLE `strapi_transfer_token_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_transfer_token_permissions_token_links`
--

CREATE TABLE `strapi_transfer_token_permissions_token_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `transfer_token_permission_id` int(10) UNSIGNED DEFAULT NULL,
  `transfer_token_id` int(10) UNSIGNED DEFAULT NULL,
  `transfer_token_permission_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `strapi_webhooks`
--

CREATE TABLE `strapi_webhooks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` longtext DEFAULT NULL,
  `headers` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`headers`)),
  `events` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`events`)),
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipos_coberturas`
--

CREATE TABLE `tipos_coberturas` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tipos_coberturas`
--

INSERT INTO `tipos_coberturas` (`id`, `num`, `nombre`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', 'Total', '2024-02-29 13:07:14.982000', '2024-02-29 13:07:15.800000', '2024-02-29 13:07:15.796000', 1, 1),
(2, '2', 'Colision', '2024-02-29 13:07:30.299000', '2024-02-29 13:07:32.312000', '2024-02-29 13:07:32.308000', 1, 1),
(3, '3', 'Robo', '2024-02-29 13:07:45.121000', '2024-02-29 13:08:14.368000', '2024-02-29 13:08:14.365000', 1, 1),
(4, '4', 'Lesiones personales', '2024-02-29 13:08:00.968000', '2024-02-29 13:08:09.456000', '2024-02-29 13:08:09.453000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipos_vehiculos`
--

CREATE TABLE `tipos_vehiculos` (
  `id` int(10) UNSIGNED NOT NULL,
  `num` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tipos_vehiculos`
--

INSERT INTO `tipos_vehiculos` (`id`, `num`, `nombre`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '1', 'Moto', '2024-02-29 11:25:57.747000', '2024-02-29 11:26:00.445000', '2024-02-29 11:26:00.440000', 1, 1),
(2, '2', 'Carro', '2024-02-29 11:26:10.457000', '2024-02-29 11:26:11.414000', '2024-02-29 11:26:11.410000', 1, 1),
(3, '3', 'Camioneta', '2024-02-29 11:26:21.581000', '2024-02-29 11:26:24.489000', '2024-02-29 11:26:24.485000', 1, 1),
(4, '4', 'Tractocamion', '2024-02-29 11:26:36.347000', '2024-02-29 11:26:37.187000', '2024-02-29 11:26:37.182000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `upload_folders`
--

CREATE TABLE `upload_folders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path_id` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upload_folders_parent_links`
--

CREATE TABLE `upload_folders_parent_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `folder_id` int(10) UNSIGNED DEFAULT NULL,
  `inv_folder_id` int(10) UNSIGNED DEFAULT NULL,
  `folder_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `up_permissions`
--

CREATE TABLE `up_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `up_permissions`
--

INSERT INTO `up_permissions` (`id`, `action`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`) VALUES
(1, 'plugin::users-permissions.user.me', '2024-02-28 08:46:05.020000', '2024-02-28 08:46:05.020000', NULL, NULL),
(2, 'plugin::users-permissions.auth.changePassword', '2024-02-28 08:46:05.020000', '2024-02-28 08:46:05.020000', NULL, NULL),
(3, 'plugin::users-permissions.auth.callback', '2024-02-28 08:46:05.035000', '2024-02-28 08:46:05.035000', NULL, NULL),
(4, 'plugin::users-permissions.auth.connect', '2024-02-28 08:46:05.035000', '2024-02-28 08:46:05.035000', NULL, NULL),
(5, 'plugin::users-permissions.auth.forgotPassword', '2024-02-28 08:46:05.035000', '2024-02-28 08:46:05.035000', NULL, NULL),
(6, 'plugin::users-permissions.auth.resetPassword', '2024-02-28 08:46:05.035000', '2024-02-28 08:46:05.035000', NULL, NULL),
(7, 'plugin::users-permissions.auth.register', '2024-02-28 08:46:05.035000', '2024-02-28 08:46:05.035000', NULL, NULL),
(8, 'plugin::users-permissions.auth.emailConfirmation', '2024-02-28 08:46:05.035000', '2024-02-28 08:46:05.035000', NULL, NULL),
(9, 'plugin::users-permissions.auth.sendEmailConfirmation', '2024-02-28 08:46:05.035000', '2024-02-28 08:46:05.035000', NULL, NULL),
(10, 'plugin::email.email.send', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(11, 'plugin::users-permissions.auth.callback', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(12, 'plugin::users-permissions.auth.changePassword', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(13, 'plugin::users-permissions.auth.sendEmailConfirmation', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(14, 'plugin::users-permissions.auth.resetPassword', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(15, 'plugin::users-permissions.auth.connect', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(16, 'plugin::users-permissions.auth.register', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(17, 'plugin::users-permissions.auth.forgotPassword', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(18, 'plugin::users-permissions.auth.emailConfirmation', '2024-02-28 09:20:17.172000', '2024-02-28 09:20:17.172000', NULL, NULL),
(19, 'plugin::email.email.send', '2024-02-28 09:20:47.281000', '2024-02-28 09:20:47.281000', NULL, NULL),
(20, 'plugin::users-permissions.auth.changePassword', '2024-02-28 09:20:47.282000', '2024-02-28 09:20:47.282000', NULL, NULL),
(21, 'plugin::content-type-builder.components.getComponents', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(22, 'plugin::content-type-builder.components.getComponent', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(23, 'plugin::content-type-builder.content-types.getContentType', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(24, 'plugin::content-type-builder.content-types.getContentTypes', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(25, 'plugin::email.email.send', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(26, 'plugin::upload.content-api.find', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(27, 'plugin::upload.content-api.findOne', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(28, 'plugin::upload.content-api.destroy', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(29, 'plugin::upload.content-api.upload', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(30, 'plugin::i18n.locales.listLocales', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(31, 'plugin::users-permissions.auth.callback', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(32, 'plugin::users-permissions.auth.resetPassword', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(33, 'plugin::users-permissions.auth.connect', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(34, 'plugin::users-permissions.auth.forgotPassword', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(35, 'plugin::users-permissions.auth.register', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(36, 'plugin::users-permissions.auth.emailConfirmation', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(37, 'plugin::users-permissions.auth.sendEmailConfirmation', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(38, 'plugin::users-permissions.user.create', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(39, 'plugin::users-permissions.user.update', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(40, 'plugin::users-permissions.user.find', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(41, 'plugin::users-permissions.user.findOne', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(42, 'plugin::users-permissions.user.count', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(43, 'plugin::users-permissions.user.destroy', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(44, 'plugin::users-permissions.role.createRole', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(45, 'plugin::users-permissions.role.findOne', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(46, 'plugin::users-permissions.role.find', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(47, 'plugin::users-permissions.role.updateRole', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(48, 'plugin::users-permissions.role.deleteRole', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(49, 'plugin::users-permissions.permissions.getPermissions', '2024-02-28 09:21:20.166000', '2024-02-28 09:21:20.166000', NULL, NULL),
(50, 'api::aseguradora.aseguradora.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(51, 'api::aseguradora.aseguradora.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(52, 'api::aseguradora.aseguradora.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(53, 'api::aseguradora.aseguradora.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(54, 'api::aseguradora.aseguradora.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(55, 'api::cargas-combustible.cargas-combustible.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(56, 'api::cargas-combustible.cargas-combustible.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(57, 'api::cargas-combustible.cargas-combustible.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(58, 'api::cargas-combustible.cargas-combustible.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(59, 'api::cargas-combustible.cargas-combustible.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(60, 'api::estado-parada.estado-parada.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(61, 'api::estado-parada.estado-parada.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(62, 'api::estado-parada.estado-parada.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(63, 'api::estado-parada.estado-parada.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(64, 'api::estado-parada.estado-parada.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(70, 'api::marca-vehiculo.marca-vehiculo.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(71, 'api::marca-vehiculo.marca-vehiculo.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(72, 'api::marca-vehiculo.marca-vehiculo.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(73, 'api::marca-vehiculo.marca-vehiculo.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(74, 'api::marca-vehiculo.marca-vehiculo.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(75, 'api::modelo-vehiculo.modelo-vehiculo.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(76, 'api::modelo-vehiculo.modelo-vehiculo.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(77, 'api::modelo-vehiculo.modelo-vehiculo.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(78, 'api::modelo-vehiculo.modelo-vehiculo.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(79, 'api::modelo-vehiculo.modelo-vehiculo.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(80, 'api::parada.parada.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(81, 'api::parada.parada.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(82, 'api::parada.parada.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(83, 'api::parada.parada.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(84, 'api::parada.parada.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(85, 'api::poliza-seguro.poliza-seguro.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(86, 'api::poliza-seguro.poliza-seguro.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(87, 'api::poliza-seguro.poliza-seguro.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(88, 'api::poliza-seguro.poliza-seguro.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(89, 'api::ruta.ruta.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(90, 'api::ruta.ruta.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(91, 'api::poliza-seguro.poliza-seguro.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(92, 'api::ruta.ruta.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(93, 'api::ruta.ruta.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(94, 'api::ruta.ruta.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(95, 'api::tipo-cobertura.tipo-cobertura.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(96, 'api::tipo-cobertura.tipo-cobertura.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(97, 'api::tipo-cobertura.tipo-cobertura.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(98, 'api::tipo-cobertura.tipo-cobertura.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(99, 'api::tipo-cobertura.tipo-cobertura.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(100, 'api::tipo-vehiculo.tipo-vehiculo.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(101, 'api::tipo-vehiculo.tipo-vehiculo.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(102, 'api::tipo-vehiculo.tipo-vehiculo.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(103, 'api::tipo-vehiculo.tipo-vehiculo.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(104, 'api::tipo-vehiculo.tipo-vehiculo.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(105, 'api::vehiculo.vehiculo.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(106, 'api::vehiculo.vehiculo.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(107, 'api::vehiculo.vehiculo.create', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(108, 'api::vehiculo.vehiculo.update', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(109, 'api::vehiculo.vehiculo.delete', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(110, 'plugin::users-permissions.user.find', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(111, 'plugin::users-permissions.user.findOne', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(112, 'plugin::users-permissions.user.me', '2024-02-29 12:15:09.390000', '2024-02-29 12:15:09.390000', NULL, NULL),
(113, 'api::aseguradora.aseguradora.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(114, 'api::aseguradora.aseguradora.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(115, 'api::aseguradora.aseguradora.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(116, 'api::aseguradora.aseguradora.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(117, 'api::aseguradora.aseguradora.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(118, 'api::cargas-combustible.cargas-combustible.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(119, 'api::cargas-combustible.cargas-combustible.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(120, 'api::cargas-combustible.cargas-combustible.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(121, 'api::cargas-combustible.cargas-combustible.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(122, 'api::cargas-combustible.cargas-combustible.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(123, 'api::estado-parada.estado-parada.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(124, 'api::estado-parada.estado-parada.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(125, 'api::estado-parada.estado-parada.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(126, 'api::estado-parada.estado-parada.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(128, 'api::marca-vehiculo.marca-vehiculo.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(129, 'api::marca-vehiculo.marca-vehiculo.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(132, 'api::estado-parada.estado-parada.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(133, 'api::marca-vehiculo.marca-vehiculo.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(136, 'api::marca-vehiculo.marca-vehiculo.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(137, 'api::marca-vehiculo.marca-vehiculo.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(138, 'api::modelo-vehiculo.modelo-vehiculo.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(139, 'api::modelo-vehiculo.modelo-vehiculo.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(140, 'api::modelo-vehiculo.modelo-vehiculo.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(141, 'api::modelo-vehiculo.modelo-vehiculo.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(142, 'api::modelo-vehiculo.modelo-vehiculo.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(143, 'api::parada.parada.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(144, 'api::parada.parada.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(145, 'api::parada.parada.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(146, 'api::parada.parada.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(147, 'api::parada.parada.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(148, 'api::poliza-seguro.poliza-seguro.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(149, 'api::poliza-seguro.poliza-seguro.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(150, 'api::poliza-seguro.poliza-seguro.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(151, 'api::poliza-seguro.poliza-seguro.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(152, 'api::poliza-seguro.poliza-seguro.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(153, 'api::ruta.ruta.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(154, 'api::ruta.ruta.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(155, 'api::ruta.ruta.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(156, 'api::ruta.ruta.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(157, 'api::ruta.ruta.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(158, 'api::tipo-cobertura.tipo-cobertura.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(159, 'api::tipo-cobertura.tipo-cobertura.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(160, 'api::tipo-cobertura.tipo-cobertura.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(161, 'api::tipo-cobertura.tipo-cobertura.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(162, 'api::tipo-cobertura.tipo-cobertura.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(163, 'api::tipo-vehiculo.tipo-vehiculo.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(164, 'api::tipo-vehiculo.tipo-vehiculo.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(165, 'api::tipo-vehiculo.tipo-vehiculo.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(166, 'api::tipo-vehiculo.tipo-vehiculo.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(167, 'api::tipo-vehiculo.tipo-vehiculo.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(168, 'api::vehiculo.vehiculo.find', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(169, 'api::vehiculo.vehiculo.findOne', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(170, 'api::vehiculo.vehiculo.create', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(171, 'api::vehiculo.vehiculo.update', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL),
(172, 'api::vehiculo.vehiculo.delete', '2024-02-29 12:15:54.676000', '2024-02-29 12:15:54.676000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `up_permissions_role_links`
--

CREATE TABLE `up_permissions_role_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `permission_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `up_permissions_role_links`
--

INSERT INTO `up_permissions_role_links` (`id`, `permission_id`, `role_id`, `permission_order`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 2, 1),
(4, 7, 2, 1),
(5, 8, 2, 1),
(6, 6, 2, 1),
(7, 5, 2, 1),
(8, 4, 2, 2),
(9, 9, 2, 2),
(10, 10, 3, 1),
(11, 12, 3, 1),
(12, 11, 3, 1),
(13, 13, 3, 2),
(14, 17, 3, 2),
(15, 16, 3, 2),
(16, 15, 3, 2),
(17, 14, 3, 2),
(18, 18, 3, 3),
(19, 19, 2, 3),
(20, 20, 2, 3),
(21, 21, 1, 2),
(22, 22, 1, 2),
(23, 23, 1, 2),
(24, 26, 1, 2),
(25, 27, 1, 2),
(26, 24, 1, 2),
(27, 25, 1, 2),
(28, 28, 1, 2),
(29, 32, 1, 2),
(30, 29, 1, 3),
(31, 31, 1, 3),
(32, 33, 1, 3),
(33, 30, 1, 3),
(34, 37, 1, 3),
(35, 35, 1, 3),
(36, 36, 1, 3),
(37, 34, 1, 4),
(38, 38, 1, 4),
(39, 44, 1, 4),
(40, 45, 1, 4),
(41, 42, 1, 4),
(42, 41, 1, 4),
(43, 40, 1, 4),
(44, 43, 1, 4),
(45, 39, 1, 4),
(46, 46, 1, 5),
(47, 47, 1, 5),
(48, 48, 1, 5),
(49, 49, 1, 5),
(50, 52, 3, 4),
(51, 54, 3, 4),
(52, 50, 3, 4),
(53, 51, 3, 4),
(54, 61, 3, 4),
(55, 63, 3, 4),
(56, 55, 3, 4),
(57, 56, 3, 4),
(58, 53, 3, 4),
(59, 58, 3, 5),
(60, 57, 3, 5),
(61, 59, 3, 5),
(65, 60, 3, 5),
(66, 62, 3, 5),
(67, 72, 3, 6),
(68, 73, 3, 6),
(69, 64, 3, 6),
(71, 70, 3, 6),
(72, 71, 3, 6),
(74, 79, 3, 7),
(75, 74, 3, 7),
(76, 78, 3, 7),
(77, 75, 3, 7),
(78, 77, 3, 7),
(79, 84, 3, 8),
(80, 85, 3, 8),
(81, 80, 3, 8),
(82, 81, 3, 8),
(83, 76, 3, 8),
(84, 82, 3, 9),
(85, 83, 3, 9),
(86, 89, 3, 9),
(87, 90, 3, 9),
(88, 92, 3, 10),
(89, 87, 3, 10),
(90, 86, 3, 10),
(91, 96, 3, 11),
(92, 91, 3, 11),
(93, 88, 3, 10),
(94, 97, 3, 11),
(95, 94, 3, 11),
(96, 95, 3, 11),
(97, 103, 3, 11),
(98, 93, 3, 12),
(99, 100, 3, 12),
(100, 98, 3, 13),
(101, 104, 3, 12),
(102, 99, 3, 13),
(103, 101, 3, 13),
(104, 108, 3, 13),
(105, 109, 3, 13),
(106, 110, 3, 13),
(107, 102, 3, 14),
(108, 105, 3, 14),
(109, 111, 3, 14),
(110, 106, 3, 15),
(111, 107, 3, 15),
(112, 112, 3, 15),
(113, 113, 1, 6),
(114, 114, 1, 6),
(115, 116, 1, 6),
(116, 117, 1, 6),
(117, 118, 1, 6),
(118, 119, 1, 6),
(119, 120, 1, 6),
(120, 122, 1, 7),
(121, 121, 1, 7),
(122, 125, 1, 7),
(123, 126, 1, 7),
(124, 123, 1, 7),
(125, 124, 1, 7),
(126, 115, 1, 7),
(127, 132, 1, 8),
(130, 133, 1, 9),
(132, 138, 1, 9),
(133, 129, 1, 9),
(135, 128, 1, 10),
(137, 137, 1, 10),
(138, 136, 1, 10),
(139, 143, 1, 10),
(140, 144, 1, 10),
(141, 142, 1, 10),
(142, 145, 1, 11),
(143, 139, 1, 11),
(144, 141, 1, 11),
(145, 140, 1, 12),
(146, 149, 1, 12),
(147, 150, 1, 12),
(148, 147, 1, 12),
(149, 148, 1, 13),
(150, 146, 1, 13),
(151, 157, 1, 13),
(152, 158, 1, 13),
(153, 151, 1, 13),
(154, 153, 1, 14),
(155, 156, 1, 13),
(156, 152, 1, 14),
(157, 154, 1, 14),
(158, 155, 1, 14),
(159, 161, 1, 14),
(160, 162, 1, 14),
(161, 163, 1, 15),
(162, 164, 1, 15),
(163, 165, 1, 16),
(164, 166, 1, 17),
(165, 159, 1, 17),
(166, 160, 1, 17),
(167, 169, 1, 17),
(168, 170, 1, 17),
(169, 167, 1, 17),
(170, 168, 1, 17),
(171, 171, 1, 17),
(172, 172, 1, 17);

-- --------------------------------------------------------

--
-- Table structure for table `up_roles`
--

CREATE TABLE `up_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `up_roles`
--

INSERT INTO `up_roles` (`id`, `name`, `description`, `type`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`) VALUES
(1, 'Authenticated', 'Default role given to authenticated user.', 'authenticated', '2024-02-28 08:46:05.005000', '2024-02-29 12:15:54.662000', NULL, NULL),
(2, 'Public', 'Default role given to unauthenticated user.', 'public', '2024-02-28 08:46:05.010000', '2024-02-28 09:20:56.281000', NULL, NULL),
(3, 'Driver', 'Driver of the vehicles.', 'driver', '2024-02-28 09:20:17.165000', '2024-02-29 12:15:09.376000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `up_users`
--

CREATE TABLE `up_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL,
  `num` varchar(255) DEFAULT NULL,
  `nombre_pila` varchar(255) DEFAULT NULL,
  `ap_paterno` varchar(255) DEFAULT NULL,
  `num_tel` varchar(255) DEFAULT NULL,
  `ap_materno` varchar(255) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `up_users`
--

INSERT INTO `up_users` (`id`, `username`, `email`, `provider`, `password`, `reset_password_token`, `confirmation_token`, `confirmed`, `blocked`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`, `num`, `nombre_pila`, `ap_paterno`, `num_tel`, `ap_materno`, `published_at`) VALUES
(1, 'WayneDev', '0322103790@ut-tijuana.edu.mx', 'local', '$2a$10$BTURB4zcf/C6EwkybMTlk.upCNAJp0CFW/xKvNnRFJ.UPzzP9lOVS', '63d6abd7a9bd949ba0fc2401a365d056008715bf68aca933758d49dc0998e7683520d7136575a6768fa8015eee4026482a2fdcfdd7da1195d257bcfef83af314', NULL, 1, 0, '2024-02-28 09:23:45.823000', '2024-03-01 11:50:50.767000', 1, 1, '1', 'Jose de Jesus', 'Ponce', '6632080121', 'Duarte', '2024-02-28 09:23:45.823000'),
(2, 'BruceWayne', 'joseph.thecrack117@gmail.com', 'local', '$2a$10$1.thH7OgjaBKwDMoW2N6YexWr49MAnXuvRaHbQ3m7nkABtkmdZQHW', 'd4db9be4dc2120843ec00935a5c069e47b38d26e6089f250e8f54e6e21afc5128a8685f2cc21cdb076883391c4e766f9da7f47d5a5c65ddd993494d8065f2129', NULL, 1, 0, '2024-02-29 09:35:22.218000', '2024-02-29 13:17:54.521000', 1, 1, '2', 'Bruce', 'Wayne', '6647556974', 'Diaz', '2024-02-29 09:35:28.880000');

-- --------------------------------------------------------

--
-- Table structure for table `up_users_role_links`
--

CREATE TABLE `up_users_role_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `user_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `up_users_role_links`
--

INSERT INTO `up_users_role_links` (`id`, `user_id`, `role_id`, `user_order`) VALUES
(1, 1, 1, 1),
(3, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `up_users_rutas_users_links`
--

CREATE TABLE `up_users_rutas_users_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ruta_id` int(10) UNSIGNED DEFAULT NULL,
  `ruta_order` double UNSIGNED DEFAULT NULL,
  `user_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `up_users_rutas_users_links`
--

INSERT INTO `up_users_rutas_users_links` (`id`, `user_id`, `ruta_id`, `ruta_order`, `user_order`) VALUES
(2, 2, 1, 1, 1),
(3, 1, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `up_users_usuarios_vehiculos_links`
--

CREATE TABLE `up_users_usuarios_vehiculos_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `vehiculo_order` double UNSIGNED DEFAULT NULL,
  `user_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `up_users_usuarios_vehiculos_links`
--

INSERT INTO `up_users_usuarios_vehiculos_links` (`id`, `user_id`, `vehiculo_id`, `vehiculo_order`, `user_order`) VALUES
(1, 2, 2, 1, 1),
(2, 1, 1, 1, 1);

--
-- Triggers `up_users_usuarios_vehiculos_links`
--
DELIMITER $$
CREATE TRIGGER `verificarBorradoVehiculo` BEFORE DELETE ON `up_users_usuarios_vehiculos_links` FOR EACH ROW BEGIN
    DECLARE cantUsuarios INT;
    
    SET cantUsuarios = (
        SELECT COUNT(*)
        FROM up_users_usuarios_vehiculos_links
        WHERE vehiculo_id = OLD.vehiculo_id
    );
    
    IF (cantUsuarios = 1) THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'No se puede borrar el vehículo porque está asociado a un usuario';
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` int(10) UNSIGNED NOT NULL,
  `num_serie` varchar(255) DEFAULT NULL,
  `placas` varchar(255) DEFAULT NULL,
  `kilometraje` bigint(20) DEFAULT NULL,
  `fecha_adquisicion` datetime(6) DEFAULT NULL,
  `usa_diesel` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `published_at` datetime(6) DEFAULT NULL,
  `created_by_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `num_serie`, `placas`, `kilometraje`, `fecha_adquisicion`, `usa_diesel`, `created_at`, `updated_at`, `published_at`, `created_by_id`, `updated_by_id`) VALUES
(1, '503218764', 'LMN456J', 42750, '2023-02-28 11:55:29.000000', 0, NULL, '2024-02-29 12:54:58.706000', '2024-02-29 11:58:19.170000', NULL, 1),
(2, '872345619', 'ABC123X', 25000, '2023-01-15 00:00:00.000000', 0, '2024-02-29 11:51:23.959000', '2024-02-29 12:55:25.707000', '2024-02-29 11:53:22.180000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehiculos_marca_links`
--

CREATE TABLE `vehiculos_marca_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `marca_vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `marca_vehiculo_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehiculos_modelo_links`
--

CREATE TABLE `vehiculos_modelo_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `modelo_vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `modelo_vehiculo_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehiculos_tipo_links`
--

CREATE TABLE `vehiculos_tipo_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `tipo_vehiculo_id` int(10) UNSIGNED DEFAULT NULL,
  `tipo_vehiculo_order` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_permissions_created_by_id_fk` (`created_by_id`),
  ADD KEY `admin_permissions_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `admin_permissions_role_links`
--
ALTER TABLE `admin_permissions_role_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_role_links_unique` (`permission_id`,`role_id`),
  ADD KEY `admin_permissions_role_links_fk` (`permission_id`),
  ADD KEY `admin_permissions_role_links_inv_fk` (`role_id`),
  ADD KEY `admin_permissions_role_links_order_inv_fk` (`permission_order`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_roles_created_by_id_fk` (`created_by_id`),
  ADD KEY `admin_roles_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_users_created_by_id_fk` (`created_by_id`),
  ADD KEY `admin_users_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `admin_users_roles_links`
--
ALTER TABLE `admin_users_roles_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_roles_links_unique` (`user_id`,`role_id`),
  ADD KEY `admin_users_roles_links_fk` (`user_id`),
  ADD KEY `admin_users_roles_links_inv_fk` (`role_id`),
  ADD KEY `admin_users_roles_links_order_fk` (`role_order`),
  ADD KEY `admin_users_roles_links_order_inv_fk` (`user_order`);

--
-- Indexes for table `aseguradoras`
--
ALTER TABLE `aseguradoras`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aseguradoras_num_unique` (`num`),
  ADD KEY `aseguradoras_created_by_id_fk` (`created_by_id`),
  ADD KEY `aseguradoras_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `cargas_combustibles`
--
ALTER TABLE `cargas_combustibles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cargas_combustibles_num_unique` (`num`),
  ADD KEY `cargas_combustibles_created_by_id_fk` (`created_by_id`),
  ADD KEY `cargas_combustibles_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `cargas_combustibles_vehiculo_links`
--
ALTER TABLE `cargas_combustibles_vehiculo_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cargas_combustibles_vehiculo_links_unique` (`cargas_combustible_id`,`vehiculo_id`),
  ADD KEY `cargas_combustibles_vehiculo_links_fk` (`cargas_combustible_id`),
  ADD KEY `cargas_combustibles_vehiculo_links_inv_fk` (`vehiculo_id`),
  ADD KEY `cargas_combustibles_vehiculo_links_order_fk` (`vehiculo_order`);

--
-- Indexes for table `estados_paradas`
--
ALTER TABLE `estados_paradas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `estados_paradas_num_unique` (`num`),
  ADD KEY `estados_paradas_created_by_id_fk` (`created_by_id`),
  ADD KEY `estados_paradas_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `upload_files_folder_path_index` (`folder_path`),
  ADD KEY `upload_files_created_at_index` (`created_at`),
  ADD KEY `upload_files_updated_at_index` (`updated_at`),
  ADD KEY `upload_files_name_index` (`name`),
  ADD KEY `upload_files_size_index` (`size`),
  ADD KEY `upload_files_ext_index` (`ext`),
  ADD KEY `files_created_by_id_fk` (`created_by_id`),
  ADD KEY `files_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `files_folder_links`
--
ALTER TABLE `files_folder_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `files_folder_links_unique` (`file_id`,`folder_id`),
  ADD KEY `files_folder_links_fk` (`file_id`),
  ADD KEY `files_folder_links_inv_fk` (`folder_id`),
  ADD KEY `files_folder_links_order_inv_fk` (`file_order`);

--
-- Indexes for table `files_related_morphs`
--
ALTER TABLE `files_related_morphs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_related_morphs_fk` (`file_id`),
  ADD KEY `files_related_morphs_order_index` (`order`),
  ADD KEY `files_related_morphs_id_column_index` (`related_id`);

--
-- Indexes for table `i18n_locale`
--
ALTER TABLE `i18n_locale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `i18n_locale_created_by_id_fk` (`created_by_id`),
  ADD KEY `i18n_locale_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `marcas_vehiculos`
--
ALTER TABLE `marcas_vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `marcas_vehiculos_num_unique` (`num`),
  ADD KEY `marcas_vehiculos_created_by_id_fk` (`created_by_id`),
  ADD KEY `marcas_vehiculos_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `modelos_vehiculos`
--
ALTER TABLE `modelos_vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `modelos_vehiculos_num_unique` (`num`),
  ADD KEY `modelos_vehiculos_created_by_id_fk` (`created_by_id`),
  ADD KEY `modelos_vehiculos_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `paradas`
--
ALTER TABLE `paradas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `paradas_num_unique` (`num`),
  ADD KEY `paradas_created_by_id_fk` (`created_by_id`),
  ADD KEY `paradas_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `polizas_seguros`
--
ALTER TABLE `polizas_seguros`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `polizas_seguros_num_poliza_unique` (`num_poliza`),
  ADD KEY `polizas_seguros_created_by_id_fk` (`created_by_id`),
  ADD KEY `polizas_seguros_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `polizas_seguros_aseguradora_links`
--
ALTER TABLE `polizas_seguros_aseguradora_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `polizas_seguros_aseguradora_links_unique` (`poliza_seguro_id`,`aseguradora_id`),
  ADD KEY `polizas_seguros_aseguradora_links_fk` (`poliza_seguro_id`),
  ADD KEY `polizas_seguros_aseguradora_links_inv_fk` (`aseguradora_id`),
  ADD KEY `polizas_seguros_aseguradora_links_order_fk` (`aseguradora_order`);

--
-- Indexes for table `polizas_seguros_cobertura_links`
--
ALTER TABLE `polizas_seguros_cobertura_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `polizas_seguros_cobertura_links_unique` (`poliza_seguro_id`,`tipo_cobertura_id`),
  ADD KEY `polizas_seguros_cobertura_links_fk` (`poliza_seguro_id`),
  ADD KEY `polizas_seguros_cobertura_links_inv_fk` (`tipo_cobertura_id`),
  ADD KEY `polizas_seguros_cobertura_links_order_fk` (`tipo_cobertura_order`);

--
-- Indexes for table `polizas_seguros_vehiculo_links`
--
ALTER TABLE `polizas_seguros_vehiculo_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `polizas_seguros_vehiculo_links_unique` (`poliza_seguro_id`,`vehiculo_id`),
  ADD KEY `polizas_seguros_vehiculo_links_fk` (`poliza_seguro_id`),
  ADD KEY `polizas_seguros_vehiculo_links_inv_fk` (`vehiculo_id`),
  ADD KEY `polizas_seguros_vehiculo_links_order_fk` (`vehiculo_order`);

--
-- Indexes for table `rutas`
--
ALTER TABLE `rutas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rutas_num_unique` (`num`),
  ADD KEY `rutas_created_by_id_fk` (`created_by_id`),
  ADD KEY `rutas_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `rutas_paradas_rutas_links`
--
ALTER TABLE `rutas_paradas_rutas_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rutas_paradas_rutas_links_unique` (`ruta_id`,`parada_id`),
  ADD KEY `rutas_paradas_rutas_links_fk` (`ruta_id`),
  ADD KEY `rutas_paradas_rutas_links_inv_fk` (`parada_id`),
  ADD KEY `rutas_paradas_rutas_links_order_fk` (`parada_order`),
  ADD KEY `rutas_paradas_rutas_links_order_inv_fk` (`ruta_order`);

--
-- Indexes for table `strapi_api_tokens`
--
ALTER TABLE `strapi_api_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `strapi_api_tokens_created_by_id_fk` (`created_by_id`),
  ADD KEY `strapi_api_tokens_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `strapi_api_token_permissions`
--
ALTER TABLE `strapi_api_token_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `strapi_api_token_permissions_created_by_id_fk` (`created_by_id`),
  ADD KEY `strapi_api_token_permissions_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `strapi_api_token_permissions_token_links`
--
ALTER TABLE `strapi_api_token_permissions_token_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `strapi_api_token_permissions_token_links_unique` (`api_token_permission_id`,`api_token_id`),
  ADD KEY `strapi_api_token_permissions_token_links_fk` (`api_token_permission_id`),
  ADD KEY `strapi_api_token_permissions_token_links_inv_fk` (`api_token_id`),
  ADD KEY `strapi_api_token_permissions_token_links_order_inv_fk` (`api_token_permission_order`);

--
-- Indexes for table `strapi_core_store_settings`
--
ALTER TABLE `strapi_core_store_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `strapi_database_schema`
--
ALTER TABLE `strapi_database_schema`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `strapi_migrations`
--
ALTER TABLE `strapi_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `strapi_releases`
--
ALTER TABLE `strapi_releases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `strapi_releases_created_by_id_fk` (`created_by_id`),
  ADD KEY `strapi_releases_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `strapi_release_actions`
--
ALTER TABLE `strapi_release_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `strapi_release_actions_created_by_id_fk` (`created_by_id`),
  ADD KEY `strapi_release_actions_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `strapi_release_actions_release_links`
--
ALTER TABLE `strapi_release_actions_release_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `strapi_release_actions_release_links_unique` (`release_action_id`,`release_id`),
  ADD KEY `strapi_release_actions_release_links_fk` (`release_action_id`),
  ADD KEY `strapi_release_actions_release_links_inv_fk` (`release_id`),
  ADD KEY `strapi_release_actions_release_links_order_inv_fk` (`release_action_order`);

--
-- Indexes for table `strapi_transfer_tokens`
--
ALTER TABLE `strapi_transfer_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `strapi_transfer_tokens_created_by_id_fk` (`created_by_id`),
  ADD KEY `strapi_transfer_tokens_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `strapi_transfer_token_permissions`
--
ALTER TABLE `strapi_transfer_token_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `strapi_transfer_token_permissions_created_by_id_fk` (`created_by_id`),
  ADD KEY `strapi_transfer_token_permissions_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `strapi_transfer_token_permissions_token_links`
--
ALTER TABLE `strapi_transfer_token_permissions_token_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `strapi_transfer_token_permissions_token_links_unique` (`transfer_token_permission_id`,`transfer_token_id`),
  ADD KEY `strapi_transfer_token_permissions_token_links_fk` (`transfer_token_permission_id`),
  ADD KEY `strapi_transfer_token_permissions_token_links_inv_fk` (`transfer_token_id`),
  ADD KEY `strapi_transfer_token_permissions_token_links_order_inv_fk` (`transfer_token_permission_order`);

--
-- Indexes for table `strapi_webhooks`
--
ALTER TABLE `strapi_webhooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipos_coberturas`
--
ALTER TABLE `tipos_coberturas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tipos_coberturas_num_unique` (`num`),
  ADD KEY `tipos_coberturas_created_by_id_fk` (`created_by_id`),
  ADD KEY `tipos_coberturas_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `tipos_vehiculos`
--
ALTER TABLE `tipos_vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tipos_vehiculos_num_unique` (`num`),
  ADD KEY `tipos_vehiculos_created_by_id_fk` (`created_by_id`),
  ADD KEY `tipos_vehiculos_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `upload_folders`
--
ALTER TABLE `upload_folders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `upload_folders_path_id_index` (`path_id`),
  ADD UNIQUE KEY `upload_folders_path_index` (`path`),
  ADD KEY `upload_folders_created_by_id_fk` (`created_by_id`),
  ADD KEY `upload_folders_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `upload_folders_parent_links`
--
ALTER TABLE `upload_folders_parent_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `upload_folders_parent_links_unique` (`folder_id`,`inv_folder_id`),
  ADD KEY `upload_folders_parent_links_fk` (`folder_id`),
  ADD KEY `upload_folders_parent_links_inv_fk` (`inv_folder_id`),
  ADD KEY `upload_folders_parent_links_order_inv_fk` (`folder_order`);

--
-- Indexes for table `up_permissions`
--
ALTER TABLE `up_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `up_permissions_created_by_id_fk` (`created_by_id`),
  ADD KEY `up_permissions_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `up_permissions_role_links`
--
ALTER TABLE `up_permissions_role_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `up_permissions_role_links_unique` (`permission_id`,`role_id`),
  ADD KEY `up_permissions_role_links_fk` (`permission_id`),
  ADD KEY `up_permissions_role_links_inv_fk` (`role_id`),
  ADD KEY `up_permissions_role_links_order_inv_fk` (`permission_order`);

--
-- Indexes for table `up_roles`
--
ALTER TABLE `up_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `up_roles_created_by_id_fk` (`created_by_id`),
  ADD KEY `up_roles_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `up_users`
--
ALTER TABLE `up_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `up_users_num_unique` (`num`),
  ADD KEY `up_users_created_by_id_fk` (`created_by_id`),
  ADD KEY `up_users_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `up_users_role_links`
--
ALTER TABLE `up_users_role_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `up_users_role_links_unique` (`user_id`,`role_id`),
  ADD KEY `up_users_role_links_fk` (`user_id`),
  ADD KEY `up_users_role_links_inv_fk` (`role_id`),
  ADD KEY `up_users_role_links_order_inv_fk` (`user_order`);

--
-- Indexes for table `up_users_rutas_users_links`
--
ALTER TABLE `up_users_rutas_users_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `up_users_rutas_users_links_unique` (`user_id`,`ruta_id`),
  ADD KEY `up_users_rutas_users_links_fk` (`user_id`),
  ADD KEY `up_users_rutas_users_links_inv_fk` (`ruta_id`),
  ADD KEY `up_users_rutas_users_links_order_fk` (`ruta_order`),
  ADD KEY `up_users_rutas_users_links_order_inv_fk` (`user_order`);

--
-- Indexes for table `up_users_usuarios_vehiculos_links`
--
ALTER TABLE `up_users_usuarios_vehiculos_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `up_users_usuarios_vehiculos_links_unique` (`user_id`,`vehiculo_id`),
  ADD KEY `up_users_usuarios_vehiculos_links_fk` (`user_id`),
  ADD KEY `up_users_usuarios_vehiculos_links_inv_fk` (`vehiculo_id`),
  ADD KEY `up_users_usuarios_vehiculos_links_order_fk` (`vehiculo_order`),
  ADD KEY `up_users_usuarios_vehiculos_links_order_inv_fk` (`user_order`);

--
-- Indexes for table `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vehiculos_num_serie_unique` (`num_serie`),
  ADD KEY `vehiculos_created_by_id_fk` (`created_by_id`),
  ADD KEY `vehiculos_updated_by_id_fk` (`updated_by_id`);

--
-- Indexes for table `vehiculos_marca_links`
--
ALTER TABLE `vehiculos_marca_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vehiculos_marca_links_unique` (`vehiculo_id`,`marca_vehiculo_id`),
  ADD KEY `vehiculos_marca_links_fk` (`vehiculo_id`),
  ADD KEY `vehiculos_marca_links_inv_fk` (`marca_vehiculo_id`),
  ADD KEY `vehiculos_marca_links_order_fk` (`marca_vehiculo_order`);

--
-- Indexes for table `vehiculos_modelo_links`
--
ALTER TABLE `vehiculos_modelo_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vehiculos_modelo_links_unique` (`vehiculo_id`,`modelo_vehiculo_id`),
  ADD KEY `vehiculos_modelo_links_fk` (`vehiculo_id`),
  ADD KEY `vehiculos_modelo_links_inv_fk` (`modelo_vehiculo_id`),
  ADD KEY `vehiculos_modelo_links_order_fk` (`modelo_vehiculo_order`);

--
-- Indexes for table `vehiculos_tipo_links`
--
ALTER TABLE `vehiculos_tipo_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vehiculos_tipo_links_unique` (`vehiculo_id`,`tipo_vehiculo_id`),
  ADD KEY `vehiculos_tipo_links_fk` (`vehiculo_id`),
  ADD KEY `vehiculos_tipo_links_inv_fk` (`tipo_vehiculo_id`),
  ADD KEY `vehiculos_tipo_links_order_fk` (`tipo_vehiculo_order`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `admin_permissions_role_links`
--
ALTER TABLE `admin_permissions_role_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_users_roles_links`
--
ALTER TABLE `admin_users_roles_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `aseguradoras`
--
ALTER TABLE `aseguradoras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cargas_combustibles`
--
ALTER TABLE `cargas_combustibles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cargas_combustibles_vehiculo_links`
--
ALTER TABLE `cargas_combustibles_vehiculo_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `estados_paradas`
--
ALTER TABLE `estados_paradas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files_folder_links`
--
ALTER TABLE `files_folder_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files_related_morphs`
--
ALTER TABLE `files_related_morphs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `i18n_locale`
--
ALTER TABLE `i18n_locale`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `marcas_vehiculos`
--
ALTER TABLE `marcas_vehiculos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `modelos_vehiculos`
--
ALTER TABLE `modelos_vehiculos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `paradas`
--
ALTER TABLE `paradas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `polizas_seguros`
--
ALTER TABLE `polizas_seguros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `polizas_seguros_aseguradora_links`
--
ALTER TABLE `polizas_seguros_aseguradora_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `polizas_seguros_cobertura_links`
--
ALTER TABLE `polizas_seguros_cobertura_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `polizas_seguros_vehiculo_links`
--
ALTER TABLE `polizas_seguros_vehiculo_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rutas`
--
ALTER TABLE `rutas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rutas_paradas_rutas_links`
--
ALTER TABLE `rutas_paradas_rutas_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `strapi_api_tokens`
--
ALTER TABLE `strapi_api_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_api_token_permissions`
--
ALTER TABLE `strapi_api_token_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_api_token_permissions_token_links`
--
ALTER TABLE `strapi_api_token_permissions_token_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_core_store_settings`
--
ALTER TABLE `strapi_core_store_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `strapi_database_schema`
--
ALTER TABLE `strapi_database_schema`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `strapi_migrations`
--
ALTER TABLE `strapi_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_releases`
--
ALTER TABLE `strapi_releases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_release_actions`
--
ALTER TABLE `strapi_release_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_release_actions_release_links`
--
ALTER TABLE `strapi_release_actions_release_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_transfer_tokens`
--
ALTER TABLE `strapi_transfer_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_transfer_token_permissions`
--
ALTER TABLE `strapi_transfer_token_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_transfer_token_permissions_token_links`
--
ALTER TABLE `strapi_transfer_token_permissions_token_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strapi_webhooks`
--
ALTER TABLE `strapi_webhooks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipos_coberturas`
--
ALTER TABLE `tipos_coberturas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tipos_vehiculos`
--
ALTER TABLE `tipos_vehiculos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `upload_folders`
--
ALTER TABLE `upload_folders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload_folders_parent_links`
--
ALTER TABLE `upload_folders_parent_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `up_permissions`
--
ALTER TABLE `up_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `up_permissions_role_links`
--
ALTER TABLE `up_permissions_role_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `up_roles`
--
ALTER TABLE `up_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `up_users`
--
ALTER TABLE `up_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `up_users_role_links`
--
ALTER TABLE `up_users_role_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `up_users_rutas_users_links`
--
ALTER TABLE `up_users_rutas_users_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `up_users_usuarios_vehiculos_links`
--
ALTER TABLE `up_users_usuarios_vehiculos_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehiculos_marca_links`
--
ALTER TABLE `vehiculos_marca_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vehiculos_modelo_links`
--
ALTER TABLE `vehiculos_modelo_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vehiculos_tipo_links`
--
ALTER TABLE `vehiculos_tipo_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD CONSTRAINT `admin_permissions_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `admin_permissions_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `admin_permissions_role_links`
--
ALTER TABLE `admin_permissions_role_links`
  ADD CONSTRAINT `admin_permissions_role_links_fk` FOREIGN KEY (`permission_id`) REFERENCES `admin_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `admin_permissions_role_links_inv_fk` FOREIGN KEY (`role_id`) REFERENCES `admin_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD CONSTRAINT `admin_roles_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `admin_roles_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD CONSTRAINT `admin_users_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `admin_users_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `admin_users_roles_links`
--
ALTER TABLE `admin_users_roles_links`
  ADD CONSTRAINT `admin_users_roles_links_fk` FOREIGN KEY (`user_id`) REFERENCES `admin_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `admin_users_roles_links_inv_fk` FOREIGN KEY (`role_id`) REFERENCES `admin_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `aseguradoras`
--
ALTER TABLE `aseguradoras`
  ADD CONSTRAINT `aseguradoras_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `aseguradoras_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cargas_combustibles`
--
ALTER TABLE `cargas_combustibles`
  ADD CONSTRAINT `cargas_combustibles_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `cargas_combustibles_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cargas_combustibles_vehiculo_links`
--
ALTER TABLE `cargas_combustibles_vehiculo_links`
  ADD CONSTRAINT `cargas_combustibles_vehiculo_links_fk` FOREIGN KEY (`cargas_combustible_id`) REFERENCES `cargas_combustibles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cargas_combustibles_vehiculo_links_inv_fk` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `estados_paradas`
--
ALTER TABLE `estados_paradas`
  ADD CONSTRAINT `estados_paradas_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `estados_paradas_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `files_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `files_folder_links`
--
ALTER TABLE `files_folder_links`
  ADD CONSTRAINT `files_folder_links_fk` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `files_folder_links_inv_fk` FOREIGN KEY (`folder_id`) REFERENCES `upload_folders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `files_related_morphs`
--
ALTER TABLE `files_related_morphs`
  ADD CONSTRAINT `files_related_morphs_fk` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `i18n_locale`
--
ALTER TABLE `i18n_locale`
  ADD CONSTRAINT `i18n_locale_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `i18n_locale_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `marcas_vehiculos`
--
ALTER TABLE `marcas_vehiculos`
  ADD CONSTRAINT `marcas_vehiculos_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `marcas_vehiculos_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `modelos_vehiculos`
--
ALTER TABLE `modelos_vehiculos`
  ADD CONSTRAINT `modelos_vehiculos_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `modelos_vehiculos_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `paradas`
--
ALTER TABLE `paradas`
  ADD CONSTRAINT `paradas_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `paradas_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `polizas_seguros`
--
ALTER TABLE `polizas_seguros`
  ADD CONSTRAINT `polizas_seguros_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `polizas_seguros_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `polizas_seguros_aseguradora_links`
--
ALTER TABLE `polizas_seguros_aseguradora_links`
  ADD CONSTRAINT `polizas_seguros_aseguradora_links_fk` FOREIGN KEY (`poliza_seguro_id`) REFERENCES `polizas_seguros` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `polizas_seguros_aseguradora_links_inv_fk` FOREIGN KEY (`aseguradora_id`) REFERENCES `aseguradoras` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `polizas_seguros_cobertura_links`
--
ALTER TABLE `polizas_seguros_cobertura_links`
  ADD CONSTRAINT `polizas_seguros_cobertura_links_fk` FOREIGN KEY (`poliza_seguro_id`) REFERENCES `polizas_seguros` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `polizas_seguros_cobertura_links_inv_fk` FOREIGN KEY (`tipo_cobertura_id`) REFERENCES `tipos_coberturas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `polizas_seguros_vehiculo_links`
--
ALTER TABLE `polizas_seguros_vehiculo_links`
  ADD CONSTRAINT `polizas_seguros_vehiculo_links_fk` FOREIGN KEY (`poliza_seguro_id`) REFERENCES `polizas_seguros` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `polizas_seguros_vehiculo_links_inv_fk` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rutas`
--
ALTER TABLE `rutas`
  ADD CONSTRAINT `rutas_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `rutas_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `rutas_paradas_rutas_links`
--
ALTER TABLE `rutas_paradas_rutas_links`
  ADD CONSTRAINT `rutas_paradas_rutas_links_fk` FOREIGN KEY (`ruta_id`) REFERENCES `rutas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rutas_paradas_rutas_links_inv_fk` FOREIGN KEY (`parada_id`) REFERENCES `paradas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `strapi_api_tokens`
--
ALTER TABLE `strapi_api_tokens`
  ADD CONSTRAINT `strapi_api_tokens_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `strapi_api_tokens_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `strapi_api_token_permissions`
--
ALTER TABLE `strapi_api_token_permissions`
  ADD CONSTRAINT `strapi_api_token_permissions_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `strapi_api_token_permissions_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `strapi_api_token_permissions_token_links`
--
ALTER TABLE `strapi_api_token_permissions_token_links`
  ADD CONSTRAINT `strapi_api_token_permissions_token_links_fk` FOREIGN KEY (`api_token_permission_id`) REFERENCES `strapi_api_token_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `strapi_api_token_permissions_token_links_inv_fk` FOREIGN KEY (`api_token_id`) REFERENCES `strapi_api_tokens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `strapi_releases`
--
ALTER TABLE `strapi_releases`
  ADD CONSTRAINT `strapi_releases_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `strapi_releases_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `strapi_release_actions`
--
ALTER TABLE `strapi_release_actions`
  ADD CONSTRAINT `strapi_release_actions_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `strapi_release_actions_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `strapi_release_actions_release_links`
--
ALTER TABLE `strapi_release_actions_release_links`
  ADD CONSTRAINT `strapi_release_actions_release_links_fk` FOREIGN KEY (`release_action_id`) REFERENCES `strapi_release_actions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `strapi_release_actions_release_links_inv_fk` FOREIGN KEY (`release_id`) REFERENCES `strapi_releases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `strapi_transfer_tokens`
--
ALTER TABLE `strapi_transfer_tokens`
  ADD CONSTRAINT `strapi_transfer_tokens_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `strapi_transfer_tokens_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `strapi_transfer_token_permissions`
--
ALTER TABLE `strapi_transfer_token_permissions`
  ADD CONSTRAINT `strapi_transfer_token_permissions_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `strapi_transfer_token_permissions_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `strapi_transfer_token_permissions_token_links`
--
ALTER TABLE `strapi_transfer_token_permissions_token_links`
  ADD CONSTRAINT `strapi_transfer_token_permissions_token_links_fk` FOREIGN KEY (`transfer_token_permission_id`) REFERENCES `strapi_transfer_token_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `strapi_transfer_token_permissions_token_links_inv_fk` FOREIGN KEY (`transfer_token_id`) REFERENCES `strapi_transfer_tokens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tipos_coberturas`
--
ALTER TABLE `tipos_coberturas`
  ADD CONSTRAINT `tipos_coberturas_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `tipos_coberturas_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `tipos_vehiculos`
--
ALTER TABLE `tipos_vehiculos`
  ADD CONSTRAINT `tipos_vehiculos_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `tipos_vehiculos_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `upload_folders`
--
ALTER TABLE `upload_folders`
  ADD CONSTRAINT `upload_folders_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `upload_folders_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `upload_folders_parent_links`
--
ALTER TABLE `upload_folders_parent_links`
  ADD CONSTRAINT `upload_folders_parent_links_fk` FOREIGN KEY (`folder_id`) REFERENCES `upload_folders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `upload_folders_parent_links_inv_fk` FOREIGN KEY (`inv_folder_id`) REFERENCES `upload_folders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `up_permissions`
--
ALTER TABLE `up_permissions`
  ADD CONSTRAINT `up_permissions_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `up_permissions_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `up_permissions_role_links`
--
ALTER TABLE `up_permissions_role_links`
  ADD CONSTRAINT `up_permissions_role_links_fk` FOREIGN KEY (`permission_id`) REFERENCES `up_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `up_permissions_role_links_inv_fk` FOREIGN KEY (`role_id`) REFERENCES `up_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `up_roles`
--
ALTER TABLE `up_roles`
  ADD CONSTRAINT `up_roles_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `up_roles_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `up_users`
--
ALTER TABLE `up_users`
  ADD CONSTRAINT `up_users_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `up_users_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `up_users_role_links`
--
ALTER TABLE `up_users_role_links`
  ADD CONSTRAINT `up_users_role_links_fk` FOREIGN KEY (`user_id`) REFERENCES `up_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `up_users_role_links_inv_fk` FOREIGN KEY (`role_id`) REFERENCES `up_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `up_users_rutas_users_links`
--
ALTER TABLE `up_users_rutas_users_links`
  ADD CONSTRAINT `up_users_rutas_users_links_fk` FOREIGN KEY (`user_id`) REFERENCES `up_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `up_users_rutas_users_links_inv_fk` FOREIGN KEY (`ruta_id`) REFERENCES `rutas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `up_users_usuarios_vehiculos_links`
--
ALTER TABLE `up_users_usuarios_vehiculos_links`
  ADD CONSTRAINT `up_users_usuarios_vehiculos_links_fk` FOREIGN KEY (`user_id`) REFERENCES `up_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `up_users_usuarios_vehiculos_links_inv_fk` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `vehiculos_created_by_id_fk` FOREIGN KEY (`created_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `vehiculos_updated_by_id_fk` FOREIGN KEY (`updated_by_id`) REFERENCES `admin_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `vehiculos_marca_links`
--
ALTER TABLE `vehiculos_marca_links`
  ADD CONSTRAINT `vehiculos_marca_links_fk` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `vehiculos_marca_links_inv_fk` FOREIGN KEY (`marca_vehiculo_id`) REFERENCES `marcas_vehiculos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehiculos_modelo_links`
--
ALTER TABLE `vehiculos_modelo_links`
  ADD CONSTRAINT `vehiculos_modelo_links_fk` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `vehiculos_modelo_links_inv_fk` FOREIGN KEY (`modelo_vehiculo_id`) REFERENCES `modelos_vehiculos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehiculos_tipo_links`
--
ALTER TABLE `vehiculos_tipo_links`
  ADD CONSTRAINT `vehiculos_tipo_links_fk` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `vehiculos_tipo_links_inv_fk` FOREIGN KEY (`tipo_vehiculo_id`) REFERENCES `tipos_vehiculos` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
