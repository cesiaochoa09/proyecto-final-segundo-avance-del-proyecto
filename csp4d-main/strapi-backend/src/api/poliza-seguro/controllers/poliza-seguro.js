'use strict';

/**
 * poliza-seguro controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::poliza-seguro.poliza-seguro');
