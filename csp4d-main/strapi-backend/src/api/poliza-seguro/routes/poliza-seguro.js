'use strict';

/**
 * poliza-seguro router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::poliza-seguro.poliza-seguro');
