'use strict';

/**
 * marca-vehiculo router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::marca-vehiculo.marca-vehiculo');
