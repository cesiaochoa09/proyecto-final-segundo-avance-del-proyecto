'use strict';

/**
 * marca-vehiculo service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::marca-vehiculo.marca-vehiculo');
