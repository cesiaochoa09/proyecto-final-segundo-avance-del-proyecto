'use strict';

/**
 * tipo-vehiculo service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::tipo-vehiculo.tipo-vehiculo');
