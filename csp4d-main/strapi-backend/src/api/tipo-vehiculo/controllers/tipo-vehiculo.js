'use strict';

/**
 * tipo-vehiculo controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::tipo-vehiculo.tipo-vehiculo');
