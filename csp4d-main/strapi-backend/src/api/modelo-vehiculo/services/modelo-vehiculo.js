'use strict';

/**
 * modelo-vehiculo service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::modelo-vehiculo.modelo-vehiculo');
