'use strict';

/**
 * estado-parada service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::estado-parada.estado-parada');
