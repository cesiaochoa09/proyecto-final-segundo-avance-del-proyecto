'use strict';

/**
 * parada router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::parada.parada');
