'use strict';

/**
 * parada service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::parada.parada');
