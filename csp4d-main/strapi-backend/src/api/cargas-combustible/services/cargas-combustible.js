'use strict';

/**
 * cargas-combustible service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::cargas-combustible.cargas-combustible');
