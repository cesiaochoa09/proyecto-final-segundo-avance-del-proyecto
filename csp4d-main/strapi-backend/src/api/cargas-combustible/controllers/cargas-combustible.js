'use strict';

/**
 * cargas-combustible controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::cargas-combustible.cargas-combustible');
