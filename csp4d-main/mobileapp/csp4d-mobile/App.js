import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from "native-base";
import { NativeBaseProvider } from "native-base";

import LoginScreen from './app/LoginScreen';

export default function App() {
  return (
    <View style={styles.container}>
      <NativeBaseProvider style={styles.container}>
          <LoginScreen />
          <StatusBar style="auto" />
      </NativeBaseProvider>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:  1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 12,
    paddingRight: 12,
    paddingTop: 60,
    paddingBottom: 25
  },
});
