
# CSP4D - Mobile app, Web app and backend.

This monorepo contains all the code related to our project TrekTraker, please read it and follow the next conventions.



## Conventional Branches Specification
We should create short-lived feature branches for new features, bug-fixes, etc. These branches are based on main branch and are merged back into the trunk once the work is completed. We should use the next format to name the branches:
```
$ git branch <type>(<scoop>)/<description>
```

Example:

```
$ git branch feature(mobile)/add-new-payment-types
```

## Types

**feat**: A new feature for the application or library.

**fix**: A bug fix for the application. 

**docs**: Documentation only changes. 

**style**: Changes that do not affect the meaning of the code(white-space, formatting, missing semi-colons, etc.).

**refactor**: A code change that neither fixes a bug nor adds a feature. This type of commit is used to improve code structure or readability.

## Scoops
Provides additional information about the specific app affected, such as **(backend)**, **(mobile)**, or **(web)**.

## Description

A simple description of the change. It should be written in present tense, and not exceed 50 characters. just like **"correct mobile navigation menu alignment"**.

## About Commit Descriptions
We should add a concise description of the change. It should be written in present tense, and not exceed 50 characters.




